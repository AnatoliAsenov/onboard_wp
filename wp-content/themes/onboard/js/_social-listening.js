$(document).ready(function() {

    if ($("#social_listening-plus").length > 0) {



        //////////////////////////////////////
        ////////// objectives dropdown
        //////////////////////////////////////
        //(function(){

            var dropdownUL = $("#socialListening-achieveDropdown");
            var dropdownUL_LIs = $("#socialListening-achieveDropdown li");
            var thePlus = $("#social_listening-plus");
            var clickableLIs = $(".socialListening-achieveLi");
            var achieveInput = $("#socialListening-achieveDropdown-other");
            var okButton = $("#socialListening-achieveDropdown-ok");
            var isDropdownOpen = false;

            $("#socialListening-achieve").click(function(){
                if(isDropdownOpen){

                    // rotate plus
                    thePlus.removeClass('social_listening-plusRotate');
                    // hide dropdown
                    hideTargetDropdown();

                }else{
                    // close other dropdowns
                    if(isSocialAcoountsDropdownOpen){
                        closeSocialAccounts(100);
                    }
                    if(isURLsDropdownOpen){
                        closeURLs(100);
                    }

                    // rotate plus
                    thePlus.addClass('social_listening-plusRotate');
                    // show dropdown
                    dropdownUL.css('display', 'block').animate({height: '294px'}, 700, function(){
                        dropdownUL_LIs.css('display', 'block');
                        isDropdownOpen = true;
                    });

                }
            });

            thePlus.click(function(){
                if(isDropdownOpen){

                    // rotate plus
                    thePlus.removeClass('social_listening-plusRotate');
                    // hide dropdown
                    hideTargetDropdown();

                }else{

                    // rotate plus
                    thePlus.addClass('social_listening-plusRotate');
                    // show dropdown
                    dropdownUL.css('display', 'block').animate({height: '294px'}, 700, function(){
                        dropdownUL_LIs.css('display', 'block');
                        isDropdownOpen = true;
                    });

                }
            });

            clickableLIs.click(function(){
                var clicked = $(this);
                clickableLIs.removeClass('socialListening-achieveLi-clicked');
                clicked.addClass('socialListening-achieveLi-clicked');
            });

            achieveInput.click(function(){
                clickableLIs.removeClass('socialListening-achieveLi-clicked');
            });

            okButton.click(function(){
                // set hidden input with the value of selected option
                if(dropdownUL_LIs.hasClass('socialListening-achieveLi-clicked')){
                    dropdownUL_LIs.each(function(){
                        if( $(this).hasClass('socialListening-achieveLi-clicked') ){
                            $("#objectivesHiddenInput").val( $(this).text() );
                        }
                    });
                }else {


                    if (achieveInput.val() != '' && achieveInput.val() != ' ') {
                        $("#objectivesHiddenInput").val(achieveInput.val());
                    } else {
                        alert('Select an option or close dropdown with clicking on "X" !');
                        return false;
                    }

                }
                // rotate plus
                thePlus.removeClass('social_listening-plusRotate');
                // hide dropdown
                hideTargetDropdown();
                $("#socialListening-achieveText").text( $("#objectivesHiddenInput").val() );
            });

            function hideTargetDropdown(){
                dropdownUL_LIs.css('display', 'none');
                dropdownUL.animate({height: 0}, 700, function(){
                    dropdownUL.css('display', 'none');
                    isDropdownOpen = false;
                });
            }

        //}());






        /////////////////////////////////////////////////////
        /////////// influencers and sources
        ////////////////////////////////////////////////////
        //(function(){

            var socialAccountsContent = [];
            var URLsContent = [];

            var socialAccounts = $("#social_listening-sources-1");
            var socialAccountDropdown = $("#socialAccountsDropdown");
            var socialAccountDropdown_lastHeight = '180';
            var socialAccountsPlus = $("#socialAccounts-plus");
            var socialAccountsADD = $("#socialAccountsADD");
            var socialAccountsInput = $("#socialAccountsInput");
            var socialAccountsContainer = $("#socialAccountsContainer");
            var URLs = $("#social_listening-sources-2");
            var URLsADD = $("#URLsADD");
            var URLsPlus = $("#socialURLS-plus");
            var URLsInput = $("#urlsInput");
            var URLsDropdown = $("#URLsDropdown");
            var URLsDropdown_lastHeight = '180';
            var URLsContainer = $("#URLsContainer");
            var sourcesDropdownReady = $("#sourcesDropdownReady");
            var URLsDropdownReady = $("#URLsDropdownReady");

            var isSocialAcoountsDropdownOpen = false;
            var isURLsDropdownOpen = false;

            socialAccounts.click(function(){
                if(isSocialAcoountsDropdownOpen){
                    // is open
                    closeSocialAccounts(700);
                }else{
                    // is close
                    openSocialAccounts();
                }
            });

            URLs.click(function(){
                if(isURLsDropdownOpen){
                    // is open
                    closeURLs(700);
                }else{
                    // is close
                    openURLs();
                }
            });

            socialAccountsADD.click(function(){
                socialAccountDropdown.css("height", "auto");
                var inputValue = socialAccountsInput.val();
                if(inputValue !== "" && inputValue !== " " && inputValue.length > 3){
                    socialAccountsContent.push(inputValue);
                    // insert in to socialAccountsContainer
                    socialAccountsContainer.append( addSearchWord('socialaccount', inputValue, socialAccountsContent.length-1) );
                }else{
                    alert("Please insert SOCIAL ACCOUNTS !");
                }
                socialAccountsInput.val("");
                boxedWordX_clickEvent();
            });

            URLsADD.click(function(){
                URLsDropdown.css("height", "auto");
                var inputValue = URLsInput.val();
                if(inputValue !== "" && inputValue !== " " && inputValue.length > 3){
                    URLsContent.push(inputValue);
                    // insert in to socialAccountsContainer
                    URLsContainer.append( addSearchWord('urls', inputValue, URLsContent.length-1) );
                }else{
                    alert("Please insert URLs !");
                }
                URLsInput.val("");
                boxedWordX_clickEvent();
            });

            sourcesDropdownReady.click(function(){
                if(socialAccountsContent.length > 0){
                    $( 'input[name~="sources_accounts"]' ).val( socialAccountsContent.join("  *  ") );
                }
                closeSocialAccounts(600);
            });

            URLsDropdownReady.click(function(){
                if(URLsContent.length > 0){
                    $( 'input[name~="sources_urls"]' ).val( URLsContent.join("  *  ") );
                }
                closeURLs(600);
            });


            function boxedWordX_clickEvent() {
                $(".boxedWordX").off("click");
                $(".boxedWordX").click(function () {

                    var thePositionInArray = $(this).data('position');

                    if ($(this).data('type') == 'socialaccount') {
                        // remove from social account dropdown
                        socialAccountsContent.splice(thePositionInArray, 1);
                    } else {
                        // remove from URLs dropdown
                        URLsContent.splice(thePositionInArray, 1);
                    }

                    $(".boxedWordX").each(function () {
                        if( $(this).data('position') == thePositionInArray ){
                            $(this).parent().remove();
                        }
                    });
                });
            }

            function openSocialAccounts(){
                if(isURLsDropdownOpen) {
                    closeURLs(100);
                }

                socialAccountDropdown.css('display', 'block').animate({height: socialAccountDropdown_lastHeight + "px"}, 700, function(){
                    $("#socialAccountsDropdown li").css('display', 'block');
                });

                rotatePlusSymbol(socialAccountsPlus);
                isSocialAcoountsDropdownOpen = true;
            }

            function closeSocialAccounts(animationTime){
                // remember last height of the dropdown
                socialAccountDropdown_lastHeight = socialAccountDropdown.height();

                $("#socialAccountsDropdown li").css('display', 'none');
                socialAccountDropdown.animate({height: 0}, animationTime, function(){
                    socialAccountDropdown.css('display', 'none');
                });

                rotatePlusSymbol(socialAccountsPlus);
                isSocialAcoountsDropdownOpen = false;
            }

            function openURLs(){
                $("#URLsDropdown").css('display', 'block').animate({height: URLsDropdown_lastHeight+'px'}, 700, function(){
                    $("#URLsDropdown li").css('display', 'block');
                });
                rotatePlusSymbol(URLsPlus);
                isURLsDropdownOpen = true;
            }

            function closeURLs(animationTime){
                URLsDropdown_lastHeight = URLsDropdown.height();

                $("#URLsDropdown li").css('display', 'none');
                $("#URLsDropdown").animate({height: 0}, animationTime, function(){
                    $("#URLsDropdown").css('display', 'none');
                });
                rotatePlusSymbol(URLsPlus);
                isURLsDropdownOpen = false;
            }

            function rotatePlusSymbol(thePlus){
                if(thePlus.hasClass('social_listening-plusRotate')){
                    // rotate plus
                    thePlus.removeClass('social_listening-plusRotate');
                }else{
                    // rotate plus
                    thePlus.addClass('social_listening-plusRotate');
                }
            }



            function addSearchWord(type, theWord, positionInArray){
                return "<div class='boxedWord' data-type='"+type+"' data-position='"+positionInArray+"'>"+theWord+"<span class='boxedWordX' data-position='"+positionInArray+"' data-type='"+type+"' >+</span></div>";
            }

            //function setValueOfHiddenInput(){
            //
            //    if(URLsIsActive || socialAccountsIsActive){
            //        theInputValue = "";
            //    }else{
            //        theInputValue = "not set";
            //    }
            //
            //    if(URLsIsActive){
            //        theInputValue = "URLs";
            //    }
            //
            //    if(socialAccountsIsActive){
            //        if(URLsIsActive){
            //            theInputValue = theInputValue + " AND ";
            //        }
            //        theInputValue = theInputValue + "Social Accounts";
            //    }
            //
            //    hiddenInput.val( theInputValue );
            //}

        //}());




        // submit the form
        $("#socialListeningForm-submit").click(function(){
            setValueOfHiddenInput();
            $('socialListeningFormSend').click();
        });




        //////////////////////////////////////////
        //////// free demo button
        //////////////////////////////////////////
        (function(){

            var compensationHeight = 256;

            $("#social_listening-block1-button").click(function(){
                //if(animationBlock2Active){
                //    compensationHeight = 0;
                //}
                var titleTopOffset = $("#socialListeningForm-title").offset().top;
                $("html, body").animate({
                    scrollTop: (titleTopOffset - compensationHeight)
                }, 1500);
            });

        }());

    }

});
