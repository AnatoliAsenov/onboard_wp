<?php

/**
 * Onboard CRM
 * User: Anatoli Asenov
 */

// how to use:
// $thumbnail = $stringManipulator->stringExtract($postContent, '{thumbnail}', '{/thumbnail}');
// return only the piece of text between the start and end strings

class StringManipulation
{
    public $neededSubString = "";
    public $reducedString = "";

    public function __construct(){}

    // extract substring
    public function stringExtract($string, $start, $end)
    {
        $pos = stripos($string, $start);// num position of beginning of $start string
        $str = substr($string, $pos);// $string minus $start strings
        $str_two = substr($str, strlen($start));
        $second_pos = stripos($str_two, $end);// length of the needed string (end-start)
        $str_three = substr($str_two, 0, $second_pos);
        $unit = trim($str_three);// remove whitespaces
        return $unit;
    }

    // extract substring and then delete it from $string
    public function stringExtractAndDelete($string, $start, $end)
    {
        $pos = stripos($string, $start);// num position of beginning of $start string
        $str = substr($string, $pos);// $string minus $start strings
        $str_two = substr($str, strlen($start));
        $second_pos = stripos($str_two, $end);// length of the needed string (end-start)
        $str_three = substr($str_two, 0, $second_pos);
        $this->neededSubString = trim($str_three);// remove whitespaces

        $stringMinusStartAndNeeded = substr($str_two, strlen($this->neededSubString));
        $this->reducedString = substr($stringMinusStartAndNeeded, strlen($end));
    }
}