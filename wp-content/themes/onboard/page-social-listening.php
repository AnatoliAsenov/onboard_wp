<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */


include 'string_manipulation/StringManipulation.php';
include 'templates/MiddlePageBoxes.php';
include 'templates/BottomRedLine.php';

get_header();
$siteURL = get_site_url();

?>
    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/banners/whatwedo.jpg" id="topBanner">
<?php

// string manipulator
$stringManipulator = new StringManipulation();

$postContent = $post->post_content;


//====== block 1 =======

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{block1-title}', '{/block1-title}');
$block1title = $stringManipulator->neededSubString;
//$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{block1-content}', '{/block1-content}');
$block1content = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;
?>
    <div id="careers-block1-title"><?php echo $block1title; ?></div>
    <div id="careers-block1-text"><?php echo $block1content; ?></div>

    <div id="social_listening-block1-button">FREE DEMO</div>

    <table id="careers-block1-picwithicons" style="background-image: url('<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/bg.png')"   border="0" cellspacing="0" cellpadding="0">
        <tr>
            <th id="careers-block1-picwithicons-title" colspan="4">SOCIAL LISTENING BENEFITS</th>
        </tr>
        <tr>
            <th class="careers-block1-picwithicons-darker"><img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/binoculars.png" class="careers-block1-picwithicons-img"></th>
            <th class="careers-block1-picwithicons-lighter"><img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/chess.png" class="careers-block1-picwithicons-img"></th>
            <th class="careers-block1-picwithicons-darker"><img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/magnet.png" class="careers-block1-picwithicons-img"></th>
            <th class="careers-block1-picwithicons-lighter"><img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/atomic.png" class="careers-block1-picwithicons-img"></th>
        </tr>
        <tr>
            <th class="careers-block1-picwithicons-darker"><p class="careers-block1-picwithicons-row1">INTELLIGENCE</p></th>
            <th class="careers-block1-picwithicons-lighter"><p class="careers-block1-picwithicons-row1">COMPETITION</p></th>
            <th class="careers-block1-picwithicons-darker"><p class="careers-block1-picwithicons-row1">INFLUENCE</p></th>
            <th class="careers-block1-picwithicons-lighter"><p class="careers-block1-picwithicons-row1">OPPORTUNITIES</p></th>
        </tr>
        <tr>
            <th class="careers-block1-picwithicons-darker">
                <p class="careers-block1-picwithicons-row3">It's never been easier to gain relevant segment insights</p>
            </th>
            <th class="careers-block1-picwithicons-lighter">
                <p class="careers-block1-picwithicons-row3">Hard to outplay them if you don't know them</p>
            </th>
            <th class="careers-block1-picwithicons-darker">
                <p class="careers-block1-picwithicons-row3">Get closer to the experts, learn & engage</p>
            </th>
            <th class="careers-block1-picwithicons-lighter">
                <p class="careers-block1-picwithicons-row3">People actually look for product & services suggestions on social media</p>
            </th>
        </tr>
    </table>

    <div id="socialListeningForm-title">REQUEST DEMO</div>

    <form action="" method='POST' id="socialListeningForm" accept-charset='UTF-8'>
        <ul id="socialListeningUL">
            <li>
                <div id="socialListeningBox" class="icon icon-open-box"></div>
                <div id="socialListeningNextToBox"></div>
            </li>
            <li>
                <div class="social-listening-L">OBJECTIVES</div>
                <div class="social_listening-R">
                    <input type="hidden" name="objectives" id="objectivesHiddenInput" />
                    <div id="socialListening-achieve" class="socialListeningBigButton">
                        <span id="socialListening-achieveText">WHAT DO YOU WISH TO ACHIEVE?</span>
                        <span id="social_listening-plus" class="socialListeningPlusBtn">+</span>
                    </div>
                    <ul id="socialListening-achieveDropdown">
                        <li class="socialListening-achieveLi">Find new business</li>
                        <li class="socialListening-achieveLi">Monitor & boost brand success</li>
                        <li class="socialListening-achieveLi">Monitor competition</li>
                        <li class="socialListening-achieveLi">Measure campaign effectiveness</li>
                        <li>
                            <input type="text" placeholder="Other..." value="" id="socialListening-achieveDropdown-other"/>
                        </li>
                        <li id="socialListening-achieveDropdown-ok-row">
                            <div id="socialListening-achieveDropdown-ok">OK</div>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="social-listening-L">TARGET AUDIENCE</div>
                <div class="social_listening-R">
                    <input type="text" placeholder="What do you want to listen to?" name="target-what" />
                </div>
            </li>
            <li class="marginTo10">
                <div class="social-listening-L"></div>
                <div class="social_listening-R">
                    <input type="text" placeholder="What industry are you interested in?" name="target-industry" />
                </div>
            </li>
            <li>
                <div class="social-listening-L">INFLUENCERS & SOURCES</div>
                <div class="social_listening-R" >
                    <!-- social accounts -->
                    <input type="hidden" name="sources_accounts" value="not set" />
                    <div class="socialListeningBigButton" id="social_listening-sources-1">
                        <span>Social accounts</span>
                        <span id="socialAccounts-plus" class="socialListeningPlusBtn">+</span>
                    </div>
                    <ul id="socialAccountsDropdown" class="influencersDropdowns">
                        <li>
                            <div id="socialAccountsContainer"></div>
                            <input type="text" class="socialAccountsDropdownInput" id="socialAccountsInput">
                            <div class="sourcesDropdownBtns" id="socialAccountsADD">ADD</div>
                        </li>
                        <li>
                            <div class="sourcesDropdownBtns" id="sourcesDropdownReady">READY</div>
                        </li>
                    </ul>
                    <!-- URLs -->
                    <input type="hidden" name="sources_urls" value="not set" />
                    <div class="socialListeningBigButton" id="social_listening-sources-2">
                        <span>URLs</span>
                        <span id="socialURLS-plus" class="socialListeningPlusBtn">+</span>
                    </div>
                    <ul id="URLsDropdown" class="influencersDropdowns">
                        <li>
                            <div id="URLsContainer"></div>
                            <input type="text" class="socialAccountsDropdownInput" id="urlsInput">
                            <div class="sourcesDropdownBtns" id="URLsADD">ADD</div>
                        </li>
                        <li>
                            <div class="sourcesDropdownBtns" id="URLsDropdownReady">READY</div>
                        </li>
                    </ul>
                </div>
            </li>
            <li>
                <div class="social-listening-L">HOT TOPICS & KEYWORDS</div>
                <div class="social_listening-R">
                    <input type="text" placeholder="What is important in your line of business?" name="keywords-important" />
                </div>
            </li>
            <li class="marginTo10">
                <div class="social-listening-L"></div>
                <div class="social_listening-R">
                    <input type="text" placeholder="What language does your audience speak?" name="keywords-language" />
                </div>
            </li>
            <li>
                <div class="social-listening-L">SOCIAL PRESENCE</div>
                <div class="social_listening-R">
                    <input type="text" placeholder="Where are you active?" name="presence-where" />
                </div>
            </li>
            <li class="marginTo10">
                <div class="social-listening-L"></div>
                <div class="social_listening-R">
                    <input type="text" placeholder="What content are you creating?" name="presence-content" />
                </div>
            </li>
            <li class="marginTo10">
                <div class="social-listening-L"></div>
                <div class="social_listening-R">
                    <input type="text" placeholder="Where is the social conversation you want to participate in?" name="presence-conversation" />
                </div>
            </li>
            <li>
                <div class="social-listening-L">ADDITIONAL INFORMATION</div>
                <div class="social_listening-R">
                    <textarea name="additional-info" placeholder="Add more details..."></textarea>
                </div>
            </li>
        </ul>
        <input type="submit" style="display:none;" id="socialListeningFormSend"/>
        <div id="socialListeningForm-submit">SUBMIT YOUR REQUEST</div>

    </form>


<!-- footer -->
<?php

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();
