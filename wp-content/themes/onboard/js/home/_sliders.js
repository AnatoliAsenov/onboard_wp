//__________________________________________________________
//__________________     carousels    ______________________
//__________________________________________________________

$(document).ready(function() {

    if($("#whatisthispage").text() == "home") {
        //============================================
        //________________ top slider ________________
        //============================================
        (function () {

            var beforeUpdateSlide = function () {
                //console.log("преди");
            };
            var afterUpdateSlide = function () {
                //console.log("след");

                // get asoc data of the images
                var items = $(".item");
                var asocText = [];
                items.each(function () {
                    var asocData = $(this).data("asoctext");
                    asocText.push(asocData);
                });

                var count = 1;
                items.each(function () {
                    if ($(this).hasClass("slick-active")) {
                        //separate the text on to lines
                        var textRows = asocText[count].split("*");
                        var newHTML = "<p class='sliderText_line1'>" + textRows[0] + "</p><p class='sliderText_line2'>" + textRows[1] + "</p><p class='sliderText_line3'>" + textRows[2] + "</p>";
                        //change the text in white triangle
                        var textHolder = $(".sliderText");
                        textHolder.fadeOut().promise().done(function () {
                            textHolder.html(newHTML).fadeIn(800);
                        });
                    }
                    count++;
                });
            };

            //------------------ carousel init ------------------------
            var carouselContainer = $('#owl-demo');
            carouselContainer.slick({
                dots: true,
                infinite: true,
                speed: 1400,
                slidesToShow: 1,
                accessibility: false,
                autoplay: true,
                arrows: false
            });

            // carousel change events
            carouselContainer.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                    afterUpdateSlide();
                })
                .on('afterChange', function (event, slick, currentSlide, nextSlide) {

                });


        })();


        //====================================================
        //________________ testimonial slider ________________
        //====================================================
        (function () {

            // init carousel
            var carouselContainer = $('#testimonials');
            carouselContainer.slick({
                dots: false,
                infinite: true,
                speed: 1400,
                slidesToShow: 1,
                accessibility: false,
                autoplay: false,
                arrows: false
            });

            // carousel change events
            var quotes = $(".testimonials_item_quote");
            carouselContainer.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                    quotes.animate({opacity: 0}, 50);
                })
                .on('afterChange', function (event, slick, currentSlide, nextSlide) {
                    quotes.animate({opacity: 0.4}, 500);
                });

            $("#testimonials_right_arrow").click(function () {
                $('#testimonials').slick('slickNext');
            });

            $("#testimonials_left_arrow").click(function () {
                $('#testimonials').slick('slickPrev');
            });

        })();

        dotsProperties();
        setTimeout(positionTheDots, 1200);
    }

});


var dotsGeoPosition = [[64, 47], [60, 43], [41, 42], [59, 48], [66, 40], [83, 50], [78, 52], [78, 58], [63, 76], [85, 74], [73, 55], [49, 72], [46, 46], [36, 47]];

//==================================================
//=============== map mask and dots ================
//==================================================
var dotsProperties = function() {

    //------- positioning the mask of the top slider ---------
    var sliderImg = $(".slick-slide");
    var imgHeight = sliderImg.eq(0).height();
    var imgOffset = sliderImg.eq(0).offset();
    var maskHeight = $(".maskHolder img").height();
    $(".maskHolder").css("top", ((imgOffset.top + imgHeight) - maskHeight) + "px");

    //------- position all dots in Sofia -------
    for (var i = 0; i < dotsGeoPosition.length; i++) {
        var dot = document.createElement('img');
        dot.src = $("#dotID").attr('src');
        dot.className = "maskDots";
        dot.style.zIndex = (111 + i) + "";
        document.body.appendChild(dot);
    }

};

var positionTheDots = function() {

    //--- hide loading animation ---
    $("#loadingContainer").animate({
        opacity: 0
    }, 300, function () {
        $(this).css("display", "none");
        showTheDots();
    });

    function showTheDots() {
        //--- show dots and position them ---
        var ableToInvoke = true;
        $(".maskDots").animate({
            opacity: 1
        }, 800, function () {
            if (ableToInvoke) {
                positionDOTS();
                ableToInvoke = false;
            }
        });

        var positionDOTS = function () {
            var maskIMG = $(".maskHolder img");
            var maskHeight = maskIMG.height();
            // position dots on map / different cities
            var maskOffset = maskIMG.offset();

            for (var z = 0; z < dotsGeoPosition.length; z++) {
                var topPropOfDots = (maskHeight + maskOffset.top + 20) * dotsGeoPosition[z][1] / 100;
                $(".maskDots").eq(z).animate({
                    top: topPropOfDots + "px",
                    left: dotsGeoPosition[z][0] + "%"
                }, 600, function () {
                    // callback after position the dots
                });
            }
        };
    }

};
