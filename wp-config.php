<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'onboard_wp');

/** MySQL database username */
define('DB_USER', 'reg_user');

/** MySQL database password */
define('DB_PASSWORD', 'rDu0HYOyPBuFTrPZ');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k9q0K{r}OQ<QbG8PO&WjSac4h-W[Q+Bd-`:T64$3W2{[H{e=#:hs][&v!O FfmFb');
define('SECURE_AUTH_KEY',  'lQ2DQ^)f.~b~p:p0/Has`P+JgbU`65y~o+0lexoRJ0{z`Rupi+52o//l(2`{`kHE');
define('LOGGED_IN_KEY',    ':t< EEkLt+YB4/2r;C<,R~6`0C`;8grc!jq,KXC,6yLSkb%MoeQZn{>oO8lBhI?#');
define('NONCE_KEY',        '2X^Dcl[%(-Aa%RpZ2OJ(:|OV]4Ev*jgg^g)rCRhYQ:r`s Hlany<*]fm-(pum8E5');
define('AUTH_SALT',        ':CYb6c(ryt}iyu4;yD M||tj3I3V=`-)iF9tajNEO~S/JPDC>p,XNeWuj$LLPU>d');
define('SECURE_AUTH_SALT', '+7-_)yO7a6|~Gq;4l]Erl5 g|<wVTq_=~lDdbY=;93+,IsJt=mODkMN&=E9!$f1f');
define('LOGGED_IN_SALT',   'K+{_T{TR/;F9GCN;R~{^XQ/3g]1IxOvNVTh#Zf6WzX>EsNydv=|pG!8Y_,l>)8Ru');
define('NONCE_SALT',       ';k63tar,yR9c&Ib}1G:*y5d7X4@>v-m7xxb#bk8+*1|7vw5FvtgHtVD$2j-s?p:$');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
