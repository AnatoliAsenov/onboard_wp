<?php


include 'string_manipulation/StringManipulation.php';
include 'templates/MiddlePageBoxes.php';
include 'templates/BottomRedLine.php';

get_header();
$siteURL = get_site_url();

?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/banners/whatwedo.jpg" id="topBanner">

    <!-- bg image -->
    <img id="channelMarketingBgImg" src="<?php bloginfo('template_url'); ?>/images/whatwedo/channel_marketing/bg_channel.jpg" />

    <div id="whatWeDo_title" style="margin-top:-670px;">CHANNEL MARKETING</div>

<?php

// string manipulator
$stringManipulator = new StringManipulation();

$postContent = $post->post_content;


//====== block 1 =======

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph1}', '{/paragraph1}');
$paragraph1 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph2}', '{/paragraph2}');
$paragraph2 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph3}', '{/paragraph3}');
$paragraph3 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

//====== block 2 =======

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block2, '{paragraph1}', '{/paragraph1}');
$paragraph4 = $stringManipulator->neededSubString;
$block2 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block2, '{paragraph2}', '{/paragraph2}');
$paragraph5 = $stringManipulator->neededSubString;
$block2 = $stringManipulator->reducedString;

//====== block 3 =======

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph1}', '{/paragraph1}');
$paragraph6 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph2}', '{/paragraph2}');
$paragraph7 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph3}', '{/paragraph3}');
$paragraph8 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph4}', '{/paragraph4}');
$paragraph9 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph5}', '{/paragraph5}');
$paragraph10 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

?>


    <div id="channelMarketingContainer">

        <ul id="channelMarketingContainerUl1">
            <li class="channelMarketingVertical1">THE CHALLENGE</li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/whatwedo/channel_marketing/respond_icon.png" /></li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/whatwedo/channel_marketing/increase_icon.png" /></li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/whatwedo/channel_marketing/generate_icon.png" /></li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/whatwedo/channel_marketing/gain_icon.png" /></li>
        </ul>
        <div id="channelMarketingBlock1Container">
            <p class="channelMarketingBlock1"><?= $paragraph1 ?></p>
            <p class="channelMarketingBlock1"><?= $paragraph2 ?></p>
            <p class="channelMarketingBlock1"><?= $paragraph3 ?></p>
        </div>
        <ul id="channelMarketingContainerUl2">
            <li class="channelMarketingVertical2">
                <p>OUR SOLUTION</p>
                <p>CHANNEL CONCIERGE</p>
            </li>
            <li>
                <div id="channelMarketingContainerUl2-r1"><?= $paragraph4 ?></div>
                <div id="channelMarketingContainerUl2-r2"><?= $paragraph5 ?></div>
            </li>
        </ul>

        <div class="channelMarketingSubTitle">THE GORILLA ONBOARD BENEFITS</div>
        <ul id="channelMarketingBenefits">
            <li>
                <div class="list-triangle facts-R-list-triangle-inline"></div>
                <p><?= $paragraph6 ?></p>
            </li>
            <li>
                <div class="list-triangle facts-R-list-triangle-inline"></div>
                <p><?= $paragraph7 ?></p>
            </li>
            <li>
                <div class="list-triangle facts-R-list-triangle-inline"></div>
                <p><?= $paragraph8 ?></p>
            </li>
            <li>
                <div class="list-triangle facts-R-list-triangle-inline"></div>
                <p><?= $paragraph9 ?></p>
            </li>
            <li>
                <div class="list-triangle facts-R-list-triangle-inline"></div>
                <p><?= $paragraph10 ?></p>
            </li>
        </ul>

    </div>


    <!-- footer -->
<?php

// bottom red line content
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();