<?php
include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(185);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(185);
$header->printHTML();

$siteURL = get_site_url();
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">facts</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/ff_768.jpg" id="topBanner">

<?php
$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{principles}', '{/principles}');
$principles = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>

<div id="ff-partnership-container">
    <h1 id="ff-partnership-title"><?php echo $title; ?></h1>
    <p class="ff-partnership-text"><?php echo $block1; ?></p>
    <p class="ff-partnership-text"><?php echo $block2; ?></p>


    <style>
        @media screen and (max-width: 320px) {
            #ff-partnership-img{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/boxes_320.png");
                height: 832px;
                width: 180px;
                margin: 70px auto 0 auto;
            }
        }
        @media screen and (max-width: 478px) and (min-width: 320px) {
            #ff-partnership-img{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/boxes_320.png");
                height: 832px;
                width: 180px;
                margin: 70px auto 0 auto;
            }
        }
        @media screen and (max-width: 766px) and (min-width: 478px) {
            #ff-partnership-img{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/boxes_480.png");
                height: 533px;
                width: 400px;
                margin: 70px auto 0 auto;
            }
        }
        @media screen and (max-width: 1022px) and (min-width: 766px){
            #ff-partnership-img{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/boxes_768.png");
                height: 352px;
                width: 600px;
                margin: 70px auto 0 auto;
            }
        }
    </style>
    <img src="" id="ff-partnership-img" />

    <ul id="ff-partnership-principles">
<?php
        $numberOfPrinciples = preg_match_all('/\bsingle-principle\b/', $principles);

        for($c = 0; $numberOfPrinciples/2 > $c; $c++){
            $stringManipulator->stringExtractAndDelete($principles, '{single-principle}', '{/single-principle}');
            $tempPrinciple = $stringManipulator->neededSubString;
            $principles = $stringManipulator->reducedString;
?>
            <li class="ff-partnership-principles-li">
                <div class="red-triangle-list-element"></div>
                <p class="ff-partnership-princip"><?php echo $tempPrinciple; ?></p>
            </li>
<?php
        }
?>
    </ul>

</div>




    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();