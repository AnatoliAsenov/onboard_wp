<?php

/**
 * Created by Onboard
 * User: Anatoli Asenov
 */
class MiddlePageLine
{
    private $text = "";
    private $color = "";

    public function __construct(){}

    public function setText($text){
        $this->text = $text;
    }

    public function setBackgroundColor($color){
        $this->color = $color;
    }

    public function printHTML(){
?>

<ul id="technology-middleLine" style="background-color:<?php echo $this->color; ?>;">
    <li id="technology-middleLine-icon"><i class="demo-icon icon-quote-right-alt">&#xe801;</i></li>
    <li id="technology-middleLine-text"><h1><?php echo $this->text; ?></h1></li>
</ul>

<div id="rocketBufferDiv"></div>
<img id="theRocket" src="<?php bloginfo('template_url'); ?>/images/technology/rocket.png">

<?php
    }
}