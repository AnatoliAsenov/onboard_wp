<?php
/**
 * Created by Onboard
 * User: AnatoliAsenov
 */


//  =========================================
//      custom post type - CaseStudies
//  =========================================

function caseStudies_custom_post_type(){
    $labels = array(
        'name' => 'CaseStudies',
        'singular_name' => 'CaseStudy',
        'add_new' => 'Add Case Study',
        'all_items' => 'All Case Studies',
        'add_new_item' => 'Add New Case Study',
        'edit_item' => 'Edit Case Study',
        'new_item' => 'New Case Study',
        'view_item' => 'View Case Study',
        'search_item' => 'Search Case Study',
        'not_found' => 'No Case Study Found!',
        'not_found_in_trash' => 'No Case Study Found in Trash!',
        'parent_item_colon' => 'Parent Case Study'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array('title', 'editor', 'excerpt', 'thumbnails', 'revisions'),
        'taxonomies' => array(),
        'menu_position' => 5,
        'exclude_from_search' => false
    );
    register_post_type('casestudies', $args);
};
add_action('init', 'caseStudies_custom_post_type');


function caseStudies_custom_taxonomies(){
    // add new taxonomy hierarchical
    $labels = array(
        'name' => 'Case Study Type',
        'singular_name' => 'Case Study Type',
        'search_items' => 'Search Types',
        'all_items' => 'All Case Studies',
        'parent_item' => 'Parent Case Study Type',
        'parent_item_colon' => 'Parent Case Study Type:',
        'edit_item' => 'Edit Type',
        'update_item' => 'Update Type',
        'add_new_item' => 'Add New Type',
        'new_item_name' => 'New Type Name',
        'menu_name' => 'Case Study Type'
    );
    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'casestudytype')
    );
    register_taxonomy('casestudytype', array('casestudies'), $args );

    // add new taxonomy NOT hierarchical

}
add_action('init', 'caseStudies_custom_taxonomies');



//  =========================================
//      custom post type - Employee Quote - disabled
//  =========================================
function employeeQuote_custom_post_type(){
    $labels = array(
        'name' => 'EmployeeQuote',
        'singular_name' => 'EmployeeQuote',
        'add_new' => 'Add Employee Quote',
        'all_items' => 'All Employee Quotes',
        'add_new_item' => 'Add New Employee Quote',
        'edit_item' => 'Edit Employee Quote',
        'new_item' => 'New Employee Quote',
        'view_item' => 'View Employee Quote',
        'search_item' => 'Search Employee Quote',
        'not_found' => 'No Employee Quote Found!',
        'not_found_in_trash' => 'No Employee Quote Found in Trash!',
        'parent_item_colon' => 'Parent Employee Quote'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array('title', 'editor'),
        'taxonomies' => array(),
        'menu_position' => 6,
        'exclude_from_search' => false
    );
    register_post_type('employeequote', $args);
};
//add_action('init', 'employeeQuote_custom_post_type');


function employeeQuote_custom_taxonomies(){
    // add new taxonomy hierarchical
    $labels = array(
        'name' => 'Employee Quote Type',
        'singular_name' => 'Employee Quote Type',
        'search_items' => 'Search Types',
        'all_items' => 'All Employee Quotes',
        'parent_item' => 'Parent Employee Quote Type',
        'parent_item_colon' => 'Parent Employee Quote Type:',
        'edit_item' => 'Edit Type',
        'update_item' => 'Update Type',
        'add_new_item' => 'Add New Type',
        'new_item_name' => 'New Type Name',
        'menu_name' => 'Employee Quote Type'
    );
    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'employeequote')
    );
    register_taxonomy('employeequote', array('employeequote'), $args );

    // add new taxonomy NOT hierarchical

}
//add_action('init', 'employeeQuote_custom_taxonomies');




//  =========================================
//      custom post type - Jobs
//  =========================================

function jobs_custom_post_type(){
    $labels = array(
        'name' => 'Jobs',
        'singular_name' => 'Job',
        'add_new' => 'Add New Job',
        'all_items' => 'All Jobs',
        'add_new_item' => 'Add New Job',
        'edit_item' => 'Edit Job',
        'new_item' => 'New Job',
        'view_item' => 'View Job',
        'search_item' => 'Search Job',
        'not_found' => 'No Job Found!',
        'not_found_in_trash' => 'No Job Found in Trash!',
        'parent_item_colon' => 'Parent Job'
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'has_archive' => true,
        'publicly_queryable' => true,
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'hierarchical' => false,
        'supports' => array('title', 'editor'),
        'taxonomies' => array(),
        'menu_position' => 6,
        'exclude_from_search' => false
    );
    register_post_type('jobs', $args);
};
add_action('init', 'jobs_custom_post_type');


function jobs_custom_taxonomies(){
    // add new taxonomy hierarchical
    $labels = array(
        'name' => 'Jobs Type',
        'singular_name' => 'Job Type',
        'search_items' => 'Search Types',
        'all_items' => 'All Jobs',
        'parent_item' => 'Parent Job Type',
        'parent_item_colon' => 'Parent Job Type:',
        'edit_item' => 'Edit Type',
        'update_item' => 'Update Type',
        'add_new_item' => 'Add New Type',
        'new_item_name' => 'New Type Name',
        'menu_name' => 'Job Type'
    );
    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'jobs')
    );
    register_taxonomy('jobs', array('jobs'), $args );

    // add new taxonomy NOT hierarchical

}
add_action('init', 'jobs_custom_taxonomies');