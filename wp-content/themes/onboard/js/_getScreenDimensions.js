/**
 * Created by Anatoli Asenov
 */

var screenWidthOnLoad22 = document.documentElement.clientWidth;
$(window).resize(function() {
    if ($(window).width() != screenWidthOnLoad22) {
        location.reload();
    }
});


var getScreenWidth = function() {
    if (self.innerWidth) {
        return self.innerWidth;
    }

    if (document.documentElement && document.documentElement.clientWidth) {
        return document.documentElement.clientWidth;
    }

    if (document.body) {
        return document.body.clientWidth;
    }
};


//====================================================
//==== set min height of page for BASIC templates ====
//====================================================

$(document).ready(function() {
    if($("#basicContainer").length > 0) {
        var windowHeight = $(window).innerHeight();
        var headerHeight = $("#header").height();
        var bottomRedLineHeight = $("#bottomRedLine").height();
        var footerHeight = $("#subFooter_container").height();
        var basicContainer = $("#basicContainer");

        //console.log("window h: " + windowHeight + " / header h: " + headerHeight + " / bottom red line: " + bottomRedLineHeight + " / footer h: " + footerHeight);

        basicContainer.css("minHeight", (windowHeight - (headerHeight + bottomRedLineHeight + footerHeight + 80)) + "px");
    }
});