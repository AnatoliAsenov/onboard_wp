/**
 * Created by Anatoli Asenov
 */

$(document).ready(function() {

    if ($("#job-form-table").length) {

        $("#job-form-table-attach-btn").click(function(){
            $("#job-form-table-attach-input").click();
        });

        $("#job-form-bottom-white-line-btn").click(function(){
            $('#job-form-hidden-submit-button').trigger('click');
        });

    }

});

function validateForm(){

    return true;
}