<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */
include 'string_manipulation/StringManipulation.php';
include 'templates/BottomRedLine.php';

get_header();

?>
<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">about</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/about/aboutus.jpg" id="topBanner">



<!-- main banner with animations -->
<?php
    $postContent = $post->post_content;
    $stringManipulator = new StringManipulation();
    $aboutPosts = array();
    for($i = 0; $i < 2; $i++) {
        $stringManipulator->stringExtractAndDelete($postContent, '{paragraph-delimiter}', '{/paragraph-delimiter}');
        $aboutPosts[$i] = $stringManipulator->neededSubString;
        $postContent = $stringManipulator->reducedString;
    }
?>

<div id="about-company-main" style="background-image:url('<?php bloginfo('template_url'); ?>/images/about/about-company/company_bg.png');">
    <ul id="about-company-main-ul1">
        <li id="about-company-main-redBlock1">
            <div id="about-company-main-block1">
                <span id="about-company-main-span1">WE ARE</span>
                <span id="about-company-main-span2">ONBOARD</span>
            </div>
            <div id="about-company-main-block2"></div>
        </li>
        <li class="about-company-main-text" id="about-company-main-text1">
            <?php echo $aboutPosts[0]; ?>
        </li>
    </ul>
    <hr>
    <ul id="about-company-main-ul2">
        <li class="about-company-main-text" id="about-company-main-text2">
            <?php echo $aboutPosts[1]; ?>
        </li>
        <li id="about-company-main-redBlock2">
            <div id="about-company-main-block3"></div>
            <div id="about-company-main-block4">
                <img src="<?php bloginfo('template_url'); ?>/images/about/about-company/onboard_logo.png" id="about-company-main-block4-img">
                <span id="about-company-main-span3">We are fully compliant with</span>
                <span id="about-company-main-span4">ISO 9001:2008 and 27001:2013</span>
            </div>
        </li>
    </ul>
</div>
<!-- main banner with animations END -->


<!-- five icons bar -->
<div id="whatDefinesUs">
    <p id="whatDefinesUs_1">WHAT DEFINES US</p>
    <p id="whatDefinesUs_2">We create solutions to impossible problems</p>
</div>
<table id="whatDefinesUs_table">
    <tr>
        <th><img src="<?php bloginfo('template_url'); ?>/images/about/about-company/icon_impobjects1.png"></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><img src="<?php bloginfo('template_url'); ?>/images/about/about-company/icon_impobjects2.png"></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><img src="<?php bloginfo('template_url'); ?>/images/about/about-company/icon_impobjects3.png"></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><img src="<?php bloginfo('template_url'); ?>/images/about/about-company/icon_impobjects4.png"></th>
    </tr>
    <tr>
        <th><p class="whatDefinesUs_subTitles">EXPERIENCE</p></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><p class="whatDefinesUs_subTitles">THOUGHT LEADERSHIP</p></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><p class="whatDefinesUs_subTitles">IMPACTFUL RELATIONSHIP</p></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><p class="whatDefinesUs_subTitles">GLOBAL REACH</p></th>
    </tr>
    <tr>
        <th><p class="whatDefinesUs_subText">We were successful in B2B before CRM automation became standard</p></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><p class="whatDefinesUs_subText">We turn ideas into reality</p></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><p class="whatDefinesUs_subText">It's all about the people, so we focus on genuine experiences that bring meaning</p></th>
        <th class="whatDefinesUs_table_v_buffer"></th>
        <th><p class="whatDefinesUs_subText">We work beyond borders and time, in over 50 languages from 38 locations</p></th>
    </tr>
</table>

<!-- footer -->
<?php

// bottom red line content
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();