<?php
/**
 * Created byOnboard
 * User: Anatoli Asenov
 */

    include 'templates/BottomRedLine.php';

    get_header();

    $siteURL = get_site_url();
?>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/banners/404.jpg" id="topBanner">

    <!-- main content -->
    <ul id="four_zero_four_page">
        <li id="four_zero_four_page_left">
            <h1>404</h1>
            <h2>Oops... page not found</h2>
            <h5>The page you are looking for could not be found.</h5>
            <a href="<?php echo $siteURL."/home";  ?>">HOME PAGE</a>
            <a href="#" id="error_page_contact_us_btn">CONTACT US</a>
        </li>
        <li id="four_zero_four_page_right">
            <img src="<?php bloginfo('template_url'); ?>/images/404/ship.png" width="500" />
        </li>
    </ul>

    <!-- footer -->
<?php

    $subFooter = new BottomRedLine("Learn more about your digital growth opportunities...");
    $subFooter->printHTML();

    get_footer();