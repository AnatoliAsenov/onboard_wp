<?php

//  function loadCSS(){
//    wp_enqueue_style('style.min', get_stylesheet_uri()."/css/style.min.css");
//  };
//
//  add_action('wp_enqueue_script', 'onboard');

//  =========================================
//      theme setup
//  =========================================
function onboard_setup(){
    // add featured image support
    add_theme_support('post-thumbnails');
    add_image_size('small-thumbnail', 180, 120, true);
    add_image_size('banner-image', 920, 210, array('left', 'top'));

    // add post format support
    add_theme_support('post-formats', array('aside', 'gallery', 'link'));
}

add_action('after_setup_theme', 'onboard_setup');
add_theme_support('post-thumbnail');



//include "inc/function-addCSS.php";
include "inc/function-postTypes.php";
include "inc/function-metaBoxes.php";
include "inc/function-excerptLength.php";
include "inc/function-pagination.php";
include "inc/function-session.php";
include "inc/function-query-vars.php";