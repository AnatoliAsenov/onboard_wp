<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(38);
//$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$header = new MobileHeader(38);
$header->printHTML();

$siteURL = get_site_url();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">blog</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/blog/blog_768.jpg" id="topBanner">


    <!-- list of categories -->
<?php
//$categoryOfPage = $_GET['category'];
$categoryOfPage = get_query_var( 'category' );
if($categoryOfPage == "" || $categoryOfPage == " " || $categoryOfPage == null){
    $categoryOfPage = "all";
}
echo "<div id='thePostCategory' style='display: none;'>".$categoryOfPage."</div>";
?>

    <ul id="allPostCategories">
        <li class="categories-item"><a href="<?php echo add_query_arg( 'category', "all", get_permalink() ); ?>" data-id="all" id="allPostCategories_allElement">ALL</a></li>
        <?php
        //$post_id = wp_list_categories('exclude=&title_li=');
        $categoriesList = get_categories();
        for($i = 0; count($categoriesList) > $i; $i++){
            $categoryName = $categoriesList[$i]->name;
            $cleanCategoryName = trim($categoryName, " ");
            $categoryID = $categoriesList[$i]->cat_ID;
            ?>
            <li class="categories-item">
                <a href="<?php echo add_query_arg( 'category', $categoryID, get_permalink() ); ?>" data-id="<?php echo $categoryID; ?>">
                    <?php echo $cleanCategoryName; ?>
                </a>
            </li>
        <?php } ?>
    </ul>



    <!-- main content of the blog -->
<?php
        $tagged = ( get_query_var( 'tag' ) ) ? get_query_var( 'tag' ) : 0;
        if($tagged != "" || $tagged != " " || $tagged != null){
            // there is "tag" send via Get query
            // render the posts associated with this tag
            $args = array(
                'posts_per_page' => 3,
                'paged' => $paged,
                'tag_id' => $tagged
            );
        }else{
            // there is no "tag" send via Get query
            // render the posts from the "category" query
            $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

            if($cleanCategoryName == "all" || $cleanCategoryName == "ALL"){
                // get all posts
                $args = array(
                    'posts_per_page' => 3,
                    'paged' => $paged
                );
            }else{
                // get just posts from the category
                $args = array(
                    'posts_per_page' => 3,
                    'paged' => $paged,
                    'cat' => $categoryOfPage
                );
            }
        }

        $custom_query = new WP_Query( $args );
        set_post_thumbnail_size( 290, 161, true );
?>
    <ul id="blog-mainContainer">
<?php
        while($custom_query->have_posts()) :
            $custom_query->the_post();
            $thumbnailDescription = get_post_meta($post->ID, 'thumbnail_description', true);
        ?>
        <li class="singlePostLi">
            <a href="<?php echo $siteURL."/mobile/mobile-post?id=".$post->ID ?>" class="singlePostLi-thumbnail">
                <?php the_post_thumbnail(); ?>
            </a>
            <a class="blog-title" href="<?php echo $siteURL."/mobile/mobile-post?id=".$post->ID ?>" rel="bookmark">
                <h1><?php the_title(); ?></h1>
            </a>
            <span>
                <span class="blog-big_article_category"><?php echo get_the_category($post->ID)[0]->name; ?></span>
                <i class="demo-icon icon-calendar">&#xe803;</i>
                <span class="blog-big_article_date"><?php the_time('F j, Y'); ?></span>
            </span>
            <!-- a class="link" href="<?php the_permalink(); ?>#comments ">
        <?php comments_number( '', '- 1 comment', '- % comments' ); ?>
    </a -->
            <div>
                <ul>
                    <li class="work-description">
                        <?php echo $thumbnailDescription; ?>
                    </li>
                    <li class="blog-description-arrow">
                        <a href="<?php echo $siteURL."/mobile/mobile-post?id=".$post->ID ?>">&#8594;</a>
                    </li>
                </ul>
            </div>
        </li>
        <!-- end blog posts -->
        <?php endwhile; ?>
    </ul>

    <hr class="blog_hr">

        <!-- pagination -->
        <?php

        $wp_query = $custom_query;

        if (function_exists("pagination")) {

            the_posts_pagination( array(
                'mid_size' => 0,
                'prev_text' => __( 'PREV', 'textdomain' ),
                'next_text' => __( 'NEXT', 'textdomain' ),
                'screen_reader_text' => __( '', 'textdomain' ),
            ) );

        } ?>

    <hr class="blog_hr">


<!-- Recent and Popular posts -->
    <!-- nav bar -->
    <ul id="blog-recent-popular-buttons">
        <li id="blog-right-button-resent" class="blog-right-button-active">RECENT</li>
        <li id="blog-right-button-popular">POPULAR</li>
    </ul>

    <!-- list of RESENT posts (4th newest posts) -->
    <div id="blog-resent-publications">
        <?php
        $paged = "paged";
        if($cleanCategoryName == "all" || $cleanCategoryName == "ALL"){
            // get all posts
            $args = array(
                'posts_per_page' => 3,
                'paged' => $paged
            );
        }else{
            // get just posts from the category
            $args = array(
                'posts_per_page' => 3,
                'paged' => $paged,
                'cat' => $categoryOfPage
            );
        }
        $custom_query = new WP_Query( $args );
        set_post_thumbnail_size( 120, 120, true );

        while($custom_query->have_posts()) {
            $custom_query->the_post();
            $thumbnailDescription = get_post_meta($post->ID, 'thumbnail_description', true);
            $firstCharsFromDesc = substr($thumbnailDescription, 0, 80);
            ?>
            <div class="blog-resent-element">
                <a href="<?php echo $siteURL."/mobile/mobile-post?id=".$post->ID ?>" class="blog-resent-thumbnail"  style="background-image: url('<?php bloginfo('template_url'); ?>/images/blog/not.png')">
                    <?php the_post_thumbnail( 'thumbnail' ); ?>
                </a>
                <div class="blog-resent-allTexts">
                    <a href="<?php echo $siteURL."/mobile/mobile-post?id=".$post->ID ?>"><?php the_title(); ?></a>
                    <p class="blog-resent-description"><?php echo $firstCharsFromDesc; ?></p>
                    <span>
                        <span class="blog-big_article_category"><?php echo get_the_category($post->ID)[0]->name; ?></span>
                        <i class="demo-icon icon-calendar">&#xe803;</i>
                        <span class="blog-big_article_date"><?php the_time('F j, Y'); ?></span>
                    </span>
                </div>
            </div>
        <?php } ?>
    </div>

    <!-- list of POPULAR posts -->
    <div id="blog-popular-publications">

        <?php while($custom_query->have_posts()) {
            $custom_query->the_post();
            $showInPopular = get_post_meta($post->ID, 'popular', true);
            if($showInPopular == "true"){
                $thumbnailDescription = get_post_meta($post->ID, 'thumbnail_description', true);
                $firstCharsFromDesc = substr($thumbnailDescription, 0, 80);
                ?>
                <div class="blog-resent-element">
                    <a href="<?php echo $siteURL."/mobile/mobile-post?id=".$post->ID ?>" class="blog-resent-thumbnail"  style="background-image: url('<?php bloginfo('template_url'); ?>/images/blog/not.png')">
                        <?php the_post_thumbnail( 'thumbnail' ); ?>
                    </a>
                    <div class="blog-resent-allTexts">
                        <a href="<?php echo $siteURL."/mobile/mobile-post?id=".$post->ID ?>"><?php the_title(); ?></a>
                        <p class="blog-resent-description"><?php echo $firstCharsFromDesc; ?></p>
                        <span class="blog-info-row">
                            <span class="blog-big_article_category"><?php echo get_the_category($post->ID)[0]->name; ?></span>
                            <i class="demo-icon icon-calendar">&#xe803;</i>
                            <span class="blog-big_article_date"><?php the_time('F j, Y'); ?></span>
                        </span>
                    </div>
                </div>
            <?php }   }?>
    </div>

    <!-- subscribe field -->
    <p id="blog-subscribe">SUBSCRIBE</p>
    <div id="blog-subscribe-input">
        <input type="text" placeholder="Your Email Address"/>
        <div class="icon" data-icon="p"></div>
    </div>



<!-- footer -->
<?php

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();
