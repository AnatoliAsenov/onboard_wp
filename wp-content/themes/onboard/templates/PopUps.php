<?php

/**
 * Created by Onboard
 * User: Anatoli Asenov
 */
class PopUps
{

    public function __construct(){}

    public function printHTML()
    {
        ?>
        <!-- pop up -->
        <div id="thePopUp">

            <i class="fa fa-plus-circle how-can-we-help-close" aria-hidden="true"></i>

            <!-- SharpSpring Form for Contact Us New  -->
            <script type="text/javascript">
                var ss_form = {'account': 'MzawMDEzMjAzBwA', 'formID': 'M7Q0MTZISzLWNTI1T9Q1MbYw1E1MSksFsiyNUo2STMxNzJIB'};
                ss_form.width = '50%';
                ss_form.height = '30%';
                ss_form.domain = 'app-3QNB9GWLJ6.marketingautomation.services';
                // ss_form.hidden = {'Company': 'Anon'}; // Modify this for sending hidden variables, or overriding values
            </script>
            <script type="text/javascript" src="https://koi-3QNB9GWLJ6.marketingautomation.services/client/form.js?ver=1.1.1"></script>


        </div>

        <?php
    }


    public function oldForm(){
?>
<!-- pop up -->
<div id="thePopUp">

    <i class="fa fa-plus-circle how-can-we-help-close" aria-hidden="true"></i>

    <div id="thankYouPopUp">
        <p>Thank you!</p>
        <p>We will connect with you ASAP.</p>
    </div>

    <!--form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads1239484000011711043 method='POST' accept-charset='UTF-8'-->
    <form action="" method='POST' id="onboardForm" accept-charset='UTF-8'>

        <!-- Do not remove this code.
        <input type='text' style='display:none;' name='xnQsjsdp' value='4d75f250d006eeddcf8d7602c119c5c612b473a9775fc1518fc3092147ffa642'/>
        <input type='hidden' name='zc_gad' id='zc_gad' value=''/>
        <input type='text' style='display:none;' name='xmIwtLD' value='e3945a2a9ad283d7b9d1a3c717b39e4561d9fa0398a899e14bcb8009fbb2040a'/>
        <input type='text' style='display:none;'  name='actionType' value='TGVhZHM='/>
        <input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;onboardcrm.com&#x2f;home' />
        Do not remove this code. -->
        <input type='text' style='display:none;' name='device' value='desktop' />

        <table>
            <tr>
                <th colspan="3">
                    <h1 id="contactUsFormTitle">Contact Us</h1>
                </th>
            </tr>
            <tr>
                <th style="width:290px;"><input type='text' maxlength='40' name='First Name' placeholder="First name*" required="true"/></th>
                <th class="contacts_middle_buffer"></th>
                <th style="width:290px;"><input type='text' maxlength='80' name='Last Name' placeholder="Last name*" required="true"/></th>
            </tr>
            <tr>
                <th><input type='email' maxlength='100' name='Email' placeholder="Email*" required="true"/></th>
                <th class="contacts_middle_buffer"></th>
                <th>
                    <select name='LEADCF3'>
                        <option value='-None-'>Function</option>
                        <option value='Executive'>Executive</option>
                        <option value='Marketing'>Marketing</option>
                        <option value='Sales'>Sales</option>
                        <option value='CRM'>CRM</option>
                        <option value='Customer Service'>Customer Service</option>
                        <option value='Technical'>Technical</option>
                        <option value='Analyst'>Analyst</option>
                        <option value='Consultant'>Consultant</option>
                        <option value='Office Manager'>Office Manager</option>
                        <option value='Secretary'>Secretary</option>
                        <option value='Staff'>Staff</option>
                    </select>
                </th>
            </tr>
            <tr>
                <th><input type='text' maxlength='30' name='Phone' placeholder="Phone" /></th>
                <th class="contacts_middle_buffer"></th>
                <th><input type='text' maxlength='100' name='Company' placeholder="Company*" required="true"/></th>
            </tr>
            <tr>
                <th>
                    <select name='Industry'>
                        <option value='-None-'>Industry</option>
                        <option value='Finance'>Finance</option>
                        <option value='CRM'>CRM</option>
                        <option value='Data/Telecom OEM'>Data/Telecom OEM</option>
                        <option value='Technology'>Technology</option>
                        <option value='Government'>Government</option>
                        <option value='MSP (Management Service Provider)'>MSP (Management Service Provider)</option>
                        <option value='Not For Profit'>Not For Profit</option>
                        <option value='Consulting'>Consulting</option>
                        <option value='Manufacturing'>Manufacturing</option>
                        <option value='Transportation'>Transportation</option>
                        <option value='Telecommunications'>Telecommunications</option>
                        <option value='Healthcare'>Healthcare</option>
                        <option value='Hospitality'>Hospitality</option>
                        <option value='Insurance'>Insurance</option>
                        <option value='Utilities'>Utilities</option>
                        <option value='Other'>Other</option>
                    </select>
                </th>
                <th class="contacts_middle_buffer"></th>
                <th><input type='text' maxlength='30' name='City' placeholder="City" /></th>
            </tr>
            <tr>
                <th>
                    <select name='LEADCF2' required="true">
                        <option value='-None-'>Country*</option>
                        <option value='Abkhazia'>Abkhazia</option>
                        <option value='Afghanistan'>Afghanistan</option>
                        <option value='Aland'>Aland</option>
                        <option value='Albania'>Albania</option>
                        <option value='Algeria'>Algeria</option>
                        <option value='American Samoa'>American Samoa</option>
                        <option value='Andorra'>Andorra</option>
                        <option value='Angola'>Angola</option>
                        <option value='Anguilla'>Anguilla</option>
                        <option value='Antigua and Barbuda'>Antigua and Barbuda</option>
                        <option value='Argentina'>Argentina</option>
                        <option value='Armenia'>Armenia</option>
                        <option value='Aruba'>Aruba</option>
                        <option value='Ascension'>Ascension</option>
                        <option value='Ashmore and Cartier Islands'>Ashmore and Cartier Islands</option>
                        <option value='Australia'>Australia</option>
                        <option value='Australian Antarctic Territory'>Australian Antarctic Territory</option>
                        <option value='Austria'>Austria</option>
                        <option value='Azerbaijan'>Azerbaijan</option>
                        <option value='Bahamas, The'>Bahamas, The</option>
                        <option value='Bahrain'>Bahrain</option>
                        <option value='Baker Island'>Baker Island</option>
                        <option value='Bangladesh'>Bangladesh</option>
                        <option value='Barbados'>Barbados</option>
                        <option value='Belarus'>Belarus</option>
                        <option value='Belgium'>Belgium</option>
                        <option value='Belize'>Belize</option>
                        <option value='Benin'>Benin</option>
                        <option value='Bermuda'>Bermuda</option>
                        <option value='Bhutan'>Bhutan</option>
                        <option value='Bolivia'>Bolivia</option>
                        <option value='Bosnia and Herzegovina'>Bosnia and Herzegovina</option>
                        <option value='Botswana'>Botswana</option>
                        <option value='Bouvet Island'>Bouvet Island</option>
                        <option value='Brazil'>Brazil</option>
                        <option value='British Antarctic Territory'>British Antarctic Territory</option>
                        <option value='British Indian Ocean Territory'>British Indian Ocean Territory</option>
                        <option value='British Sovereign Base Areas'>British Sovereign Base Areas</option>
                        <option value='British Virgin Islands'>British Virgin Islands</option>
                        <option value='Brunei'>Brunei</option>
                        <option value='Bulgaria'>Bulgaria</option>
                        <option value='Burkina Faso'>Burkina Faso</option>
                        <option value='Burundi'>Burundi</option>
                        <option value='Cambodia'>Cambodia</option>
                        <option value='Cameroon'>Cameroon</option>
                        <option value='Canada'>Canada</option>
                        <option value='Cape Verde'>Cape Verde</option>
                        <option value='Cayman Islands'>Cayman Islands</option>
                        <option value='Central African Republic'>Central African Republic</option>
                        <option value='Chad'>Chad</option>
                        <option value='Chile'>Chile</option>
                        <option value='China, People&quot;s Republic of'>China, People's Republic of</option>
                        <option value='China, Republic of (Taiwan)'>China, Republic of (Taiwan)</option>
                        <option value='Christmas Island'>Christmas Island</option>
                        <option value='Clipperton Island'>Clipperton Island</option>
                        <option value='Cocos (Keeling) Islands'>Cocos (Keeling) Islands</option>
                        <option value='Colombia'>Colombia</option>
                        <option value='Comoros'>Comoros</option>
                        <option value='Congo, (Congo A� Brazzaville)'>Congo, (Congo A� Brazzaville)</option>
                        <option value='Congo, (Congo A� Kinshasa)'>Congo, (Congo A� Kinshasa)</option>
                        <option value='Cook Islands'>Cook Islands</option>
                        <option value='Coral Sea Islands'>Coral Sea Islands</option>
                        <option value='Costa Rica'>Costa Rica</option>
                        <option value='Cote d&quot;Ivoire (Ivory Coast)'>Cote d'Ivoire (Ivory Coast)</option>
                        <option value='Croatia'>Croatia</option>
                        <option value='Cuba'>Cuba</option>
                        <option value='Cyprus'>Cyprus</option>
                        <option value='Czech Republic'>Czech Republic</option>
                        <option value='Denmark'>Denmark</option>
                        <option value='Djibouti'>Djibouti</option>
                        <option value='Dominica'>Dominica</option>
                        <option value='Dominican Republic'>Dominican Republic</option>
                        <option value='Ecuador'>Ecuador</option>
                        <option value='Egypt'>Egypt</option>
                        <option value='El Salvador'>El Salvador</option>
                        <option value='Equatorial Guinea'>Equatorial Guinea</option>
                        <option value='Eritrea'>Eritrea</option>
                        <option value='Estonia'>Estonia</option>
                        <option value='Ethiopia'>Ethiopia</option>
                        <option value='Falkland Islands (Islas Malvinas)'>Falkland Islands (Islas Malvinas)</option>
                        <option value='Faroe Islands'>Faroe Islands</option>
                        <option value='Fiji'>Fiji</option>
                        <option value='Finland'>Finland</option>
                        <option value='France'>France</option>
                        <option value='French Guiana'>French Guiana</option>
                        <option value='French Polynesia'>French Polynesia</option>
                        <option value='French Southern and Antarctic Lands'>French Southern and Antarctic Lands</option>
                        <option value='Gabon'>Gabon</option>
                        <option value='Gambia, The'>Gambia, The</option>
                        <option value='Georgia'>Georgia</option>
                        <option value='Germany'>Germany</option>
                        <option value='Ghana'>Ghana</option>
                        <option value='Gibraltar'>Gibraltar</option>
                        <option value='Greece'>Greece</option>
                        <option value='Greenland'>Greenland</option>
                        <option value='Grenada'>Grenada</option>
                        <option value='Guadeloupe'>Guadeloupe</option>
                        <option value='Guam'>Guam</option>
                        <option value='Guatemala'>Guatemala</option>
                        <option value='Guernsey'>Guernsey</option>
                        <option value='Guinea'>Guinea</option>
                        <option value='Guinea-Bissau'>Guinea-Bissau</option>
                        <option value='Guyana'>Guyana</option>
                        <option value='Haiti'>Haiti</option>
                        <option value='Heard Island and McDonald Islands'>Heard Island and McDonald Islands</option>
                        <option value='Honduras'>Honduras</option>
                        <option value='Hong Kong'>Hong Kong</option>
                        <option value='Howland Island'>Howland Island</option>
                        <option value='Hungary'>Hungary</option>
                        <option value='Iceland'>Iceland</option>
                        <option value='India'>India</option>
                        <option value='Indonesia'>Indonesia</option>
                        <option value='Iran'>Iran</option>
                        <option value='Iraq'>Iraq</option>
                        <option value='Ireland'>Ireland</option>
                        <option value='Isle of Man'>Isle of Man</option>
                        <option value='Israel'>Israel</option>
                        <option value='Italy'>Italy</option>
                        <option value='Jamaica'>Jamaica</option>
                        <option value='Japan'>Japan</option>
                        <option value='Jarvis Island'>Jarvis Island</option>
                        <option value='Jersey'>Jersey</option>
                        <option value='Johnston Atoll'>Johnston Atoll</option>
                        <option value='Jordan'>Jordan</option>
                        <option value='Kazakhstan'>Kazakhstan</option>
                        <option value='Kenya'>Kenya</option>
                        <option value='Kingman Reef'>Kingman Reef</option>
                        <option value='Kiribati'>Kiribati</option>
                        <option value='Korea, North'>Korea, North</option>
                        <option value='Korea, South'>Korea, South</option>
                        <option value='Kuwait'>Kuwait</option>
                        <option value='Kyrgyzstan'>Kyrgyzstan</option>
                        <option value='Laos'>Laos</option>
                        <option value='Latvia'>Latvia</option>
                        <option value='Lebanon'>Lebanon</option>
                        <option value='Lesotho'>Lesotho</option>
                        <option value='Liberia'>Liberia</option>
                        <option value='Libya'>Libya</option>
                        <option value='Liechtenstein'>Liechtenstein</option>
                        <option value='Lithuania'>Lithuania</option>
                        <option value='Luxembourg'>Luxembourg</option>
                        <option value='Macau'>Macau</option>
                        <option value='Macedonia'>Macedonia</option>
                        <option value='Madagascar'>Madagascar</option>
                        <option value='Malawi'>Malawi</option>
                        <option value='Malaysia'>Malaysia</option>
                        <option value='Maldives'>Maldives</option>
                        <option value='Mali'>Mali</option>
                        <option value='Malta'>Malta</option>
                        <option value='Marshall Islands'>Marshall Islands</option>
                        <option value='Martinique'>Martinique</option>
                        <option value='Mauritania'>Mauritania</option>
                        <option value='Mauritius'>Mauritius</option>
                        <option value='Mayotte'>Mayotte</option>
                        <option value='Mexico'>Mexico</option>
                        <option value='Micronesia'>Micronesia</option>
                        <option value='Midway Islands'>Midway Islands</option>
                        <option value='Moldova'>Moldova</option>
                        <option value='Monaco'>Monaco</option>
                        <option value='Mongolia'>Mongolia</option>
                        <option value='Montenegro'>Montenegro</option>
                        <option value='Montserrat'>Montserrat</option>
                        <option value='Morocco'>Morocco</option>
                        <option value='Mozambique'>Mozambique</option>
                        <option value='Myanmar (Burma)'>Myanmar (Burma)</option>
                        <option value='Nagorno-Karabakh'>Nagorno-Karabakh</option>
                        <option value='Namibia'>Namibia</option>
                        <option value='Nauru'>Nauru</option>
                        <option value='Navassa Island'>Navassa Island</option>
                        <option value='Nepal'>Nepal</option>
                        <option value='Netherlands'>Netherlands</option>
                        <option value='Netherlands Antilles'>Netherlands Antilles</option>
                        <option value='New Caledonia'>New Caledonia</option>
                        <option value='New Zealand'>New Zealand</option>
                        <option value='Nicaragua'>Nicaragua</option>
                        <option value='Niger'>Niger</option>
                        <option value='Nigeria'>Nigeria</option>
                        <option value='Niue'>Niue</option>
                        <option value='Norfolk Island'>Norfolk Island</option>
                        <option value='Northern Cyprus'>Northern Cyprus</option>
                        <option value='Northern Mariana Islands'>Northern Mariana Islands</option>
                        <option value='Norway'>Norway</option>
                        <option value='Oman'>Oman</option>
                        <option value='Pakistan'>Pakistan</option>
                        <option value='Palau'>Palau</option>
                        <option value='Palmyra Atoll'>Palmyra Atoll</option>
                        <option value='Panama'>Panama</option>
                        <option value='Papua New Guinea'>Papua New Guinea</option>
                        <option value='Paraguay'>Paraguay</option>
                        <option value='Peru'>Peru</option>
                        <option value='Peter I Island'>Peter I Island</option>
                        <option value='Philippines'>Philippines</option>
                        <option value='Pitcairn Islands'>Pitcairn Islands</option>
                        <option value='Poland'>Poland</option>
                        <option value='Portugal'>Portugal</option>
                        <option value='Pridnestrovie (Transnistria)'>Pridnestrovie (Transnistria)</option>
                        <option value='Puerto Rico'>Puerto Rico</option>
                        <option value='Qatar'>Qatar</option>
                        <option value='Queen Maud Land'>Queen Maud Land</option>
                        <option value='Reunion'>Reunion</option>
                        <option value='Romania'>Romania</option>
                        <option value='Ross Dependency'>Ross Dependency</option>
                        <option value='Russia'>Russia</option>
                        <option value='Rwanda'>Rwanda</option>
                        <option value='Saint Barthelemy'>Saint Barthelemy</option>
                        <option value='Saint Helena'>Saint Helena</option>
                        <option value='Saint Kitts and Nevis'>Saint Kitts and Nevis</option>
                        <option value='Saint Lucia'>Saint Lucia</option>
                        <option value='Saint Martin'>Saint Martin</option>
                        <option value='Saint Pierre and Miquelon'>Saint Pierre and Miquelon</option>
                        <option value='Saint Vincent and the Grenadines'>Saint Vincent and the Grenadines</option>
                        <option value='Samoa'>Samoa</option>
                        <option value='San Marino'>San Marino</option>
                        <option value='Sao Tome and Principe'>Sao Tome and Principe</option>
                        <option value='Saudi Arabia'>Saudi Arabia</option>
                        <option value='Senegal'>Senegal</option>
                        <option value='Serbia'>Serbia</option>
                        <option value='Seychelles'>Seychelles</option>
                        <option value='Sierra Leone'>Sierra Leone</option>
                        <option value='Singapore'>Singapore</option>
                        <option value='Slovakia'>Slovakia</option>
                        <option value='Slovenia'>Slovenia</option>
                        <option value='Solomon Islands'>Solomon Islands</option>
                        <option value='Somalia'>Somalia</option>
                        <option value='Somaliland'>Somaliland</option>
                        <option value='South Africa'>South Africa</option>
                        <option value='South Georgia & South Sandwich Islands'>South Georgia & South Sandwich Islands</option>
                        <option value='South Ossetia'>South Ossetia</option>
                        <option value='Spain'>Spain</option>
                        <option value='Sri Lanka'>Sri Lanka</option>
                        <option value='Sudan'>Sudan</option>
                        <option value='Suriname'>Suriname</option>
                        <option value='Svalbard'>Svalbard</option>
                        <option value='Swaziland'>Swaziland</option>
                        <option value='Sweden'>Sweden</option>
                        <option value='Switzerland'>Switzerland</option>
                        <option value='Syria'>Syria</option>
                        <option value='Tajikistan'>Tajikistan</option>
                        <option value='Tanzania'>Tanzania</option>
                        <option value='Thailand'>Thailand</option>
                        <option value='Timor-Leste (East Timor)'>Timor-Leste (East Timor)</option>
                        <option value='Togo'>Togo</option>
                        <option value='Tokelau'>Tokelau</option>
                        <option value='Tonga'>Tonga</option>
                        <option value='Trinidad and Tobago'>Trinidad and Tobago</option>
                        <option value='Tristan da Cunha'>Tristan da Cunha</option>
                        <option value='Tunisia'>Tunisia</option>
                        <option value='Turkey'>Turkey</option>
                        <option value='Turkmenistan'>Turkmenistan</option>
                        <option value='Turks and Caicos Islands'>Turks and Caicos Islands</option>
                        <option value='Tuvalu'>Tuvalu</option>
                        <option value='U.S. Virgin Islands'>U.S. Virgin Islands</option>
                        <option value='Uganda'>Uganda</option>
                        <option value='Ukraine'>Ukraine</option>
                        <option value='United Arab Emirates'>United Arab Emirates</option>
                        <option value='United Kingdom'>United Kingdom</option>
                        <option value='United States'>United States</option>
                        <option value='Uruguay'>Uruguay</option>
                        <option value='Uzbekistan'>Uzbekistan</option>
                        <option value='Vanuatu'>Vanuatu</option>
                        <option value='Vatican City'>Vatican City</option>
                        <option value='Venezuela'>Venezuela</option>
                        <option value='Vietnam'>Vietnam</option>
                        <option value='Wake Island'>Wake Island</option>
                        <option value='Wallis and Futuna'>Wallis and Futuna</option>
                        <option value='Yemen'>Yemen</option>
                        <option value='Zambia'>Zambia</option>
                        <option value='Zimbabwe'>Zimbabwe</option>
                    </select>
                </th>
                <th class="contacts_middle_buffer"></th>
                <th>
                    <select name='LEADCF4' required="true">
                        <option value='-None-'>Request type*</option>
                        <option value='General Information'>General Information</option>
                        <option value='Quote for Services'>Quote for Services</option>
                        <option value='Partnership'>Partnership</option>
                        <option value='Job Application'>Job Application</option>
                        <option value='Other'>Other</option>
                    </select>
                </th>
            </tr>
            <tr>
                <th colspan="3" style="padding-top:10px;">
                    <textarea name='Description' maxlength='1000' rows="3" placeholder="Description"></textarea>
                </th>
            </tr>
            <!--tr>
                <th>
                    <input value="Upload File" type="button" id="theFileBtn" />
                    <span id="theFileText">Max file size is 20MB</span>
                    <input type='file' name='theFile' id="theFile" />
                    <input type='file' name='theFile' id='theFile' multiple/>
                </th>
                <th class="contacts_middle_buffer"></th>
                <th style="vertical-align:bottom;">
                    <input type='text' maxlength="80" name="enterdigest" placeholder="Captcha*" autocomplete="off" id="captchaInput" />
                    <span id="captchaSpan">=</span>
                    <img id="imgid" src='https://crm.zoho.com/crm/CaptchaServlet?formId=e3945a2a9ad283d7b9d1a3c717b39e45cf2efec7e8adb945f316c629fce93d91&grpid=4d75f250d006eeddcf8d7602c119c5c612b473a9775fc1518fc3092147ffa642' alt="validation image" />
                    <i class="fa fa-refresh" aria-hidden="true" id="captchaRefresh" onclick="reloadImg();"></i>
                </th>
            </tr-->
            <tr>
                <th colspan="2" style="padding-top:10px;">
                    <div class="g-recaptcha" data-sitekey="6LewPSsUAAAAAM25-WIDM6x8fHal3VyYyLNOiUPr"></div>
                </th>
                <th style="vertical-align:top;">
                    <input type="submit" style="display:none;" id="contact_us_submit_btn"/>
                    <div id="form_submit_btn">SUBMIT</div>
                </th>
            </tr>
        </table>
    </form>
    <script>

        /* Do not remove this code. */
        function reloadImg() {
            if(document.getElementById('imgid').src.indexOf('&d') !== -1 ) {
                document.getElementById('imgid').src=document.getElementById('imgid').src.substring(0,document.getElementById('imgid').src.indexOf('&d'))+'&d'+new Date().getTime();
            }  else {
                document.getElementById('imgid').src = document.getElementById('imgid').src+'&d'+new Date().getTime();
            }
        }

        function validateFileUpload(){
            var uploadedFiles = document.getElementById('theFile');
            var totalFileSize =0;
            if(uploadedFiles.files.length >3){
                alert('You can upload a maximum of three files at a time.');
                return false;
            }
            if ('files' in uploadedFiles) {
                if (uploadedFiles.files.length != 0) {
                    for (var i = 0; i < uploadedFiles.files.length; i++) {
                        var file = uploadedFiles.files[i];
                        if ('size' in file) {
                            totalFileSize = totalFileSize + file.size;
                        }
                    }
                    if(totalFileSize > 20971520){
                        alert('Total file(s) size should not exceed 20MB.');
                        return false;
                    }
                }
            }
        }
    </script>
    <!-- contact form end -->


<!-- What can we do FORM -->
<!--    <form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads1239484000010844070 method='POST' id="how-can-we-help-form" accept-charset='UTF-8'>-->
<!--        <input type='text' style='display:none;' name='xnQsjsdp' value='4d75f250d006eeddcf8d7602c119c5c612b473a9775fc1518fc3092147ffa642'/>-->
<!--        <input type='hidden' name='zc_gad' id='zc_gad' value=''/>-->
<!--        <input type='text' style='display:none;' name='xmIwtLD' value='e3945a2a9ad283d7b9d1a3c717b39e457d07bf9d67d76d4f4a0b2efa703c213a'/>-->
<!--        <input type='text' style='display:none;'  name='actionType' value='TGVhZHM='/>-->
<!--        <input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;onboardcrm.com&#x2f;case-studies&#x2f;' />-->
<!--        <!-- Do not remove this code. -->-->
<!--        <input type='text' style='display:none;' name='LEADCF9' value='HowCanWeHelp form'>-->
<!---->
<!--        <table id="how-can-we-help-tab">-->
<!--            <tr>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--                <th><p class="how-can-we-help-tit">How can we help you?</p></th>-->
<!--                <th></th>-->
<!--                <th></th>-->
<!--                <th class="how-can-we-help-buffer">-->
<!--                    <i class="fa fa-plus-circle how-can-we-help-close" aria-hidden="true"></i>-->
<!--                </th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--                <th colspan="3">-->
<!--                    <div class="how_can_inputs">-->
<!--                        <input type="text" placeholder="Your name" name='Last Name' id="how-can-we-help-fullname" />-->
<!--                        <input type="text" placeholder="Your email" name='Email' />-->
<!--                    </div>-->
<!--                    <div data-icon="n" class="icon" id="how_can_paperplane"></div>-->
<!--                    <div class="how_can_inputs">-->
<!--                        <input type="text" placeholder="Company" name='Company' />-->
<!--                        <input type="text" placeholder="Phone" name='Phone' />-->
<!--                    </div>-->
<!--                </th>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--                <th colspan="3"><textarea name='Description' placeholder="Hello, my company is interested of"></textarea></th>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th colspan="5">-->
<!--                    <div id="how-can-we-help-send">SEND</div>-->
<!--                    <input style='display:none;' type='submit' id="how-can-we-help-submit" />-->
<!--                </th>-->
<!--            </tr>-->
<!--        </table>-->
<!--    </form>-->


<!-- Download PDF -->
<!--    <form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads1239484000010856034 method='POST' accept-charset='UTF-8' id="download-pdf-form">-->
<!--        <input type='text' style='display:none;' name='xnQsjsdp' value='4d75f250d006eeddcf8d7602c119c5c612b473a9775fc1518fc3092147ffa642'/>-->
<!--        <input type='hidden' name='zc_gad' id='zc_gad' value=''/>-->
<!--        <input type='text' style='display:none;' name='xmIwtLD' value='e3945a2a9ad283d7b9d1a3c717b39e45787a029ec2f06f50612841908df3a499'/>-->
<!--        <input type='text' style='display:none;'  name='actionType' value='TGVhZHM='/>-->
<!--        <input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;onboardcrm.com&#x2f;case-studies&#x2f;' />-->
<!---->
<!--        <table id="download-pdf-tab">-->
<!--            <tr>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--                <th colspan="2"><p class="how-can-we-help-tit">Download full version as pdf</p></th>-->
<!--                <th></th>-->
<!--                <th class="how-can-we-help-buffer">-->
<!--                    <i class="fa fa-plus-circle how-can-we-help-close" aria-hidden="true" ></i>-->
<!--                </th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--                <th colspan="3"><p id="download-pdf-subtitle">Please, enter your details in order to download the full version of this case study.</p></th>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--                <th>-->
<!--                    <input type="text" name='Last Name' placeholder="Your name" id="download-pdf-form-fullname" />-->
<!--                    <input type="text" placeholder="Your email" name='Email' />-->
<!--                </th>-->
<!--                <th><div data-icon="n" class="icon" id="download_pdf_paperplane"></div></th>-->
<!--                <th>-->
<!--                    <input type="text" placeholder="Company" name='Company' />-->
<!--                    <input type="text" placeholder="Phone" name='Phone' />-->
<!--                </th>-->
<!--                <th class="how-can-we-help-buffer"></th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th colspan="5">-->
<!--                    <div id="download-pdf-download">DOWNLOAD</div>-->
<!--                    <input style='display:none;' type='submit' id="download-pdf-submit" />-->
<!--                </th>-->
<!--            </tr>-->
<!--        </table>-->
<!--    </form>-->


<!-- Let us call you -->
<!--    <form action="https://crm.zoho.com/crm/WebToLeadForm" name=WebToLeads1239484000010844013 method='POST' accept-charset='UTF-8' id="let_us_call_you_form">-->
<!--        <input type='text' style='display:none;' name='xnQsjsdp' value='4d75f250d006eeddcf8d7602c119c5c612b473a9775fc1518fc3092147ffa642'/>-->
<!--        <input type='hidden' name='zc_gad' id='zc_gad' value=''/>-->
<!--        <input type='text' style='display:none;' name='xmIwtLD' value='e3945a2a9ad283d7b9d1a3c717b39e45aa7a45608b3cd6896de2a8bb99a36627'/>-->
<!--        <input type='text' style='display:none;' name='actionType' value='TGVhZHM='/>-->
<!--        <input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;onboardcrm.com&#x2f;' />-->
<!--        <input type='text' style='display:none;' name='Last Name' value="LetUsCallYou form" />-->
<!---->
<!--        <table id="let_us_call_you_tab">-->
<!--            <tbody>-->
<!--            <tr>-->
<!--                <th class="let_us_call_you_buffer"></th>-->
<!--                <th><p id="let_us_call_you_tit">Let us call you</p></th>-->
<!--                <th></th>-->
<!--                <th class="let_us_call_you_buffer">-->
<!--                    <i class="fa fa-plus-circle how-can-we-help-close" aria-hidden="true" ></i>-->
<!--                </th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th class="let_us_call_you_buffer"></th>-->
<!--                <th>-->
<!--                    <div id="letuscallyou-inputs">-->
<!--                        <input type="text" placeholder="Your phone" name='Phone' />-->
<!--                        <input type="text" placeholder="Your email" name='Email' />-->
<!--                    </div>-->
<!--                    <div data-icon="n" class="icon" id="letuscallyou-paperplane"></div>-->
<!--                </th>-->
<!--                <th class="let_us_call_you_buffer"></th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <th class="let_us_call_you_buffer"></th>-->
<!--                <th colspan="2">-->
<!--                    <textarea name='Description'></textarea>-->
<!--                </th>-->
<!--                <th class="let_us_call_you_buffer"></th>-->
<!--            </tr>-->
<!--            <tr>-->
<!--                <input style='display:none;' type='submit' value='Submit' id="let-us-call-you-hidden-send" />-->
<!--                <th colspan="4"><div id="let-us-call-you-send">SEND</div></th>-->
<!--            </tr>-->
<!--            </tbody>-->
<!--        </table>-->
<!--    </form>-->

</div>

<?php
    }
}