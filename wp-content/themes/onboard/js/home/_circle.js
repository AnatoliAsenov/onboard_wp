
if($("#whatisthispage").text() == "home") {
    
    function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
      var angleInRadians = (angleInDegrees - 90) * Math.PI / 180;

      return {
        x: centerX + (radius * Math.cos(angleInRadians)),
        y: centerY + (radius * Math.sin(angleInRadians))
      };
    }

    function drawArc(x, y, radius, startAngle, endAngle){
      var start = polarToCartesian(x, y, radius, endAngle),
          end = polarToCartesian(x, y, radius, startAngle),
          arcSweep = endAngle - startAngle <= 180 ? "0" : "1",
          d = [
            "M", start.x, start.y,
            "A", radius, radius, 0, arcSweep, 0, end.x, end.y
          ].join(" ");
      return d;
    }

    function textArc(x, y, radius, startAngle, endAngle, clockwise) {
      var start = polarToCartesian(x, y, radius, startAngle),
          end = polarToCartesian(x, y, radius, endAngle),
          arcSweep = endAngle - startAngle <= 180 ? "0" : "1",
          d = [
            "M", start.x, start.y,
            "A", radius, radius, 0, arcSweep, 1, end.x, end.y
          ].join(" ");
      return d;
    }

    $(function() {
      var $svg = $('svg'),
          $defs = $('defs'),
          $path = $('svg > path'),
          $text_path = null,
          x = ($('svg').width() / 2.2),
          y = x,
          r = x,
          total = $path.length,
          degree_padding = 5,
          sector = (360 / total),
          active_index = 0;

      $.each($path, function(i, el) {
        var $p = $(el),
            start_stroke_angle = (i * sector),
            end_stroke_angle = ((start_stroke_angle - degree_padding) + sector),
            start_text_angle = (sector * i),
            end_text_angle =  (sector * (i + 1)),
            id = (i + 1),
            label = $p.attr('data-label');

        // Set id and draw path
        $p.attr('d', drawArc(x, y, r, start_stroke_angle, end_stroke_angle));

        // Set defs
        var def_path = document.createElementNS("http://www.w3.org/2000/svg", "path");
        def_path.setAttributeNS(null, 'id', 'text-path-' + id);
        $(def_path).attr('d', textArc(x, y, (r + 20), start_text_angle, end_text_angle));
        $defs.append($(def_path));

        // Set out circle
        var def_pa = document.createElementNS("http://www.w3.org/2000/svg", "path");
        def_pa.setAttributeNS(null, 'id', 'text-path-' + id);
        $(def_pa).attr('d', textArc(x, y, (r + 52), start_text_angle, end_text_angle));
        $svg.append($(def_pa));

        // Set <text>
        var text = document.createElementNS("http://www.w3.org/2000/svg", "text");

        // Set <textPath>
        var text_path = document.createElementNS("http://www.w3.org/2000/svg", "textPath");
        text_path.setAttributeNS(null, 'startOffset', '50%');
        text_path.setAttributeNS(null, "text-anchor", "middle");
        text_path.setAttributeNS(null, 'dominant-baseline', 'center');
        text_path.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", "#text-path-" + id);

        var data = document.createTextNode(label);

        text_path.appendChild(data);
        $svg.append($(text).append(text_path));
      });

      var $text = $('text');

    });

    //--------------------- loading animation -------------------------
    var intro_animation = function(i, setIntervalid){
        var text = $('text');
        var path = $('svg > path');

        if(i == 5){
            clearInterval(setIntervalid);
            collectCircleElements();
        }else{
            path.eq(i).addClass('existing');
            path.eq(i).addClass("active");
            text.eq(i).attr('class', 'active');
            if(i != 0){
                path.eq(i - 1).removeClass("active").addClass("complete");
            }
        }

    };

    var wheelActivationFunc = function(){
        var count = 0;
        var setIntervalID = setInterval(function(){
            intro_animation(count, setIntervalID);
            count++;
        }, 400);
    };
    //===========================================================================

    //----------- grab all elements of the circle and attach events -------------
    var collectCircleElements = function(){

        var elementsLinks = {
            WorldwideMarketEntry: "/what-we-do/worldwide-market-entry",
            DigitalMarketingServices: "/what-we-do/digital-marketing-services",
            DatabaseServices: "/what-we-do/database-services",
            B2BMarketingSales: "/what-we-do/b2b-marketing-and-sales",
            BusinessProcessEnablement: "/what-we-do/business-process-enablement"
        };

        var elementsDescriptions = {
            WorldwideMarketEntry: ["<p>GO WHERE OTHERS DON’T…</p><p>and make it worthwhile</p>", "Market Research - Entry Strategy - Staffing & Payrolling - Project Management"],
            DigitalMarketingServices: ["<p>ROCK YOUR DIGITAL</p>", "Social Media Listening - Digital PR & Marketing - Reputation Management - Web Design & SEO"],
            DatabaseServices: ["<p>UP CLOSE AND PERSONAL: DO YOU</p><p>KNOW ENOUGH ABOUT YOUR</p><p>CUSTOMERS TO MAKE A DIFFERENCE?</p>", "Analytics - Consolidation - Enrichment - Quality - Integration"],
            B2BMarketingSales: ["<p>We Do Beyond Average</p>", "Channel Marketing - Prospect Identification - Lead Generation - Opportunity Pipeline - Management - ROI Tracking"],
            BusinessProcessEnablement: ["<p>Change the Game</p><p>Power Your Tech</p>", "CRM Consultancy Services - Salesforce.com - Oracle Marketing Cloud - G Suite"]
        };

        var elements = $(".existing");
        var texts = $("text");
        var innerText = $("#innerText");
        var baseURL = innerText.data("siteurl");

        elements.click(function(){
            var label = $(this).data("label");
            var modLabel = label.replace( /\s/g, "");
            var finalLabel = modLabel.split('&').join('');
            var baseURL = $("#innerText").data("siteurl");
            window.location = baseURL + elementsLinks[finalLabel];
        });

        texts.click(function(){
            var child = $(this).children('textPath');
            var content = child.text();
            var modContent = content.replace( /\s/g, "");
            var finalContent = modContent.split('&').join('');
            window.location = baseURL + elementsLinks[finalContent];
        });

        elements.hover(function(){
            //========================
            //======= mouse in =======
            //========================
            var hoveredElement = $(this);

            // remove class .active from path elements
            elements.each(function(){
               if($(this).hasClass("active")){
                   $(this).removeClass("active").addClass("complete");
               }
            });

            // add class .active to the hovered element
            hoveredElement.removeClass("complete").addClass("active").css('cursor', 'pointer');
            var label = hoveredElement.data("label");
            var modLabel = label.replace( /\s/g, "");
            var finalLabel = modLabel.split('&').join('');
            $("#innerText_1").html(elementsDescriptions[finalLabel][0]).data('link', label);
            $("#innerText_2").text(elementsDescriptions[finalLabel][1]).data('link', label);
            innerText.attr("href", baseURL + elementsLinks[finalLabel]);
            //positionInnerTextHolder();
        }, function(){
            //=========================
            //======= mouse out =======
            //=========================

            //var HoverOutedObj = $(this);
            //setTimeout(function(){
            //    HoverOutedObj.css( 'cursor', 'default' ).attr('class', 'complete');
            //}, 500);
        });

        texts.hover(function(){
            //========================
            //======= mouse in =======
            //========================
            $(this).attr('class', 'active').css( 'cursor', 'pointer' );
            var child = $(this).children('textPath');
            var content = child.text();
            var modContent = content.replace( /\s/g, "");
            var finalContent = modContent.split('&').join('');
            $("#innerText_1").html(elementsDescriptions[finalContent][0]).data('link', content);
            $("#innerText_2").text(elementsDescriptions[finalContent][1]).data('link', content);
            innerText.attr("href", baseURL + elementsLinks[finalContent]);
            //positionInnerTextHolder();
            // set active path element
            elements.each(function(){
                $(this).removeClass("active").addClass("complete");
                var label = $(this).data("label");
                var modLabel = label.replace( /\s/g, "");
                var finalLabel = modLabel.split('&').join('');
                if(finalContent == finalLabel){
                    $(this).removeClass("complete").addClass("active").css('cursor', 'pointer');
                }
            });
        }, function(){
            //=========================
            //======= mouse out =======
            //=========================
            $(this).css( 'cursor', 'default' );
        });

        var positionInnerTextHolder = function(){
            var circleHolderThHeight = $("#circle_holder_th").height();
            var innerTextHeight = innerText.height();
            innerText.css("marginTop", ((circleHolderThHeight-innerTextHeight)/2 - 350) + "px");
        };

    };

}
//===========================================================================





