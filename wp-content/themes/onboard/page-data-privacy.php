<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include 'templates/BottomRedLine.php';
include 'string_manipulation/StringManipulation.php';

get_header();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">facts</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/facts/facts.jpg" id="topBanner">

<?php
    $stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
    $title = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{content}', '{/content}');
    $content = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
?>
    <div id="technology-title"><?php echo $title; ?></div>
    <div id="facts-main-container"><?php echo $content; ?></div>



<?php
// bottom red line content
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();