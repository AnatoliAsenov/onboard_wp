<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';
include 'templates/Contacts_Offices.php';

$siteURL = get_site_url();

//$postID = get_query_var( 'id' );
$singlePost = get_post( 32 );
$postContent = $singlePost->post_content;

$header = new MobileHeader(32);
$header->printHTML();

// bottom red line content
$stringManipulator = new StringManipulation();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">contacts</div>

    <!-- top banner -->
    <style>
        @media screen and (max-width: 320px) {
            #contactsBannerContainer{
                width: 320px;
                margin: 0 auto;
            }
            #contactsBanner{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/contacts/contacts_320.png");
                height: auto;
                width: 270px;
                margin: 70px 50px 0 0;
            }
        }
        @media screen and (max-width: 478px) and (min-width: 320px) {
            #contactsBannerContainer{
                width: 320px;
                margin: 0 auto;
            }
            #contactsBanner{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/contacts/contacts_320.png");
                height: auto;
                width: 270px;
                margin: 70px 50px 0 0;
            }
        }
        @media screen and (max-width: 766px) and (min-width: 478px) {
            #contactsBannerContainer{
                width: 450px;
                margin: 0 auto;
            }
            #contactsBanner{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/contacts/contacts_320.png");
                height: auto;
                width: 270px;
                margin: 70px 50px 0 0;
            }
        }
        @media screen and (max-width: 1022px) and (min-width: 766px){
            #contactsBannerContainer{
                width: 725px;
                margin: 100px auto 0 auto;
            }
            #contactsBanner{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/contacts/contacts_768.png");
                height: auto;
                width: 270px;
                margin: 70px 50px 0 0;
            }
        }
    </style>
    <div id="contactsBannerContainer">
        <img src="" id="contactsBanner">
    </div>

<?php
// get main blocks
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
$block4 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

// separate elements of block1
$stringManipulator->stringExtractAndDelete($block1, '{general-content}', '{/general-content}');
$block1_general_content = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{general-phone}', '{/general-phone}');
$block1_general_phone = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{marketing-sales-content}', '{/marketing-sales-content}');
$block1_marketing_content = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{marketing-sales-phone}', '{/marketing-sales-phone}');
$block1_marketing_phone = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{text-content}', '{/text-content}');
$block1_text = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

// separate elements of block2
?>


    <!-- Google Code for Contact us 2017 Conversion Page -->
    <script type="text/javascript">
        /* <![CDATA[ */
        goog_snippet_vars = function() {
            var w = window;
            w.google_conversion_id = 1066581569;
            w.google_conversion_language = "en";
            w.google_conversion_format = "3";
            w.google_conversion_color = "ffffff";
            w.google_conversion_label = "hb62CL27wHIQwfzK_AM";
            w.google_remarketing_only = false;
        };

        // DO NOT CHANGE THE CODE BELOW.
        goog_report_conversion = function(url) {
            console.log("click on: " + url);
            goog_snippet_vars();
            window.google_conversion_format = "3";
            var opt = new Object();
            opt.onload_callback = function() {
                if (typeof(url) != 'undefined') {
                    window.location = url;
                }
            };
            var conv_handler = window['google_trackConversion'];
            if (typeof(conv_handler) == 'function') {
                conv_handler(opt);
            }
        };
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/1066581569/?label=hb62CL27wHIQwfzK_AM&amp;guid=ON&amp;script=0"/>
        </div>
    </noscript>




    <div id="contacts-page-top">
        <ul id="contacts-page-top-L">
            <li>
                <p class="contacts-page-sub-tit1"><?php echo $block1_general_content; ?></p>
                <p class="contacts-page-sub-tit2"><?php echo $block1_general_phone; ?></p>
            </li>
            <li>
                <p class="contacts-page-sub-tit1"><?php echo $block1_marketing_content; ?></p>
                <p class="contacts-page-sub-tit2"><?php echo $block1_marketing_phone; ?></p>
            </li>
        </ul>
        <ul id="contacts-page-top-R">
            <li>
                <div class="contacts_icons icon icon-chat"></div>
                <a href="#" class="contacts-page-exp" id="talkToExpert" onclick="goog_report_conversion('Talk to an expert button - mobile')">
                    <p class="contacts-page-top-exp1">Talk to an expert</p>
                    <p class="contacts-page-top-exp2">Drop us a line...</p>
                </a>
            </li>
            <li>
                <div class="contacts_icons icon icon-team"></div>
                <a href="<?php echo $siteURL."/mobile/mobile-careers"; ?>" class="contacts-page-exp">
                    <p class="contacts-page-top-exp1">Join our team</p>
                    <p class="contacts-page-top-exp2">Explore a career path with Onboard</p>
                </a>
            </li>
            <li>
                <div data-icon="g" class="contacts_icons icon"></div>
                <a href="#" class="contacts-page-exp" id="let_us_call_you_btn" onclick="goog_report_conversion('Let us call you button - mobile')">
                    <p class="contacts-page-top-exp1">Let us call you</p>
                    <p class="contacts-page-top-exp2">Drop us a note and we'll get back to you!</p>
                </a>
            </li>
            <li>
                <div class="contacts_icons icon icon-network"></div>
                <div class="contacts_icons_div">
                    <a class="single-post-share-a" href="https://www.facebook.com/OnboardCRM">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a class="single-post-share-a" href="https://www.linkedin.com/company/onboardcrm?trk=company_logo">
                        <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                    </a>
                    <a class="single-post-share-a" href="https://twitter.com/Onboardcrm">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a class="single-post-share-a" href="https://www.instagram.com/onboardcrm/">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                </div>
            </li>
        </ul>
    </div>

    <!-- list of offices -->
    <ul id="contacts-page-office-list">

        <!-- EMEA offices -->
        <?php
        $officeItem = new Contacts_Offices($stringManipulator, $block2);
        ?>

        <!-- AMERICAS offices -->
        <?php
        $officeItem = new Contacts_Offices($stringManipulator, $block3);
        ?>

        <!-- APAC offices -->
        <?php
        $officeItem = new Contacts_Offices($stringManipulator, $block4);
        ?>

    </ul>





<?php
$footer = new MobileFooter();
$footer->printHTML();
