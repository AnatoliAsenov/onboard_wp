<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$siteURL = get_site_url();

$postID = get_query_var( 'id' );
$singlePost = get_post( $postID );
$postContent = $singlePost->post_content;

$header = new MobileHeader($postID);
$header->printHTML();

// bottom red line content
$stringManipulator = new StringManipulation();

$postEssence = get_post_meta($postID, 'essence', true);
$postQuote = get_post_meta($postID, 'testimonial', true);
$postQuoteAuthor = get_post_meta($postID, 'testimonial_author', true);

$image = $stringManipulator->stringExtract($postContent, '{image}', '{/image}');
$imageURL = $stringManipulator->stringExtract($image, 'src="', '"');
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">cases</div>

    <!-- top banner of the page -->
    <div id="single-cs-banner">
        <div id="single-cs-banner-leftdiv">
            <div id="single-cs-banner-triangle"></div>
            <a id="single-cs-banner-triangletext"  href="<?php echo $siteURL.'/mobile/mobile-cases'; ?>">
                <p>CASE STUDY</p>
                <p>&#8592;</p>
            </a>
        </div>
        <div id="single-cs-banner-right">
            <img src="<?php bloginfo('template_url'); ?>/images/topMenu/logo.png">
            <p id="single-cs-banner-title"><?php echo $singlePost->post_title; ?></p>
        </div>
        <div id="single-cs-essence">
            <div id="single-cs-essence-text"><?php echo $postEssence; ?></div>
            <div id="single-cs-essence-line"></div>
        </div>
    </div>


    <!-- quote -->
    <div id="single-cs-main-left">
        <i class="demo-icon icon-quote-left-alt" id="single-cs-main-left-quote-symbol">&#xe802;</i>
        <div id="single-cs-quote">
            <p id="single-cs-main-left-quote">"<?php echo $postQuote; ?>"</p>
            <p id="single-cs-main-left-quoteauthor"><?php echo $postQuoteAuthor; ?></p>
        </div>
        <img id="single-cs-main-right-img" src="<?php if($imageURL!=''){ echo $imageURL; }else{ echo bloginfo('template_url').'/images/home/partner.png'; } ?>" />
        <?php

        // print all sub content elements
        $numberOfSubTitles = preg_match_all('/\bsub_title\b/', $postContent);

        for($i = 0; $i < $numberOfSubTitles/2; $i++){
            $stringManipulator->stringExtractAndDelete($postContent, '{sub_title}', '{/sub_title}');
            $tempSubTitle = $stringManipulator->neededSubString;
            $postContent = $stringManipulator->reducedString;

            $stringManipulator->stringExtractAndDelete($postContent, '{content}', '{/content}');
            $tempContent = $stringManipulator->neededSubString;
            $postContent = $stringManipulator->reducedString;

            echo "<p class='single-cs-leftTitle'>".$tempSubTitle."</p>";
            echo "<p class='single-cs-leftContent'>".$tempContent."</p>";
        }
?>



    <!-- solutions -->
    <p class='single-cs-leftTitle'>Solutions</p>
    <ul class="single-cs-main-right-ul">
        <?php

        // print all solutions
        $allSolutions = get_post_meta($postID, 'solutions', true);
        $numberOfSolutions = preg_match_all('/\bsolution\b/', $allSolutions);

        for($i = 0; $i < $numberOfSolutions/2; $i++){
            $stringManipulator->stringExtractAndDelete($allSolutions, '{solution}', '{/solution}');
            $tempSolution = $stringManipulator->neededSubString;
            $allSolutions = $stringManipulator->reducedString;
            ?>
            <li>
                <div class="red-triangle-list-element"></div>
                <p><?php echo $tempSolution; ?></p>
            </li>
        <?php } ?>
    </ul>

    <!-- results -->
    <p class='single-cs-leftTitle'>Results</p>
    <ul class="single-cs-main-right-ul">
        <?php

        // print all results
        $allResults = get_post_meta($postID, 'results', true);
        $numberOfResults = preg_match_all('/\bresult\b/', $allResults);

        for($i = 0; $i < $numberOfResults/2; $i++){
            $stringManipulator->stringExtractAndDelete($allResults, '{result}', '{/result}');
            $tempResult = $stringManipulator->neededSubString;
            $allResults = $stringManipulator->reducedString;
            ?>
            <li>
                <div class="red-triangle-list-element"></div>
                <p><?php echo $tempResult; ?></p>
            </li>
        <?php } ?>
    </ul>


<?php   // get pdf file path
        $pdf_file = $stringManipulator->stringExtract($postContent, '{pdf_file}', '{/pdf_file}');
        $pdf_url = $stringManipulator->stringExtract($postContent, 'href="', '"');
        ?>
        <div id="download-pdf">
            <img src="<?php bloginfo('template_url'); ?>/images/icons/download_pdf.png" >
            <div data-link="<?php echo $pdf_url; ?>" id="download-pdf-button">DOWNLOAD FULL TEXT</div>
        </div>
    </div>


<?php
$stringManipulator->stringExtractAndDelete($singlePost->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();