<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include "templates/MiddlePageLine.php";
include "templates/BottomRedLine.php";
include 'string_manipulation/StringManipulation.php';

get_header();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">technology</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/technology/technology.jpg" id="topBanner">


<?php
$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

//====== block 1 =======

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph1}', '{/paragraph1}');
$paragraph1 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph2}', '{/paragraph2}');
$paragraph2 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;



//====== block 2 =======
$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block2, '{paragraph1}', '{/paragraph1}');
$paragraph3 = $stringManipulator->neededSubString;
$block2 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block2, '{paragraph2}', '{/paragraph2}');
$paragraph4 = $stringManipulator->neededSubString;
$block2 = $stringManipulator->reducedString;


//====== block 3 =======

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph1}', '{/paragraph1}');
$paragraph5 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph2}', '{/paragraph2}');
$paragraph6 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph3}', '{/paragraph3}');
$paragraph7 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block3, '{paragraph4}', '{/paragraph4}');
$paragraph8 = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;



//====== block 4 =======

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
$block4 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block4, '{paragraph1}', '{/paragraph1}');
$paragraph9 = $stringManipulator->neededSubString;
$block4 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block4, '{paragraph2}', '{/paragraph2}');
$paragraph10 = $stringManipulator->neededSubString;
$block4 = $stringManipulator->reducedString;



$stringManipulator->stringExtractAndDelete($postContent, '{middle-line-text}', '{/middle-line-text}');
$middleLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>
    <!-- title of page -->
    <h1 id="technology-title"><?php echo $title; ?></h1>

    <!-- first part -->
    <ul id="technology-firstPart">
        <li id="technology-firstPart-li1">
            <p><?php echo $paragraph1; ?></p>
            <p><?php echo $paragraph2; ?></p>
        </li>
        <li id="technology-firstPart-li2">
            <img src="<?php bloginfo('template_url'); ?>/images/technology/marketingxpress/book.png" style="margin-top:-26px;float:right;width:400px;">
        </li>
    </ul>


    <!-- middle red line -->
<?php
$middleLine = new MiddlePageLine();
$middleLine->setBackgroundColor("#ec632d");
$middleLine->setText($middleLineText);
$middleLine->printHTML();
?>

<style>
    @media screen and (min-width: 1400px) {
        #marketingxpress_second_bg {
            width: 1100px;
            display: block;
            margin: 20px auto 120px;
        }
    }
    @media screen and (min-width: 1200px) and (max-width: 1400px) {
        #marketingxpress_second_bg {
            width: 1100px;
            display: block;
            margin: 20px auto 120px;
        }
    }
    @media screen and (max-width: 1200px) {
        #marketingxpress_second_bg {
            width: 95%;
            display: block;
            margin: 20px auto 120px;
        }
    }
</style>

    <!-- second part -->
    <img src="<?php bloginfo('template_url'); ?>/images/technology/marketingxpress/second_bg.png" id="marketingxpress_second_bg"/>



<?php

// bottom red line content
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();
