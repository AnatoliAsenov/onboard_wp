<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$siteURL = get_site_url();

$pageContent = get_post(42);
$postContent = $pageContent->post_content;

$header = new MobileHeader(42);
$header->printHTML();

$stringManipulator = new StringManipulation();

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{block1-title}', '{/block1-title}');
$block1title = $stringManipulator->neededSubString;
//$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{block1-content}', '{/block1-content}');
$block1content = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($block2, '{block2-title}', '{/block2-title}');
$block2title = $stringManipulator->neededSubString;
$stringManipulator->stringExtractAndDelete($block2, '{block2-content}', '{/block2-content}');
$block2content = $stringManipulator->neededSubString;
$block2 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($block3, '{block3-title}', '{/block3-title}');
$block3title = $stringManipulator->neededSubString;
$stringManipulator->stringExtractAndDelete($block3, '{block3-content}', '{/block3-content}');
$block3content = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;

// bottom red line content
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

echo "<div id='questions_and_answers' style='display: none;'>".$postContent."</div>";

?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">careers</div>

    <!-- top banner -->
    <img id="topBanner" src="<?php bloginfo('template_url'); ?>/images/mobile/careers/careers_768.jpg" alt="careers" />


    <div class="careers-block-title"><?php echo $block1title; ?></div>
    <div class="careers-block-text"><?php echo $block1content; ?></div>

    <div id="careers-block1-button">SEE WHERE YOU FIT</div>

<!-- -->
    <style>
        @media screen and (max-width: 320px) {
            #careers-img1{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/work_320.png");
                height: 1040px;
                width: 320px;
                margin: 70px auto 0 auto;
            }
        }
        @media screen and (max-width: 478px) and (min-width: 320px) {
            #careers-img1{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/work_320.png");
                height: 1040px;
                width: 320px;
                margin: 70px auto 0 auto;
            }
        }
        @media screen and (max-width: 766px) and (min-width: 478px) {
            #careers-img1{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/work_480.png");
                height: 634px;
                width: 450px;
                margin: 70px auto 0 auto;
            }
        }
        @media screen and (max-width: 1022px) and (min-width: 766px){
            #careers-img1{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/work_768.png");
                height: 334px;
                width: 738px;
                margin: 70px auto 0 auto;
            }
        }
    </style>
    <img id="careers-img1" alt="careers" />

<!-- employee quotes -->
    <div class="careers-block-title" id="emp_short_quo">EMPLOYEE SHORT QUOTE</div>

    <div class="careers_bigicon_container">
        <div id="block2-quote">
            <i class="demo-icon icon-quote-right-alt">&#xe801;</i>
        </div>
    </div>
    <hr class="careers_fullHR">
    <div id="careers_onboard_container">
        <div id="block2_onboard_Text">
            <div class="careers_emp_name"></div>
            <p class="careers_emp_text"></p>
        </div>
    </div>

    <hr class="careers_partHR" id="first_partHR">

    <hr class="careers_fullHR" id="carees_second_fullHR">
    <div class="careers_bigicon_container">
        <div id="block2_gender" class="icon" data-icon="g"></div>
    </div>

    <div id="careers_employee_container">
        <div id="block2_employee_Text">
            <div class="careers_emp_name"></div>
            <p class="careers_emp_text"></p>
        </div>
        <hr class="careers_partHR" id="second_partHR">
    </div>


<!-- jobs list -->
    <div class="careers-block-title" id="careers-joblist-title"><?php echo $block2title; ?></div>
    <div class="careers-block-text"><?php echo $block2content; ?></div>

    <table id="careers-block3-table">
        <tr>
            <th class="careers-block3-table-tit-row">JOB TITLE</th>
            <th class="careers-block3-table-tit-row careers-tab-lang-col">LANGUAGE</th>
            <th class="careers-block3-table-tit-row">LOCATION</th>
        </tr>
<?php
    // get the jobs posts
    $args = array('post_type' => 'jobs', 'post_per_page' => -1);
    $loop = new WP_Query($args);

    if($loop->have_posts()) {
        foreach ($loop->posts as $job) {
            // check for visibility
            $jobVisibility = get_post_meta($job->ID, 'visibility', true);
            $jobLocation = get_post_meta($job->ID, 'location', true);
            $jobLanguages = get_post_meta($job->ID, 'languages', true);
            if ($jobVisibility == "visible" || $jobVisibility == "") {
                ?>
                <tr>
                    <th><a href="<?php echo $siteURL."/mobile/mobile-job?id=".$job->ID; ?>" class="block3-job-title"><?php echo $job->post_title; ?></a></th>
                    <th class="block3-job-language careers-tab-lang-col"><?php echo $jobLanguages; ?></th>
                    <th class="block3-job-location"><?php echo $jobLocation; ?></th>
                </tr>
                <?php
            }
        }
    }
?>
    </table>


    <!-- block 4 -->
    <div class="careers-block-title"><?php echo $block3title; ?></div>
    <div class="careers-block-text" id="careers_lasttext"><?php echo $block3content; ?></div>



    <!-- footer -->
<?php

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();