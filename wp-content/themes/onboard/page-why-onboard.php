<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include 'templates/BottomRedLine.php';
include 'string_manipulation/StringManipulation.php';

get_header();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">facts</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/facts/facts.jpg" id="topBanner">

<?php
$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
$block4 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{list}', '{/list}');
$list = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>
<!-- title -->
<div id="technology-title"><?php echo $title; ?></div>

<div id="facts-main-container">
    <div id="facts-why1">
        <img src="<?php bloginfo('template_url'); ?>/images/facts/why/1.png">
        <ul>
            <li><?php echo $block1; ?></li>
            <li><?php echo $block2; ?></li>
        </ul>
    </div>
    <table id="facts-why2_1" class="facts-why2">

<?php
    $numberOfListItems = preg_match_all('/\blist-item\b/', $list);

    for($z = 0; $numberOfListItems/2 > $z; $z++) {

        $stringManipulator->stringExtractAndDelete($list, '{list-item}', '{/list-item}');
        $tempParagraph = $stringManipulator->neededSubString;
        $list = $stringManipulator->reducedString;

        if(!($z%2)){
            echo "<tr>";
            echo "<th><div class='list-triangle facts-R-list-triangle-inline'></div><p>".$tempParagraph."</p></th>";
        }else{
            echo "<th><div class='list-triangle facts-R-list-triangle-inline'></div><p>".$tempParagraph."</p></th>";
            echo "</tr>";
        }

    }

    if($z%2){
        echo "<th></th></tr>";
    }

?>

    </table>
    <div id="facts-why3">
        <ul>
            <li><?php echo $block3; ?></li>
            <li><?php echo $block4; ?></li>
        </ul>
        <img src="<?php bloginfo('template_url'); ?>/images/facts/why/2.png">
    </div>
</div>



<?php
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();