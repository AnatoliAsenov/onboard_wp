<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */


include 'string_manipulation/StringManipulation.php';
include 'templates/BottomRedLine.php';

get_header();
$siteURL = get_site_url();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/banners/whatwedo.jpg" id="topBanner">

<?php
    // string manipulator
    $stringManipulator = new StringManipulation();

    $slug = basename(get_permalink());
    $siteURL = get_site_url();
?>

<ul id="whatWeDoMainContentUl" style="background-image: url(<?php bloginfo('template_url'); ?>/images/whatwedo/back_wwd_main.jpg);">
        
<?php
    // print all paragraphs
    $postContent = $post->post_content;
    $numberOfSubTitles = preg_match_all('/\bparagraph-delimiter\b/', $postContent);

    for($i = 0; $i < $numberOfSubTitles/2; $i++) {
        // get paragraph
        $stringManipulator->stringExtractAndDelete($postContent, '{paragraph-delimiter}', '{/paragraph-delimiter}');
        $tempParagraph = $stringManipulator->neededSubString;
        $postContent = $stringManipulator->reducedString;

        // get paragraph title
        $stringManipulator->stringExtractAndDelete($tempParagraph, '{paragraph-title}', '{/paragraph-title}');
        $tempParagraphTitle = $stringManipulator->neededSubString;
        $tempParagraph = $stringManipulator->reducedString;

        // get paragraph text
        $stringManipulator->stringExtractAndDelete($tempParagraph, '{paragraph-text}', '{/paragraph-text}');
        $tempParagraphText = $stringManipulator->neededSubString;
        $tempParagraph = $stringManipulator->reducedString;

        // get paragraph link
        $stringManipulator->stringExtractAndDelete($tempParagraph, '{paragraph-link}', '{/paragraph-link}');
        $tempParagraphLink = $stringManipulator->neededSubString;
        $tempParagraph = $stringManipulator->reducedString;

        // get paragraph number
        $theNum = $i + 1;
?>
    <li style="background-image: url(<?php bloginfo('template_url'); ?>/images/whatwedo/box_back.png);">
        <h2><?php echo $tempParagraphTitle; ?></h2>
        <h4><?php echo $tempParagraphText; ?></h4>
        <p><?php echo "0{$theNum}"; ?></p>
        <a href="<?php echo $tempParagraphLink; ?>"><div class="icon icon-slim-right"></div></a>
    </li>            
<?php } ?>

</ul>

<?php
    // get the page title
    $stringManipulator->stringExtractAndDelete($postContent, '{page-title}', '{/page-title}');
    $pageTitle = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    // get the page sub title
    $stringManipulator->stringExtractAndDelete($postContent, '{page-sub-title}', '{/page-sub-title}');
    $pageSubTitle = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
?>

<!-- h1><?php echo $pageTitle; ?></h1>
<h3><?php echo $pageSubTitle; ?></h3 -->

<?php
    // bottom red line content
    $stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
    $bottomRedLineText = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $subFooter = new BottomRedLine($bottomRedLineText);
    $subFooter->printHTML();

    get_footer();