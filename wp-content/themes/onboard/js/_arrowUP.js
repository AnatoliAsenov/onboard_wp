/**
 * Created by Anatoli Asenov
 * Onboard Bulgaria
 */

$(document).ready(function() {
    if($("#arrowUP").length){
        $("#arrowUP").click(function(){
            $("html, body").animate({ scrollTop: 0 }, 900);
            return false;
        });
    }
});