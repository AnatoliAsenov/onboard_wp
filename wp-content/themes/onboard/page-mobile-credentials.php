<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(174);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(174);
$header->printHTML();

$siteURL = get_site_url();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/wwdo_768.jpg" id="topBanner">

    <div id="wwd-container">

        <div id="wwd_title">CREDENTIALS</div>

        <div id="technology-texts">
<?php
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
$block4 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>
            <p><?php echo $block1; ?></p>
            <p><?php echo $block2; ?></p>
        </div>

        <img src="<?php bloginfo('template_url'); ?>/images/mobile/technology/credentials.png" id="technology-internal-img" />

        <ul id="technology-subscribe-list">
            <?php
            $numberOfParagraphs = preg_match_all('/\blist-item\b/', $postContent);

            for($z = 0; $numberOfParagraphs/2 > $z; $z++) {
                $stringManipulator->stringExtractAndDelete($postContent, '{list-item}', '{/list-item}');
                $tempParagraph = $stringManipulator->neededSubString;
                $postContent = $stringManipulator->reducedString;
                echo "<li><div class='red-triangle-list-element'></div><p>".$tempParagraph."</p></li>";
            } ?>
        </ul>

        <a href="<?php echo $siteURL."/technology/credentials/quality-of-services"; ?>" class="technology-credentials-a">
            <div class="icon icon-download"></div>
            <p><?php echo $block3; ?></p>
        </a>
        <a href="<?php echo $siteURL."/technology/credentials/information-security"; ?>" class="technology-credentials-a">
            <div class="icon icon-download"></div>
            <p><?php echo $block4; ?></p>
        </a>

    </div>

    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;


$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();