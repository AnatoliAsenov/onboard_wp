/**
 * Created by Anatoli Asenov
 */

$(document).ready(function() {

    if ($("#technology-middleLine").length) {

        //========== position the rocket ============
        var screenWith = getScreenWidth();
        var theRocket = $("#theRocket");
        var middleLine = $("#technology-middleLine");

        var middleLineHeight = middleLine.height();
        var middleLineWidth = middleLine.width();
        var middleLineTopOffset = middleLine.offset().top;

        theRocket.css({
            marginRight: ((screenWith - middleLineWidth)/2 + 100) + "px",
            opacity: 1
        });

        //========= activate animation ==========
        var notActivated = true;

        $(window).scroll(function(){

            var middleTrackingPixelTop = $("#trackingPixel_middle").offset().top;

            if(notActivated && middleLineTopOffset < middleTrackingPixelTop){
                theRocket.animate({
                    width: "220px",
                    marginTop: "-" + (middleLineHeight + 90) + "px",
                    marginRight: "20px"
                }, 800, function(){
                    $("#technology-secondPart").animate({
                        opacity: 1
                    }, 600);
                });

                notActivated = false;
            }
        });

    }

});
