<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

function add_query_vars_filter( $vars ){
    $vars[] = "id";
    $vars[] = "tag";
    $vars[] = "category";
    return $vars;
}

//Add custom query vars
add_filter( 'query_vars', 'add_query_vars_filter' );