<?php

/**
 * Anatoli Asenov
 * Onboard Bulgaria
 */
class Contacts_Offices
{

    public function __construct($stringManipulator, $block2){

        $stringManipulator->stringExtractAndDelete($block2, '{title}', '{/title');
        $block2_title = $stringManipulator->neededSubString;
        $block2 = $stringManipulator->reducedString;

        $numberOfLocations = preg_match_all('/\blocation-item\b/', $block2);
?>

<li><p class="contacts-page-office-list-titles"><?php echo $block2_title; ?></p></li>
<li>
    <ul class="offices-list">
<?php

        for($v = 0; $numberOfLocations/2 > $v; $v++) {
            $stringManipulator->stringExtractAndDelete($block2, '{location-item}', '{/location-item}');
            $location = $stringManipulator->neededSubString;
            $block2 = $stringManipulator->reducedString;

            $stringManipulator->stringExtractAndDelete($location, '{location-title}', '{/location-title}');
            $location_title = $stringManipulator->neededSubString;
            $location = $stringManipulator->reducedString;

            $stringManipulator->stringExtractAndDelete($location, '{location-content}', '{/location-content}');
            $location_content = $stringManipulator->neededSubString;
            $location = $stringManipulator->reducedString;

            $stringManipulator->stringExtractAndDelete($location, '{location-image}', '{/location-image}');
            $location_image = $stringManipulator->neededSubString;
            $location = $stringManipulator->reducedString;

            $stringManipulator->stringExtractAndDelete($location_image, 'src="', '"');
            $location_image = $stringManipulator->neededSubString;

            $stringManipulator->stringExtractAndDelete($location, '{location-coordinates}', '{/location-coordinates}');
            $locationCoordinates = $stringManipulator->neededSubString;
            $location = $stringManipulator->reducedString;

$theLI_1 = <<<LKJ
        <li class="offices-list-li">
            <a href="{$locationCoordinates}" target="_blank">
                <div class="offices-list-holder" >
LKJ;
                        echo $theLI_1;
            ?>
                    <div class="offices-list-image" style="background-image:url(<?php echo $location_image; ?>);"></div>
                    <div class="offices-list-mask" style="background-image:url('<?php bloginfo('template_url'); ?>/images/contacts/mask.png');">
                        <h3><?php echo $location_title; ?></h3>
            <?php
                    $allElementsOfText = explode("*", $location_content);
                    for($i = 0; count($allElementsOfText) > $i; $i++){
                        echo "<p class='offices-list-texts-p'>".$allElementsOfText[$i]."</p>";
                    }
            ?>
                    </div>
                </div>
            </a>
        </li>
<?php
}
?>
    </ul>
</li>

<?php

    }
}