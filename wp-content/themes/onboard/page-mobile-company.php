<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$siteURL = get_site_url();

$pageContent = get_post(126);
$postContent = $pageContent->post_content;

$header = new MobileHeader(126);
$header->printHTML();

$stringManipulator = new StringManipulation();

$aboutPosts = array();
for($i = 0; $i < 2; $i++) {
    $stringManipulator->stringExtractAndDelete($postContent, '{paragraph-delimiter}', '{/paragraph-delimiter}');
    $aboutPosts[$i] = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
}

// bottom red line content
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">about</div>

    <!-- top banner -->
    <img id="topBanner" src="<?php bloginfo('template_url'); ?>/images/mobile/aboutus/about_768.jpg" alt="About us" />


<!-- main content -->
<style>
    @media screen and (max-width: 320px) {
        #about-company-main{
            background-image: url("<?php bloginfo('template_url'); ?>/images/mobile/aboutus/company_320.png");
            height: 831px;
            width: 320px;
            margin: 62px auto 0 auto;
        }
    }
    @media screen and (max-width: 478px) and (min-width: 320px) {
        #about-company-main{
            background-image: url("<?php bloginfo('template_url'); ?>/images/mobile/aboutus/company_320.png");
            height: 831px;
            width: 320px;
            margin: 62px auto 0 auto;
        }
        .about-company-main-sub{
            width: 250px;
            height: 160px;
            text-align: center;
            color: #ffffff;
            font-size: 14px;
            font-weight: 200;
            padding: 0 33px;
        }
        #about-company-main-1{
            padding-top: 212px;
        }
        #about-company-main-1 a{    
            text-decoration: none;
	    color: #e4e4e4;
	    font-weight: bold;
	}
        #about-company-main-2{
            padding-top: 75px;
        }
        /*img*/
        #about-company-img{
            content:url("<?php bloginfo('template_url'); ?>/images/mobile/aboutus/wd_320.png");
            width: 250px;
            height: auto;
            display: block;
            margin: 75px auto 90px auto;
        }
    }
    @media screen and (max-width: 766px) and (min-width: 478px) {
        #about-company-main{
            background-image: url("<?php bloginfo('template_url'); ?>/images/mobile/aboutus/company_480.png");
            height: 833px;
            width: 448px;
            margin: 62px auto 0 auto;
        }
        .about-company-main-sub{
            width: 382px;
            height: 160px;
            text-align: center;
            color: #ffffff;
            font-size: 16px;
            font-weight: 200;
            padding: 0 33px;
        }
        #about-company-main-1{
            padding-top: 230px;
        }
        #about-company-main-1 a{    
            text-decoration: none;
	    color: #e4e4e4;
	    font-weight: bold;
	}
        #about-company-main-2{
            padding-top: 75px;
        }
        /*img*/
        #about-company-img{
            content:url("<?php bloginfo('template_url'); ?>/images/mobile/aboutus/wd_480.png");
            width: 340px;
            height: auto;
            display: block;
            margin: 75px auto 90px auto;
        }
    }
    @media screen and (max-width: 1022px) and (min-width: 766px){
        #about-company-main{
            background-image: url("<?php bloginfo('template_url'); ?>/images/mobile/aboutus/company_768.png");
            height: 831px;
            width: 737px;
            margin: 62px auto 0 auto;
        }
        .about-company-main-sub{
            width: 671px;
            height: 160px;
            text-align: center;
            color: #ffffff;
            font-size: 18px;
            font-weight: 200;
            padding: 0 33px;
        }
        #about-company-main-1{
            padding-top: 255px;
        }
        #about-company-main-1 a{    
            text-decoration: none;
	    color: #e4e4e4;
	    font-weight: bold;
	}
        #about-company-main-2{
            padding-top: 70px;
        }
        /*img*/
        #about-company-img{
            content:url("<?php bloginfo('template_url'); ?>/images/mobile/aboutus/wd_768.png");
            width: 600px;
            height: auto;
            display: block;
            margin: 75px auto 90px auto;
        }
    }
</style>
<div id="about-company-main">
    <div class="about-company-main-sub" id="about-company-main-1"><?php echo $aboutPosts[0]; ?></div>
    <div class="about-company-main-sub" id="about-company-main-2"><?php echo $aboutPosts[1]; ?></div>
</div>

<!-- img src="" id="about-company-img" / -->
<h1 id="about-company-defines">WHAT DEFINES US</h1>
<h2 id="about-company-defines-sub">We create solutions to impossible problems</h2>

<ul id="about-company-defines-ul">
    <li>
        <div class="icon icon-icon-impobjects1"></div>
        <h3>EXPERIENCE</h3>
        <h4>We were successful in B2B before CRM automation became standard</h4>
    </li>
    <li>
        <div class="icon icon-icon-impobjects3"></div>
        <h3>THOUGHT LEADERSHIP</h3>
        <h4>We turn ideas into reality</h4>
    </li>
    <li>
        <div class="icon icon-icon-impobjects2"></div>
        <h3>IMPACTFUL RELATIONSHIP</h3>
        <h4>It's all about the people, so we focus on genuine experiences that bring meaning</h4>
    </li>
    <li>
        <div class="icon icon-icon-impobjects4"></div>
        <h3>GLOBAL REACH</h3>
        <h4>We work beyond borders and time, in over 50 languages from 38 locations</h4>
    </li>
</ul>


<!-- footer -->
<?php

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();
