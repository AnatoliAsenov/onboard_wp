/**
 * Created by Anatoli Asenov
 */

$(document).ready(function() {
    if($("#about-company-main").length){

        // vertical center the text in the animated banner
        var ul1Height = $("#about-company-main-ul1").height();
        var ul2Height = $("#about-company-main-ul2").height();
        var text1 = $("#about-company-main-text1");
        var text1Height = text1.height();
        var text2 = $("#about-company-main-text2");
        var text2Height = text2.height();
        //text1.css("marginTop", ((ul1Height-text1Height)/2 + 10) + "px");
        text2.css("marginTop", ((ul2Height-text2Height)/2 + 1) + "px");

        // animation
        var theAnimation = function(){
            $("#about-company-main-ul1").animate({
                marginLeft: 0,
                opacity: 1
            }, 800, function(){
                // show text
                $("#about-company-main-text1").animate({
                   opacity: 1
                }, 500);
            });
            $("#about-company-main-ul2").animate({
                marginLeft: 0,
                opacity: 1
            }, 800, function(){
                // show text
                $("#about-company-main-text2").animate({
                    opacity: 1
                }, 500);
            });
        };

        // activate animation
        var isAnimationActivated = false;
        $(window).scroll(function () {
            if(isAnimationActivated == false){
                isAnimationActivated = true;
                theAnimation();
            }
        });
    }
});