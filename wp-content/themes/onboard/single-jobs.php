<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

get_header();

$postLocation = get_post_meta($post->ID, 'location', true);
$postLanguages = get_post_meta($post->ID, 'languages', true);
$domainURL = get_site_url();

?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">careers</div>


<!-- single job container -->
<div id="single-job-container">
    <div id="single-job-container1">
        <h1 id="single-job-diagonal-tit">CAREERS</h1>
        <div id="single-job-diagonal"></div>
    </div>
    <div id="single-job-container2">
        <p><?php echo $post->post_title; ?></p>
        <ul>
            <li>
                <i class="fa fa-language" aria-hidden="true"></i>
                <p><?php echo $postLanguages; ?></p>
            </li>
            <li>
                <i class="fa fa-globe" aria-hidden="true"></i>
                <p><?php echo $postLocation; ?></p>
            </li>
            <li>
                <a id="single-job-applynow" href="<?php echo add_query_arg(array("id" => $post->ID), $domainURL."/apply-for-job" ); ?>">APPLY NOW</a>
            </li>
        </ul>
    </div>

    <div id="single-job-main-content" class="page-with-alotof-text"><?php echo $post->post_content; ?></div>

    <div id="single-post-bottom-white-line">
        <ul id="single-post-bottom-buttons">
            <li>
                <a id="single-post-apply-bottom" href="<?php echo add_query_arg(array("id" => $post->ID), $domainURL."/apply-for-job" ); ?>">APPLY NOW</a>
            </li>
            <!--li>
                <a id="single-post-refer-bottom" href="#">REFER A FRIEND</a>
            </li-->
            <li id="single-post-share-bottom">
                <p class="single-post-share single-post-share-p">SHARE THIS JOB</p>
                <a class="single-post-share single-post-share-div" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a class="single-post-share single-post-share-div" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&source=http://www.onboardcrm.com" target="_blank">
                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                </a>
                <a class="single-post-share single-post-share-div" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
            </li>
        </ul>
    </div>

</div>

<?php

get_footer();