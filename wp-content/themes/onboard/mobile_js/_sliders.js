$(document).ready(function() {

//====================================================
//________________ testimonial slider ________________
//====================================================

    // init carousel
    var carouselContainer = $('#testimonials');
    carouselContainer.slick({
        dots: false,
        infinite: true,
        speed: 1400,
        slidesToShow: 1,
        accessibility: false,
        autoplay: false,
        arrows: false
    });

    // carousel change events
    var quotes = $(".testimonials_item_quote");
    carouselContainer.on('beforeChange', function (event, slick, currentSlide, nextSlide) {
            quotes.animate({opacity: 0}, 50);
        })
        .on('afterChange', function (event, slick, currentSlide, nextSlide) {
            quotes.animate({opacity: 0.4}, 500);
        });

    $("#testimonials_right_arrow").click(function () {
        $('#testimonials').slick('slickNext');
    });

    $("#testimonials_left_arrow").click(function () {
        $('#testimonials').slick('slickPrev');
    });

});
