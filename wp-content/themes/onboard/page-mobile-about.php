<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$header = new MobileHeader(36);
$header->printHTML();

$siteURL = get_site_url();

$pageContent = get_post(36);
$postContent = $pageContent->post_content;

$stringManipulator = new StringManipulation();

$stringManipulator->stringExtractAndDelete($postContent, '{title1}', '{/title1}');
$title1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($postContent, '{sub-title1}', '{/sub-title1}');
$subtitle1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{title2}', '{/title2}');
$title2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($postContent, '{sub-title2}', '{/sub-title2}');
$subtitle2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{title3}', '{/title3}');
$title3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($postContent, '{sub-title3}', '{/sub-title3}');
$subtitle3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

// bottom red line content
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">about</div>

<!-- top banner -->
<img id="topBanner" src="<?php bloginfo('template_url'); ?>/images/mobile/aboutus/about_768.jpg" alt="About us" />


<div id="about_big_container">
    <!-- company block -->
    <div class="about-module">
        <h1><?php echo $title1; ?></h1>
        <h2><?php echo $subtitle1; ?></h2>
        <img src="<?php bloginfo('template_url'); ?>/images/mobile/aboutus/box1.jpg" id="" />
        <a href="<?php echo $siteURL."/mobile/mobile-company"; ?>" >
            <div data-icon="f" class="icon"></div>
        </a>
        <div><?php echo $block1; ?></div>
    </div>
    <!-- grey line -->
    <div class="about-grey-line"></div>

    <!-- team block -->
    <div class="about-module">
        <h1><?php echo $title2; ?></h1>
        <h2><?php echo $subtitle2; ?></h2>
        <img src="<?php bloginfo('template_url'); ?>/images/mobile/aboutus/box2.jpg" id="" />
        <a href="<?php echo $siteURL."/mobile/mobile-careers"; ?>" >
            <div data-icon="f" class="icon"></div>
        </a>
        <div><?php echo $block2; ?></div>
    </div>
    <!-- grey line -->
    <div class="about-grey-line"></div>
</div>


    <!-- community block -->
    <div class="about-module" id="about-community-block">
        <h1><?php echo $title3; ?></h1>
        <h2><?php echo $subtitle3; ?></h2>
        <img src="<?php bloginfo('template_url'); ?>/images/mobile/aboutus/box3.jpg" id="" />
        <a href="<?php echo $siteURL."/mobile/mobile-factsandfigures/mobile-community/"; ?>" >
            <div data-icon="f" class="icon"></div>
        </a>
        <div><?php echo $block3; ?></div>
    </div>
    <!-- grey line -->
    <div class="about-grey-line"></div>


<!-- continents counter -->
<table id="about_tab" style="background-image:url('<?php bloginfo('template_url'); ?>/images/mobile/aboutus/map.png');">
    <tr>
        <th class="about_tab_th" id="countries_num">0</th>
        <th class="about_tab_th" id="offices_num">0</th>
        <th class="about_tab_th" id="continents_num">0</th>
    </tr>
    <tr>
        <th class="about_tab_txt">COUNTRIES</th>
        <th class="about_tab_txt">OFFICES</th>
        <th class="about_tab_txt">CONTINENTS</th>
    </tr>
</table>



<!-- footer -->
<?php

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();