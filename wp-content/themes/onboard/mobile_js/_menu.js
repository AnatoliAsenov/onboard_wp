/**
 *
 */

(function(){

    // position menu
    function positionMenu(isBlackLineVisible){
        var topBlackLineHeight = $("#header-topBlackLine").height();
        var logoMenuHeight = $("#logoAndMenuContainer").height();
        //var greyLineOffset = $("#header-grey-line").position().top;
        var menuCSSTop = logoMenuHeight;
        if (isBlackLineVisible){
            menuCSSTop = logoMenuHeight + topBlackLineHeight;
        }
        $("#site-menu").css("top", menuCSSTop + "px");
    }

    positionMenu(true);

    // sticky top nav bar
    $(document).scroll(function(){

        var logoAndMenuContainer = $("#logoAndMenuContainer");

        if( $("body").scrollTop() > 4 ){
            positionMenu(false);
            if(logoAndMenuContainer.hasClass("logoAndMenu_begin")) {
                logoAndMenuContainer.removeClass("logoAndMenu_begin").addClass("logoAndMenu_fixed");
            }
        }else{
            positionMenu(true);
            if(logoAndMenuContainer.hasClass("logoAndMenu_fixed")) {
                logoAndMenuContainer.removeClass("logoAndMenu_fixed").addClass("logoAndMenu_begin");
            }
        }

    });

    // open/close menu
    var isMenuOpen = false;
    $("#header-menu-icon").click(function(){
        // check screen resolution
        if(screenWidth <= 766) {
            console.log("var value: "+screenWidth+" / ");
            console.log("menu mode: smaller than 768px");
            //screen is smaller than 766px
            if (isMenuOpen) {
                // menu is open and need to be closed
                // -change menu symbol
                $("#header-menu-icon").attr("data-icon", "i");
                $("#site-menu").animate({left: "-100%"}, 500);
                // -change status via boolean var
                isMenuOpen = false;
            } else {
                // menu is closed and need to be open
                // -change menu symbol
                $("#header-menu-icon").attr("data-icon", "j");
                $("#site-menu").animate({left: "0"}, 500);
                // -change status via boolean var
                isMenuOpen = true;
            }
        }else{
            console.log("menu mode: bigger than 768px");
            // screen is bigger than 768px
            if (isMenuOpen) {
                // menu is open and need to be closed
                // -change menu symbol
                $("#header-menu-icon").attr("data-icon", "i");
                $("#site-menu").animate({right: "-100%"}, 500);
                // -change status via boolean var
                isMenuOpen = false;
            } else {
                // menu is closed and need to be open
                // -change menu symbol
                $("#header-menu-icon").attr("data-icon", "j");
                $("#site-menu").animate({right: "0"}, 500);
                // -change status via boolean var
                isMenuOpen = true;
            }
        }
    });

    // open/close WHAT-WE-DO sub menu
    var isWhatwedoOpen = false;
    $("#header-submenu-whatwedo").click(function(){
        if(isWhatwedoOpen){
            // menu is open and need to be closed
            $("#header-submenu-whatwedo").css("color", "#ffffff");
            // animate close sub menu
            $(".submenu-whatwedo-a").animate({opacity: 0}, 400, function(){
                $(this).css("display", "none");
                $("#submenu-whatwedo-content")
                    .animate({height: "0"}, 400, function(){
                        $(this).css("display", "none");
                    });
            });
            isWhatwedoOpen = false;
        }else{
            // menu is closed and need to be open
            $("#header-submenu-whatwedo").css("color", "#EB5C4E");
            // animate open sub menu
            $("#submenu-whatwedo-content")
                .css({display: "block"})
                .animate({height: "146px"}, 400, function(){
                    $(".submenu-whatwedo-a").css("display", "block").animate({opacity: 1}, 400);
                });
            isWhatwedoOpen = true;
        }
    });

    // open/close TECHNOLOGY sub menu
    var isTechnologyOpen = false;
    $("#header-submenu-technology").click(function(){
        if(isTechnologyOpen){
            // menu is open and need to be closed
            $("#header-submenu-technology").css("color", "#ffffff");
            // animate close sub menu
            $(".submenu-technology-a").animate({opacity: 0}, 400, function(){
                $(this).css("display", "none");
                $("#submenu-technology-content")
                    .animate({height: "0"}, 400, function(){
                        $(this).css("display", "none");
                    });
            });
            isTechnologyOpen = false;
        }else{
            // menu is closed and need to be open
            $("#header-submenu-technology").css("color", "#EB5C4E");
            // animate open sub menu
            $("#submenu-technology-content")
                .css({display: "block"})
                .animate({height: "146px"}, 400, function(){
                    $(".submenu-technology-a").css("display", "block").animate({opacity: 1}, 400);
                });
            isTechnologyOpen = true;
        }
    });

    // click on "Live chat" in top black line
    $("#live-chat-topmenu-btn").click(function(){
        tidioChatApi.method('popUpOpen');
    });
    // click on "Live chat" in contacts page (talk to an expert)
    $("#talkToExpert").click(function(){
        tidioChatApi.method('popUpOpen');
    });

})();