<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include "templates/BottomRedLine.php";
include 'string_manipulation/StringManipulation.php';

// bottom red line content

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();

get_header();

?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">careers</div>


<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/careers/careers.jpg" id="topBanner">


<!-- block 1 -->
<?php
//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{block1-title}', '{/block1-title}');
$block1title = $stringManipulator->neededSubString;
//$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{block1-content}', '{/block1-content}');
$block1content = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;
?>
<div id="careers-block1-title"><?php echo $block1title; ?></div>
<div id="careers-block1-text"><?php echo $block1content; ?></div>

<div id="careers-block1-button">SEE WHERE YOU FIT</div>
<table id="careers-block1-picwithicons" style="background-image: url('<?php bloginfo('template_url'); ?>/images/careers/careers_bg.png')"   border="0" cellspacing="0" cellpadding="0">
    <tr>
        <th id="careers-block1-picwithicons-title" colspan="4">WORK AT ONBOARD</th>
    </tr>
    <tr>
        <th class="careers-block1-picwithicons-darker"><img src="<?php bloginfo('template_url'); ?>/images/careers/growth.png" class="careers-block1-picwithicons-img"></th>
        <th class="careers-block1-picwithicons-lighter"><img src="<?php bloginfo('template_url'); ?>/images/careers/people.png" class="careers-block1-picwithicons-img"></th>
        <th class="careers-block1-picwithicons-darker"><img src="<?php bloginfo('template_url'); ?>/images/careers/enterpreneurship.png" class="careers-block1-picwithicons-img"></th>
        <th class="careers-block1-picwithicons-lighter"><img src="<?php bloginfo('template_url'); ?>/images/careers/fair_relations.png" class="careers-block1-picwithicons-img"></th>
    </tr>
    <tr>
        <th class="careers-block1-picwithicons-darker"><p class="careers-block1-picwithicons-row1">GROWTH</p></th>
        <th class="careers-block1-picwithicons-lighter"><p class="careers-block1-picwithicons-row1">PEOPLE</p></th>
        <th class="careers-block1-picwithicons-darker"><p class="careers-block1-picwithicons-row1">ENTERPRENEURSHIP</p></th>
        <th class="careers-block1-picwithicons-lighter"><p class="careers-block1-picwithicons-row1">FAIR RELATIONS</p></th>
    </tr>
    <tr>
        <th class="careers-block1-picwithicons-darker"><p class="careers-block1-picwithicons-row2">Dream big</p></th>
        <th class="careers-block1-picwithicons-lighter"><p class="careers-block1-picwithicons-row2">Not just "Hello"</p></th>
        <th class="careers-block1-picwithicons-darker"><p class="careers-block1-picwithicons-row2">Team up for change</p></th>
        <th class="careers-block1-picwithicons-lighter"><p class="careers-block1-picwithicons-row2">Be genuine</p></th>
    </tr>
    <tr>
        <th class="careers-block1-picwithicons-darker">
            <p class="careers-block1-picwithicons-row3">We work with ambition and audacity to accomplish our own and our client's goals</p>
        </th>
        <th class="careers-block1-picwithicons-lighter">
            <p class="careers-block1-picwithicons-row3">We build strong and shared relations that link and engage our global teams</p>
        </th>
        <th class="careers-block1-picwithicons-darker">
            <p class="careers-block1-picwithicons-row3">Our innovative energy and intensity radiate out to our work milieu</p>
        </th>
        <th class="careers-block1-picwithicons-lighter">
            <p class="careers-block1-picwithicons-row3">We are committed to building trust among our collaborators, and earning the trust of our clients</p>
        </th>
    </tr>
</table>



<!-- block 2 -->
<div id="careers-block2-title">EMPLOYEE SHORT QUOTE</div>
<div id="block2-middleBoxes_bigContainer">
    <div id="block2-middleBoxes_middleContainer">
        <div id="block2-middleBoxes_middleContainer_quote">
            <i class="demo-icon icon-quote-right-alt">&#xe801;</i>
        </div>
        <div id="block2-middleBoxes_middleContainer_box">
            <img src="<?php bloginfo('template_url'); ?>/images/careers/hipster.png" id="male-hipster">
            <img src="<?php bloginfo('template_url'); ?>/images/careers/fem_hipster.png" id="female-hipster">
        </div>
    </div>
    <div id="block2-middleBoxes">
        <div id="block2-middleBoxes_leftContainer" class="middleBoxes_text_holders">
            <div id="block2-middleBoxes_leftTopBorder"></div>
            <div id="block2-middleBoxes_leftLeftBorder"></div>
            <div id="block2-middleBoxes_leftText">
                <div></div>
                <p></p>
            </div>
            <div id="block2-middleBoxes_leftRightBorder"></div>
            <div id="block2-middleBoxes_leftBottomBorder"></div>
        </div>
        <div id="block2-middleBoxes_rightContainer" class="middleBoxes_text_holders">
            <div id="block2-middleBoxes_rightTopBorder"></div>
            <div id="block2-middleBoxes_rightLeftBorder"></div>
            <div id="block2-middleBoxes_rightText">
                <div></div>
                <p></p>
            </div>
            <div id="block2-middleBoxes_rightRightBorder"></div>
            <div id="block2-middleBoxes_rightBottomBorder"></div>
        </div>
    </div>
</div>


<!-- block 3 -->
<?php
//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($block2, '{block2-title}', '{/block2-title}');
$block2title = $stringManipulator->neededSubString;
//$block1 = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($block2, '{block2-content}', '{/block2-content}');
$block2content = $stringManipulator->neededSubString;
$block2 = $stringManipulator->reducedString;


$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($block3, '{block3-title}', '{/block3-title}');
$block3title = $stringManipulator->neededSubString;
//$block1 = $stringManipulator->reducedString;
$stringManipulator->stringExtractAndDelete($block3, '{block3-content}', '{/block3-content}');
$block3content = $stringManipulator->neededSubString;
$block3 = $stringManipulator->reducedString;
?>
<div id="careers-block3-title"><?php echo $block2title; ?></div>
<div id="careers-block3-text"><?php echo $block2content; ?></div>

<table id="careers-block3-table">
    <tr>
        <th class="careers-block3-table-tit-row">JOB TITLE</th>
        <th class="careers-block3-table-tit-row careers-block3-table-langandloc">LANGUAGE</th>
        <th class="careers-block3-table-tit-row careers-block3-table-langandloc">LOCATION</th>
    </tr>
<?php

// get content of the bottom red line
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

echo "<div id='questions_and_answers' style='display: none;'>".$post->post_content."</div>";

// get the jobs posts
$args = array('post_type' => 'jobs', 'post_per_page' => -1);
$loop = new WP_Query($args);

if($loop->have_posts()) {
    foreach ($loop->posts as $job) {
        // check for visibility
        $jobVisibility = get_post_meta($job->ID, 'visibility', true);
        $jobLocation = get_post_meta($job->ID, 'location', true);
        $jobLanguages = get_post_meta($job->ID, 'languages', true);
        if ($jobVisibility == "visible" || $jobVisibility == "") {
?>
            <tr>
                <th><a href="<?php echo get_permalink($job->ID); ?>" class="block3-job-title"><?php echo $job->post_title; ?></a></th>
                <th class="block3-job-language"><?php echo $jobLanguages; ?></th>
                <th class="block3-job-location"><?php echo $jobLocation; ?></th>
            </tr>
<?php
        }
    }
}
?>
</table>



<!-- block 4 -->
<div id="careers-block4-title"><?php echo $block3title; ?></div>
<div id="careers-block4-text"><?php echo $block3content; ?></div>

<!-- footer -->
<?php

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();
