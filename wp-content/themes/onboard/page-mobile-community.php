<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(188);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(188);
$header->printHTML();

$siteURL = get_site_url();
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">facts</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/ff_768.jpg" id="topBanner">

<?php
    $stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
    $title = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
    $block1 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
    $block2 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
    $block3 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
    $block4 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{links}', '{/links}');
    $allLinksContent = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
?>

    <div id="facts-community-container">

        <!-- title of the page -->
        <div id="facts-title"><?php echo $title; ?></div>

        <ul id="facts-community-ul1">
            <li><?php echo $block1; ?></li>
            <li><?php echo $block2; ?></li>
            <li><?php echo $block3; ?></li>
            <li><?php echo $block4; ?></li>
        </ul>

        <img src="<?php bloginfo('template_url'); ?>/images/facts/Logo_UN.png"  id="facts-community-img">

        <ul id="facts-community-ul2">
<?php
            $numberOfLinks = preg_match_all('/\bsingle-link\b/', $allLinksContent);
            for($x = 0; $numberOfLinks/2 > $x; $x++){
                $stringManipulator->stringExtractAndDelete($allLinksContent, '{single-link}', '{/single-link}');
                $tempLink = $stringManipulator->neededSubString;
                $allLinksContent = $stringManipulator->reducedString;
                echo '<li class="facts-community-ul-links">'.$tempLink.'</li>';
            }
?>
        </ul>

    </div>

    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();