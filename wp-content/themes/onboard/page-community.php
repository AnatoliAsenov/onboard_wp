<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include 'templates/BottomRedLine.php';
include 'string_manipulation/StringManipulation.php';

get_header();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">facts</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/facts/facts.jpg" id="topBanner">


<?php
$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
$block4 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{links}', '{/links}');
$allLinksContent = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>

    <!-- title of the page -->
    <div id="technology-title"><?php echo $title; ?></div>

    <div id="facts-main-container">
        <ul id="facts-community-ul">
            <li><?php echo $block1; ?></li>
            <li><?php echo $block2; ?></li>
            <li><?php echo $block3; ?></li>
            <li><?php echo $block4; ?></li>
<?php

    $numberOfLinks = preg_match_all('/\bsingle-link\b/', $allLinksContent);

    for($x = 0; $numberOfLinks/2 > $x; $x++){
        $stringManipulator->stringExtractAndDelete($allLinksContent, '{single-link}', '{/single-link}');
        $tempLink = $stringManipulator->neededSubString;
        $allLinksContent = $stringManipulator->reducedString;
        echo '<li class="facts-community-ul-links">'.$tempLink.'</li>';
    }
?>
        </ul>
        <img src="<?php bloginfo('template_url'); ?>/images/facts/Logo_UN.png"  id="facts-community-img">
    </div>



<?php
// bottom red line content
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();


get_footer();