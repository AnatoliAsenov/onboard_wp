$(document).ready(function() {

    if($("#wwd-container").length){
        var firstSlogan = $("#wwd-firstSlogan").text();
        var secondSlogan = $("#wwd-secondSlogan").text();

        // first slogan
        var sloganElements = firstSlogan.split("*");
        for(var element in sloganElements){
            $("#block2_onboard_Text").append("<p>"+sloganElements[element]+"</p>");
        }

        // second slogan
        sloganElements = secondSlogan.split("*");
        for(element in sloganElements){
            $("#block2_employee_Text").append("<p>"+sloganElements[element]+"</p>");
        }
    }

    if($("#sl-form").length){
        (function(){

            /////////////////////////////
            /////// objectives dropdown
            ////////////////////////////

            var dropdownUL = $("#socialListening-achieveDropdown");
            var dropdownUL_LIs = $("#socialListening-achieveDropdown li");
            var thePlus = $("#social_listening-plus");
            var clickableLIs = $(".socialListening-achieveLi");
            var achieveInput = $("#socialListening-achieveDropdown-other");
            var okButton = $("#socialListening-achieveDropdown-ok");
            var isDropdownOpen = false;

            $("#sl-form-objectives").click(function(){
                if(isDropdownOpen){

                    // rotate plus
                    thePlus.removeClass('social_listening-plusRotate');
                    // hide dropdown
                    hideDropdown();

                }else{

                    // rotate plus
                    thePlus.addClass('social_listening-plusRotate');
                    // show dropdown
                    dropdownUL.css('display', 'block').animate({height: '290px'}, 700, function(){
                        dropdownUL_LIs.css('display', 'block');
                        isDropdownOpen = true;
                    });

                }
            });

            thePlus.click(function(){
                if(isDropdownOpen){

                    // rotate plus
                    thePlus.removeClass('social_listening-plusRotate');
                    // hide dropdown
                    hideDropdown();

                }else{

                    // rotate plus
                    thePlus.addClass('social_listening-plusRotate');
                    // show dropdown
                    dropdownUL.css('display', 'block').animate({height: '294px'}, 700, function(){
                        dropdownUL_LIs.css('display', 'block');
                        isDropdownOpen = true;
                    });

                }
            });

            clickableLIs.click(function(){
                var clicked = $(this);
                clickableLIs.removeClass('socialListening-achieveLi-clicked');
                clicked.addClass('socialListening-achieveLi-clicked');
            });

            achieveInput.click(function(){
                clickableLIs.removeClass('socialListening-achieveLi-clicked');
            });

            okButton.click(function(){
                // set hidden input with the value of selected option
                if(dropdownUL_LIs.hasClass('socialListening-achieveLi-clicked')){
                    dropdownUL_LIs.each(function(){
                        if( $(this).hasClass('socialListening-achieveLi-clicked') ){
                            $("#objectivesHiddenInput").val( $(this).text() );
                        }
                    });
                }else {


                    if (achieveInput.val() != '' && achieveInput.val() != ' ') {
                        $("#objectivesHiddenInput").val(achieveInput.val());
                    } else {
                        alert('Select an option or close dropdown with clicking on "X" !');
                        return false;
                    }

                }
                // rotate plus
                thePlus.removeClass('social_listening-plusRotate');
                // hide dropdown
                hideDropdown();
                $("#socialListening-achieveText").text( $("#objectivesHiddenInput").val() );
            });

            function hideDropdown(){
                dropdownUL_LIs.css('display', 'none');
                dropdownUL.animate({height: 0}, 700, function(){
                    dropdownUL.css('display', 'none');
                    isDropdownOpen = false;
                });
            }

            ////////////////////////////////
            /////// influencers and sources
            ///////////////////////////////

            var hiddenInput = $("#socialListeningSourcesInput");
            var socialAccounts = $("#social_listening-sources-2");
            var URLs = $("#social_listening-sources-1");
            var socialAccountsIsActive = false;
            var URLsIsActive = false;
            var theInputValue = "not set";

            socialAccounts.click(function(){
                if(socialAccountsIsActive){
                    // set it to not active
                    socialAccountsIsActive = false;
                    socialAccounts.removeClass('socialListeningSourcesActive');
                }else{
                    // set it to active
                    socialAccountsIsActive = true;
                    socialAccounts.addClass('socialListeningSourcesActive');
                }
            });

            URLs.click(function(){
                if(URLsIsActive){
                    // set it to not active
                    URLsIsActive = false;
                    URLs.removeClass('socialListeningSourcesActive');
                }else{
                    // set it to active
                    URLsIsActive = true;
                    URLs.addClass('socialListeningSourcesActive');
                }
            });

            function setValueOfHiddenInput(){

                if(URLsIsActive || socialAccountsIsActive){
                    theInputValue = "";
                }else{
                    theInputValue = "not set";
                }

                if(URLsIsActive){
                    theInputValue = "URLs";
                }

                if(socialAccountsIsActive){
                    if(URLsIsActive){
                        theInputValue = theInputValue + " AND ";
                    }
                    theInputValue = theInputValue + "Social Accounts";
                }

                hiddenInput.val( theInputValue );
            }



            $("#socialListeningForm-submit").click(function(){
                setValueOfHiddenInput();
                $('socialListeningFormSend').click();
            });


            ///////////////////////
            //// free demo button
            //////////////////////
            var compensationHeight = 256;

            $("#sl-block1-button").click(function(){
                //if(animationBlock2Active){
                //    compensationHeight = 0;
                //}
                var titleTopOffset = $("#sl-the-tit").offset().top;
                $("html, body").animate({
                    scrollTop: (titleTopOffset - compensationHeight)
                }, 1500);
            });

        }());
    }

});
