<?php


class MobileHeader
{
    private $id = 0;

    public function __construct($postID){
        $this->id = $postID;
    }

    public function printHTML(){

        $slug = basename(get_permalink());
        $siteURL = get_site_url();

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> >

<head>

    <script>
        if( document.documentElement.clientWidth > 1000 ){
            window.location = "<?php echo $siteURL; ?>" + "/home" + "<?php //echo $slug; ?>";
        }
    </script>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-T9QNRDX');</script>
    <!-- End Google Tag Manager -->

    <meta content='text/html' charset='UTF-8' http-equiv='Content-Type' />
    <meta name="viewport" content="width=device-width" />

    <!-- title -->
    <title><?php bloginfo('name'); ?></title>

    <!-- meta tags -->
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <meta name="keywords" content="B2B sales, Salesforce.com, CRM Consultancy, telemarketing, digital marketing services, customer relationship, outsourcing, data-driven marketing, channel sales, data quality services, global coverage, CRM automation, business development, market entry, Lead generation, Demand generation, B2B sales cycle, Marketing technology, Business process platform" />
    <meta name="robots" content="index, follow" />

    <!-- fonts -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css' />

    <!-- fav icon -->
    <link rel="icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />

    <!-- styles -->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_loadingAnimation.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_fonts.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/lib/slick.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/lib/slick-theme.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_sliders.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_header.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_menu.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_home.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_blog.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_singlePost.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_about.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_company.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_careers.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_case-studies.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_contacts.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_facts.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_whatwedo.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_technology.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_bottomRedLine.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_footer.css" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/mobile_css/_popUps.css" />

    <!-- Google+ id -->
    <link rel="publisher" href="https://plus.google.com/+your_business_google_plus_id">

    <!-- FaceBook Open Graph -->
    <meta property="fb:app_id" content="100004612067691"/>
    <meta property="og:url" content="<?php echo get_post_meta($this->id, 'fb_url', true); ?>" />
    <meta property="og:type" content="<?php echo get_post_meta($this->id, 'fb_type', true); ?>" />
    <meta property="og:title" content="<?php echo get_post_meta($this->id, 'fb_title', true); ?>" />
    <meta property="og:image" content="<?php echo get_post_meta($this->id, 'fb_image_url', true); ?>" />
    <meta property="og:description" content="<?php echo get_post_meta($this->id, 'fb_description', true); ?>" />


    <!-- Twitter Card -->
    <meta name="twitter:card" content="<?php echo get_post_meta($this->id, 'tw_type', true); ?>" />
    <meta name="twitter:site" content="<?php echo get_post_meta($this->id, 'tw_site', true); ?>" />
    <meta name="twitter:title" content="<?php echo get_post_meta($this->id, 'tw_title', true); ?>" />
    <meta name="twitter:description" content="<?php echo get_post_meta($this->id, 'tw_description', true); ?>" />
    <meta name="twitter:image" content="<?php echo get_post_meta($this->id, 'tw_image_url', true); ?>" />
<?php if( get_post_meta($this->id, 'tw_type', true) == "summary_large_image" ){ ?>
    <meta name="twitter:creator" content="<?php echo get_post_meta($this->id, 'tw_creator', true); ?>" />
    <meta name="twitter:image:alt" content="<?php echo get_post_meta($this->id, 'tw_image_alt', true); ?>" />
<?php }

if( get_post_meta($this->id, 'tw_type', true) == "app" ){ ?>
    <meta name="twitter:app:country" content="<?php echo get_post_meta($this->id, 'tw_country', true); ?>" />
    <meta name="twitter:app:name:iphone" content="<?php echo get_post_meta($this->id, 'tw_name_iphone', true); ?>" />
    <meta name="twitter:app:id:iphone" content="<?php echo get_post_meta($this->id, 'tw_id_iphone', true); ?>" />
    <meta name="twitter:app:url:iphone" content="<?php echo get_post_meta($this->id, 'tw_url_iphone', true); ?>" />
    <meta name="twitter:app:name:ipad" content="<?php echo get_post_meta($this->id, 'tw_name_ipad', true); ?>" />
    <meta name="twitter:app:id:ipad" content="<?php echo get_post_meta($this->id, 'tw_id_ipad', true); ?>" />
    <meta name="twitter:app:url:ipad" content="<?php echo get_post_meta($this->id, 'tw_url_ipad', true); ?>" />
    <meta name="twitter:app:name:googleplay" content="<?php echo get_post_meta($this->id, 'tw_name_google', true); ?>" />
    <meta name="twitter:app:id:googleplay" content="<?php echo get_post_meta($this->id, 'tw_id_google', true); ?>" />
    <meta name="twitter:app:url:googleplay" content="<?php echo get_post_meta($this->id, 'tw_url_google', true); ?>" />
<?php }

if( get_post_meta($this->id, 'tw_type', true) == "player" ){ ?>
    <meta name="twitter:player" content="<?php echo get_post_meta($this->id, 'tw_video_url', true); ?>" />
    <meta name="twitter:player:width" content="<?php echo get_post_meta($this->id, 'tw_video_width', true); ?>" />
    <meta name="twitter:player:height" content="<?php echo get_post_meta($this->id, 'tw_video_height', true); ?>" />
    <meta name="twitter:player:stream" content="<?php echo get_post_meta($this->id, 'tw_video_stream', true); ?>" />
    <meta name="twitter:player:stream:content_type" content="<?php echo get_post_meta($this->id, 'tw_video_stream_content', true); ?>" />
<?php } ?>



    <!-- TiDiO -->
    <script src="//code.tidio.co/f1o91z5fmeru5tyurnbveyamblxy8m3j.js"></script>
    <script>
        // show/hide chat icon
        //tidioChatApi.method('chatDisplay', false);
    </script>

    </head>
    <body <?php body_class(); ?> >

        <!-- Google Tag Manager (noscript) -->
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9QNRDX" height="0" width="0" style="display:none;visibility:hidden"></iframe>
        </noscript>
        <!-- End Google Tag Manager (noscript) -->

        <!-- loading animation -->
        <div id="loadingContainer">
            <img src="<?php bloginfo('template_url'); ?>/images/logos/onboard_logo.png">
        </div>


        <!-- popup settings -->
        <div id="popup-settings" data-visibility="<?php echo get_post_meta($this->id, 'visibility', true); ?>" data-time="<?php echo get_post_meta($this->id, 'time_to_show', true); ?>"></div>


        <!-- tracking pixels -->
        <div id="pageNameDiv" data-pagename="<?php echo $slug; ?>"><?php echo $slug; ?></div>
        <div id="trackingPixel_middle"></div>
        <div id="trackingPixel_bottom"></div>


        <!--------- header begin ---------->
        <!-- top black line -->
        <div id="header-topBlackLine">
            <div id="header-inner-topBlackLine">
                <a id="header-iso" href="<?php echo $siteURL."/technology/credentials/"; ?>">ISO 9001:2008 / 27001:2005</a>
                <a class="callUsAndChat inner-call-chat-icons" href="#" id="live-chat-topmenu-btn">
                    <div class="icon icon-chat-1" style="display: inline-block;"></div>
                    <p style="display: inline-block;">Live chat</p>
                </a>
                <a class="callUsAndChat inner-call-chat-icons" href="<?php echo $siteURL."/mobile/mobile-contacts"; ?>">
                    <div data-icon="g" class="contacts_icons icon" style="display:inline-block;"></div>
                    <p style="display: inline-block;">Call us</p>
                </a>
                <div id="header-social">
                    <a class="topLineLinks" target="_blank" href="https://www.facebook.com/OnboardCRM">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a class="topLineLinks" target="_blank" href="https://www.linkedin.com/company/onboardcrm?trk=company_logo">
                        <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                    </a>
                    <a class="topLineLinks" target="_blank" href="https://twitter.com/Onboardcrm">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a class="topLineLinks" target="_blank" href="https://www.instagram.com/onboardcrm/">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        <!-- logo and menu -->
        <div id="logoAndMenuContainer" class="logoAndMenu_begin">
            <!-- logo image -->
            <img src="<?php bloginfo('template_url'); ?>/images/topMenu/logo.png" id="header-logo"/>
            <!-- menu icon -->
            <div id="header-menu-icon" data-icon="i" class="icon"></div>
            <!-- grey line -->
            <div id="header-grey-line"></div>
        </div>
        <!---------- header end ----------->


        <!-- MENU -->
        <div id="site-menu">
            <div id="site-menu-inner">
                <a href="<?php echo $siteURL.'/mobile/mobile-home'; ?>" class="site-menu-a">HOME</a>
                <div class="site-submenu">
                    <a href="<?php echo $siteURL.'/mobile/mobile-whatwedo'; ?>" class="site-menu-a-plus">WHAT WE DO</a>
                    <div id="header-submenu-whatwedo" class="site-menu-a-plus-icon icon" data-icon="i"></div>
                </div>
                <div id="submenu-whatwedo-content">
                    <a href="<?php echo $siteURL.'/mobile/mobile-market-entry'; ?>" class="submenu-whatwedo-a">Worldwide Market Entry</a>
                    <a href="<?php echo $siteURL.'/mobile/mobile-marketing-services'; ?>" class="submenu-whatwedo-a">Digital Marketing Services</a>
                    <a href="<?php echo $siteURL.'/mobile/mobile-database-services'; ?>" class="submenu-whatwedo-a">Database Services</a>
                    <a href="<?php echo $siteURL.'/mobile/mobile-b2b'; ?>" class="submenu-whatwedo-a">B2B Marketing & Sales</a>
                    <a href="<?php echo $siteURL.'/mobile/mobile-business-process-enablement'; ?>" class="submenu-whatwedo-a">Business Process Enablement Platform</a>
                </div>
                <div class="site-submenu">
                    <a href="<?php echo $siteURL.'/mobile/mobile-technology'; ?>" class="site-menu-a-plus">TECHNOLOGY</a>
                    <div id="header-submenu-technology" class="site-menu-a-plus-icon icon" data-icon="i"></div>
                </div>
                <div id="submenu-technology-content">
                    <a href="<?php echo $siteURL.'/mobile/mobile-salesforce'; ?>" class="submenu-technology-a">SalesForce</a>
                    <a href="<?php echo $siteURL.'/mobile/mobile-gsuite'; ?>" class="submenu-technology-a">G Suite</a>
                    <a href="<?php echo $siteURL.'/mobile/mobile-eloqua'; ?>" class="submenu-technology-a">Eloqua</a>
                    <a href="<?php echo $siteURL.'/mobile/mobile-salesmanago'; ?>" class="submenu-technology-a">Sales Manago</a>
                    <a href="<?php echo $siteURL.'/mobile/mobile-credentials'; ?>" class="submenu-technology-a">Credentials</a>
                </div>
                <a href="<?php echo $siteURL.'/mobile/mobile-cases'; ?>" class="site-menu-a">CASES</a>
                <a href="<?php echo $siteURL.'/mobile/mobile-blog'; ?>" class="site-menu-a">BLOG</a>
                <a href="<?php echo $siteURL.'/mobile/mobile-careers'; ?>" class="site-menu-a">CAREERS</a>
                <a href="<?php echo $siteURL.'/mobile/mobile-about'; ?>" class="site-menu-a">ABOUT</a>
                <a href="<?php echo $siteURL.'/mobile/mobile-contacts'; ?>" class="site-menu-a">CONTACTS</a>

                <hr id="menu-horizontal-line">

                <a href="<?php echo $siteURL.'/mobile/mobile-company'; ?>" class="site-menu-a">Company</a>
                <a href="<?php echo $siteURL.'/mobile/mobile-whyonboard'; ?>" class="site-menu-a">Why Onboard</a>
                <a href="<?php echo $siteURL.'/mobile/mobile-partnership'; ?>" class="site-menu-a">Partnership Advantages</a>
                <a href="<?php echo $siteURL.'/mobile/mobile-community'; ?>" class="site-menu-a">Community</a>
                <a href="<?php echo $siteURL.'/mobile/mobile-data-privacy'; ?>" class="site-menu-a">Data Privacy</a>

                <div id="menu-social">
                    <a class="topLineLinks" target="_blank" href="https://www.facebook.com/OnboardCRM">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a class="topLineLinks" target="_blank" href="https://www.linkedin.com/company/onboardcrm?trk=company_logo">
                        <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                    </a>
                    <a class="topLineLinks" target="_blank" href="https://twitter.com/Onboardcrm">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                    <a class="topLineLinks" target="_blank" href="https://www.instagram.com/onboardcrm/">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>

<?php
    }
}