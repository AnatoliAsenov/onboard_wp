<?php


class MobileFooter
{

    public function __construct(){}

    public function printHTML(){
?>
        <!-- Footer -->
        <div id="footer">
            <div id="footer_social_icons">
                <a class="footerSocialLinks" target="_blank" href="https://www.facebook.com/OnboardCRM">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
                <a class="footerSocialLinks" target="_blank" href="https://www.linkedin.com/company/onboardcrm?trk=company_logo">
                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                </a>
                <a class="footerSocialLinks" target="_blank" href="https://twitter.com/Onboardcrm">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
                <a class="footerSocialLinks" target="_blank" href="https://www.instagram.com/onboardcrm/" style="margin-right:0;">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
            </div>
            <p>Copyright &copy; <?php echo date("Y"); ?> Onboardcrm. All Rights reserved.</p>
        </div>


        <!-- pop ups -->
        <div id="popUps">
            <!-- let us call you -->
            <!--div id="popup-let-us">
                <form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads1239484000010844070 method='POST' id="popup-let-us-form" accept-charset='UTF-8' >
                    <div id="popup-let-us-tit">Let us call you?</div>
                    <i class="fa fa-plus-circle closePopUp" aria-hidden="true"></i>
                    <div id="popup-let-us-inputs">
                        <input type="email" id="let-us-email" placeholder="email"/>
                        <input type="number" id="let-us-phone" placeholder="phone"/>
                    </div>
                    <div data-icon="n" class="icon"></div>
                    <textarea placeholder="Your question..."></textarea>
                    <div id="popup-let-us-send">SEND</div>
                </form>
            </div-->

            <div id="thankYouPopUp">
                <p>Thank you!</p>
                <p>We will connect with you ASAP.</p>
            </div>

            <!-- how can we help you -->
            <div id="popup-how-can">
                <form  action="" method='POST' accept-charset='UTF-8' id="popup-how-can-form" >
                    <div id="popup-how-can-tit">Contact Us</div>
                    <i class="fa fa-plus-circle closePopUp" aria-hidden="true"></i>
                    <div id="popup-how-can-inputs">
                        <input type='text' style='display:none;' name='device' value='mobile' />
                        <input type="text" id="how-can-name" placeholder="First Name" name="First Name"/>
                        <input type="text" id="how-can-name" placeholder="Last Name" name="Last Name"/>
                        <input type="email" id="how-can-email" placeholder="Email" name='Email'/>
                        <input type="text" id="how-can-company" placeholder="Company" name='Company'/>
                        <input type="number" id="how-can-phone" placeholder="Phone" name='Phone'/>
                    </div>
                    <div data-icon="n" class="icon"></div>
                    <textarea placeholder="Your question..." name='Description'></textarea>
                    <input type="submit" style="display:none;" id="contact_us_submit_btn"/>
                    <div id="popup-how-can-send">SEND</div>
                </form>
            </div>

            <!-- download pdf -->
            <div id="popup-download-pdf">
                <form action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads1239484000010856034 method='POST' accept-charset='UTF-8' id="popup-download-pdf-form">
                    <div id="popup-download-pdf-tit">Download full version as pdf</div>
                    <i class="fa fa-plus-circle closePopUp" aria-hidden="true"></i>
                    <div id="popup-download-pdf-inner">Please, enter your details in order to download the full version of this case studiy.</div>
                    <div id="popup-download-pdf-inputs">
                        <input type="email" id="download-pdf-email" placeholder="email"/>
                        <input type="number" id="download-pdf-phone" placeholder="phone"/>
                    </div>
                    <div data-icon="n" class="icon"></div>
                    <textarea placeholder="Your question..."></textarea>
                    <input type="submit" style="display:none;" id="contact_us_submit_btn"/>
                    <div id="popup-download-pdf-send">SEND</div>
                </form>
            </div>

        </div>


        <!-- scripts -->
		<script src="<?php bloginfo('template_url'); ?>/js/lib/jquery-3.1.1.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/mobile_js/_getScreenDimensions.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/lib/jquery.placeholder.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/lib/countUp.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/lib/jquery-color.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/lib/mixitup.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/js/lib/slick.min.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_home.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_menu.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/mobile_js/_loadingAnimation.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/mobile_js/_blog.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_singlePost.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_about.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_careers.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_cases.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_whatwedo.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_sliders.js"></script>
        <script src="<?php bloginfo('template_url'); ?>/mobile_js/_popUps.js"></script>

<?php


	if(is_page('case-studies')){ ?>

<!--        <script src="--><?php //bloginfo('template_url'); ?><!--/js/casestudies/casestudies.js"></script>-->
<?php
    }
?>


<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
    <script type="text/javascript">
        /* <![CDATA[ */
        var google_conversion_id = 1066581569;
        var google_custom_params = window.google_tag_params;
        var google_remarketing_only = true;
        /* ]]> */
    </script>
    <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
    <noscript>
        <div style="display:inline;">
            <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1066581569/?guid=ON&amp;script=0"/>
        </div>
    </noscript>


    <!-- Google Universal Analytics -->
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-2082072-3', 'auto');
        ga('send', 'pageview');

    </script>



    <!-- sharp spring tracking code -->
    <script type="text/javascript">
        var _ss = _ss || [];
        _ss.push(['_setDomain', 'https://koi-3QNB9GWLJ6.marketingautomation.services/net']);
        _ss.push(['_setAccount', 'KOI-3UOYS7U91K']);
        _ss.push(['_trackPageView']);
        (function() {
            var ss = document.createElement('script');
            ss.type = 'text/javascript'; ss.async = true;

            ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNB9GWLJ6.marketingautomation.services/client/ss.js?ver=1.1.1';
            var scr = document.getElementsByTagName('script')[0];
            scr.parentNode.insertBefore(ss, scr);
        })();
    </script>
    <script type="text/javascript">
        var __ss_noform = [];
        __ss_noform.push(['baseURI', 'https://app-3QNB9GWLJ6.marketingautomation.services/webforms/receivePostback/MzawMDEzMjAzBwA/']);
        __ss_noform.push(['submitType', 'manual']);
        __ss_noform.push(['form', 'popup-how-can-form', '01229ed3-1b4d-4bd1-9b47-f0993a4103f0']);
    </script>
    <script type="text/javascript" src="https://koi-3QNB9GWLJ6.marketingautomation.services/client/noform.js?ver=1.24" ></script>



<!-- albacross tracking code -->
<script type="text/javascript">
	(function(a,l,b,c,r,s){_nQc=c,r=a.createElement(l),s=a.getElementsByTagName(l)[0];r.async=1;
		r.src=l.src=("https:"==a.location.protocol?"https://":"http://")+b;s.parentNode.insertBefore(r,s);
	})(document,"script","serve.albacross.com/track.js","89960742");
</script>


<!-- Google Structured Data -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Onboard",
  "url": "http://onboardcrm.com",
  "sameAs": [
    "http://www.facebook.com/profile.php?id=100004612067691",
    "http://www.linkedin.com/company-beta/391328/",
    "https://twitter.com/Onboardcrm"
  ]
}
</script>

</body>
</html>

<?php
    }
}