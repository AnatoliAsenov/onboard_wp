/**
 * Created by Anatoli Asenov
 * Onboard Bulgaria
 */

$(document).ready(function() {
    if($("#facts-community-ul").length){
        var allA = $("#facts-community-ul a");
        var arrowIcon = '<i class="fa fa-share" aria-hidden="true"></i>';
        allA.each(function(){
            $(this).append(arrowIcon);
        });
    }
});
