/**
 * Created by Anatoli Asenov
 */

$(document).ready(function() {

    var pageName = $("#whatisthispage").text();

///////////////////////////////////////////////
//------------- set the active tab ------------
///////////////////////////////////////////////
    (function(){

        var pageName_lowerCase = pageName.toLowerCase();
        var topMenuElements = $("#primary_nav a");

        topMenuElements.each(function(){
            var actualElement = $(this).data("pagename");
            if(actualElement == pageName_lowerCase){
                var parent = $(this).parent();
                parent.addClass("current");
            }
        });

    }());

///////////////////////////////////////////////
//------------ WHAT WE DO dropdown ------------
///////////////////////////////////////////////
    (function(){
        var topMenuElement = $("#whatWeDoElement");
        var theDropDown = $("#whatWeDo_dropdown");

    //_______________ position the dropbox ______________
        var topMenuElementOffset = topMenuElement.offset();
        theDropDown.css({
            top: topMenuElementOffset.top + topMenuElement.height() + "px",
            left: topMenuElementOffset.left + "px"
        });

    //_______________ hover functions ______________
        var topElementHovered = false;
        var dropdownHovered = false;
        topMenuElement.hover(function(){
            // hover in
            hideTheDropdowns("technology");
            topElementHovered = true;
            topMenuElement.css({
                backgroundColor: "#EB5C4E",
                color: "#ffffff"
            });
            theDropDown.css("display", "block").animate({
                opacity: 0.85
            }, 200);
        }, function(){
            // hover out
            topElementHovered = false;
            setTimeout(hideDropdown, 1000);
        });

        theDropDown.hover(function(){
            // hover in
            dropdownHovered = true;
        }, function(){
            // hover out
            dropdownHovered = false;
            if(topElementHovered == false) {
                setTimeout(hideDropdown, 1000);
            }
        });

        var hideDropdown = function(){
            if(topElementHovered == false && dropdownHovered == false){
                theDropDown.animate({
                    opacity: 0
                }, 200, function(){
                    theDropDown.css("display", "none");
                    if(pageName != "whatwedo") {
                        topMenuElement.css({
                            backgroundColor: "#ffffff",
                            color: "#42474a"
                        });
                    }
                });
            }
        };
    }());


    ///////////////////////////////////////////////
//------------ TECHNOLOGY dropdown ------------
///////////////////////////////////////////////
    (function(){
        var topMenuElement = $("#technologyElement");
        var theDropDown = $("#technology_dropdown");

        //_______________ position the dropbox ______________
        var topMenuElementOffset = topMenuElement.offset();
        theDropDown.css({
            top: topMenuElementOffset.top + topMenuElement.height() + "px",
            left: topMenuElementOffset.left + "px"
        });

        //_______________ hover functions ______________
        var topElementHovered = false;
        var dropdownHovered = false;
        topMenuElement.hover(function(){
            // hover in
            hideTheDropdowns("whatwedo");
            topElementHovered = true;
            topMenuElement.css({
                backgroundColor: "#EB5C4E",
                color: "#ffffff"
            });
            theDropDown.css("display", "block").animate({
                opacity: 0.85
            }, 200);
        }, function(){
            // hover out
            topElementHovered = false;
            setTimeout(hideDropdown, 1000);
        });

        theDropDown.hover(function(){
            // hover in
            dropdownHovered = true;
        }, function(){
            // hover out
            dropdownHovered = false;
            if(topElementHovered == false) {
                setTimeout(hideDropdown, 1000);
            }
        });

        var hideDropdown = function(){
            if(topElementHovered == false && dropdownHovered == false){
                theDropDown.animate({
                    opacity: 0
                }, 200, function(){
                    theDropDown.css("display", "none");
                    if(pageName != "technology") {
                        topMenuElement.css({
                            backgroundColor: "#ffffff",
                            color: "#42474a"
                        });
                    }
                });
            }
        };
    }());


    function hideTheDropdowns(n){
        var technologyTopMenuElement = $("#technologyElement");
        var technologyTheDropDown = $("#technology_dropdown");
        var whatwedoTopMenuElement = $("#whatWeDoElement");
        var whatwedoTheDropDown = $("#whatWeDo_dropdown");

        switch (n){
            case "both":
                whatwedoTheDropDown.css("display", "none");
                if(pageName != "whatwedo") {
                    whatwedoTopMenuElement.css({
                        backgroundColor: "#ffffff",
                        color: "#42474a"
                    });
                }
                technologyTheDropDown.css("display", "none");
                if(pageName != "technology") {
                    technologyTopMenuElement.css({
                        backgroundColor: "#ffffff",
                        color: "#42474a"
                    });
                }
                break;
            case "whatwedo":
                whatwedoTheDropDown.css("display", "none");
                if(pageName != "whatwedo") {
                    whatwedoTopMenuElement.css({
                        backgroundColor: "#ffffff",
                        color: "#42474a"
                    });
                }
                break;
            case "technology":
                technologyTheDropDown.css("display", "none");
                if(pageName != "technology") {
                    technologyTopMenuElement.css({
                        backgroundColor: "#ffffff",
                        color: "#42474a"
                    });
                }
                break;
        }
    }


    //_________________ hide dropdown if scroll ______________
    $(window).scroll(function(){
        hideTheDropdowns("both")
    });


    //______________ hide loading animation _______________
    var hideLoadingAnimation = function(){
        $("#loadingContainer").animate({
            opacity: 0
        }, 300, function(){
            $(this).css("display", "none");
        });
    };

    if(pageName != "home"){
        setTimeout(hideLoadingAnimation, 1200);
    }

    //______________ show chat after click ot top btn _______________
    $("#live-chat-topmenu-btn").click(function(){
        tidioChatApi.method('popUpOpen');
    });
});