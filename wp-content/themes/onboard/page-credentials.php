<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include "templates/MiddlePageLine.php";
include "templates/BottomRedLine.php";
include 'string_manipulation/StringManipulation.php';

get_header();

$siteURL = get_site_url();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">technology</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/technology/technology.jpg" id="topBanner">


<?php
$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
$block4 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

?>

    <!-- title of page -->
    <h1 id="technology-title"><?php echo $title; ?></h1>

    <!-- first part -->
    <ul id="technology-firstPart">
        <li id="technology-firstPart-li1">
            <p><?php echo $block1; ?></p>
            <p><?php echo $block2; ?></p>

            <a href="<?php echo $siteURL."/technology/credentials/quality-of-services"; ?>" class="technology-firstPart-li1-a">
<!--                <div class="icon icon-download"></div>-->
                <div data-icon="c" class="icon"></div>
                <p><?php echo $block3; ?></p>
            </a>
            <a href="<?php echo $siteURL."/technology/credentials/information-security"; ?>" class="technology-firstPart-li1-a">
                <div data-icon="c" class="icon"></div>
                <p><?php echo $block4; ?></p>
            </a>
        </li>
        <li id="technology-firstPart-li2">
            <img src="<?php bloginfo('template_url'); ?>/images/technology/iso-icon.png" id="technology-credentials-img">
        </li>
    </ul>

    <div id="technology-credentials-buffer"></div>



<?php
// bottom red line content
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();