/**
 * Created by Anatoli Asenov
 * Onboard Bulgaria
 */

$(document).ready(function() {
    if($("#contacts-page-office-list").length){
        //
        $(".offices-list-li").hover(function(){
            $(this).find(".offices-list-image").addClass("offices-list-animation");
            $(this).find(".offices-list-mask").animate({opacity: "0.9"}, 900);
        }, function(){
            $(this).find(".offices-list-image").removeClass("offices-list-animation");
            $(this).find(".offices-list-mask").animate({opacity: "1"}, 500);
        });


        //=================================================
        //========== Open TiDiO chat via click ============
        //=================================================
        //$("#talkToExpert").click(function(){
        //    tidioChatApi.method('popUpOpen');
        //});
    }
});