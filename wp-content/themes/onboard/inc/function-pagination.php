<?php
/**
 * Created by Onboard
 * User: AnatoliAsenov
 */

//  =========================================
//              pagination
//  =========================================

function pagination($pages = '', $range = 4)
{
    $showitems = ($range * 2)+1;

    global $paged;
    if(empty($paged)) $paged = 1;

    if($pages == '')
    {
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }

    if(1 != $pages)
    {
        echo "<div class='pagination blog-pagination-container'>";
        echo "<span class='blog-pagination-ww'>Page ".$paged." of ".$pages."</span>";
        echo "<div class='blog-page-numbers'>";
        for ($i=1; $i <= $pages; $i++)
        {
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
            {
                echo ($paged == $i)? "<span class='current-blog-pages'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive-blog-pages'>".$i."</a>";
            }
        }

        echo "</div></div>";
    }
}