<?php

/**
 * Created by Onboard
 * User: AnatoliAsenov
 */

class SocialMetaboxes
{

    public function __construct(){}

    public function fb_linkedin_googlePlus($postType){

        $meta_boxes = array(
            'title'      => __( '<span style="font-weight:bold;color:#3b5998;">Facebook, LinkedIn and <span style="color: #ff0000;">Google+</span> meta tags</span>', 'textdomain' ),
            'post_types' => $postType,
            'fields'     => array(
                array(
                    'id'   => 'fb_url',
                    'name' => __( 'URL:', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'fb_type',
                    'name' => __( 'Type:', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'fb_title',
                    'name' => __( 'Title:', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'fb_image_url',
                    'name' => __( 'Image URL:', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'fb_description',
                    'name' => __( 'Description:', 'textdomain' ),
                    'type' => 'textarea',
                ),
            ),
        );
        return $meta_boxes;
    }

    public function twitter($postType){
        $meta_boxes = array(
            'title'      => __( '<span style="font-weight:bold;color:#4099FF;">Twitter meta tags</span>', 'textdomain' ),
            'post_types' => $postType,
            'fields'     => array(
                array(
                    'id'   => 'tw_type',
                    'name' => __( 'Type:', 'textdomain' ),
                    'type'    => 'radio',
                    'options' => array(
                        'summary' => __( 'Summary<span style="color:#4099FF;font-size:24px;">*</span>', 'textdomain' ),
                        'summary_large_image' => __( 'Summary Large Image<span style="color:#ff0000;font-size:24px;">*</span>', 'textdomain' ),
                        'app' => __( 'App<span style="color:#00ff00;font-size:24px;">*</span>', 'textdomain' ),
                        'player' => __( 'Player<span style="color:#ffba00;font-size:24px;">*</span>', 'textdomain' ),
                    )
                ),
                array(
                    'id'   => 'tw_site',
                    'name' => __( 'Username (@user):<span style="color:#4099FF;font-size:22px;">*</span><span style="color:#ff0000;font-size:22px;">*</span><span style="color:#00ff00;font-size:22px;">*</span><span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_title',
                    'name' => __( 'Title:<span style="color:#4099FF;font-size:22px;">*</span><span style="color:#ff0000;font-size:22px;">*</span><span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_description',
                    'name' => __( 'Description:<span style="color:#4099FF;font-size:22px;">*</span><span style="color:#ff0000;font-size:22px;">*</span><span style="color:#00ff00;font-size:22px;">*</span><span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'textarea',
                ),
                array(
                    'id'   => 'tw_image_url',
                    'name' => __( 'Image URL:<span style="color:#4099FF;font-size:22px;">*</span><span style="color:#ff0000;font-size:22px;">*</span><span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                // large image card
                array(
                    'id'   => 'tw_image_alt',
                    'name' => __( 'Description of the image (alt):<span style="color:#ff0000;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),array(
                    'id'   => 'tw_creator',
                    'name' => __( 'Creator:<span style="color:#ff0000;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                // app card
                array(
                    'id'   => 'tw_country',
                    'name' => __( 'Country:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_name_iphone',
                    'name' => __( 'Name-iPhone:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_id_iphone',
                    'name' => __( 'Id-iPhone:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_url_iphone',
                    'name' => __( 'URL-iPhone:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_name_ipad',
                    'name' => __( 'Name-iPad:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_id_ipad',
                    'name' => __( 'Id-iPad:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_url_ipad',
                    'name' => __( 'URL-iPad:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_name_google',
                    'name' => __( 'Name-GooglePlay:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_id_google',
                    'name' => __( 'Id-GooglePlay:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_url_google',
                    'name' => __( 'URL-GooglePlay:<span style="color:#00ff00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                // video card
                array(
                    'id'   => 'tw_video_url',
                    'name' => __( 'Video URL:<span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_video_width',
                    'name' => __( 'Video width (in pixels):<span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_video_height',
                    'name' => __( 'Video height (in pixels):<span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_video_stream',
                    'name' => __( 'Video stream URL for Android and iOS (same as video url):<span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
                array(
                    'id'   => 'tw_video_stream_content',
                    'name' => __( 'Video format (video/mp4):<span style="color:#ffba00;font-size:22px;">*</span>', 'textdomain' ),
                    'type' => 'text',
                ),
            ),
        );
        return $meta_boxes;

    }
}