<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

    include 'templates/BottomRedLine.php';
    include 'string_manipulation/StringManipulation.php';

    $siteURL = get_site_url();
    $postContent = $post->post_content;
    $stringManipulator = new StringManipulation();

    get_header();
    the_content();
?>
    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">about</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/about/aboutus.jpg" id="topBanner">

<?php
    $stringManipulator->stringExtractAndDelete($postContent, '{title1}', '{/title1}');
    $title1 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
    $stringManipulator->stringExtractAndDelete($postContent, '{sub-title1}', '{/sub-title1}');
    $subtitle1 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
    $stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
    $block1 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{title2}', '{/title2}');
    $title2 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
    $stringManipulator->stringExtractAndDelete($postContent, '{sub-title2}', '{/sub-title2}');
    $subtitle2 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
    $stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
    $block2 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{title3}', '{/title3}');
    $title3 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
    $stringManipulator->stringExtractAndDelete($postContent, '{sub-title3}', '{/sub-title3}');
    $subtitle3 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
    $stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
    $block3 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
?>

    <!-- about table -->
    <table id="about_tab">
        <tr>
            <th class="about_tab_th">
                <a href="<?php echo $siteURL."/company"; ?>" class="about_tab_a">
                    <p class="about_tab_big_tit"><?php echo $title1; ?></p>
                    <p class="about_tab_small_tit"><?php echo $subtitle1; ?></p>
                </a>
            </th>
            <th class="about_tab_v_buffer"></th>
            <th class="about_tab_th">
                <a href="<?php echo $siteURL."/careers"; ?>" class="about_tab_a">
                    <p class="about_tab_big_tit"><?php echo $title2; ?></p>
                    <p class="about_tab_small_tit"><?php echo $subtitle2; ?></p>
                </a>
            </th>
            <th class="about_tab_v_buffer"></th>
            <th class="about_tab_th">
                <a href="<?php echo $siteURL."/facts-and-figures/community"; ?>" class="about_tab_a">
                    <p class="about_tab_big_tit"><?php echo $title3; ?></p>
                    <p class="about_tab_small_tit"><?php echo $subtitle3; ?></p>
                </a>
            </th>
        </tr>
        <tr>
            <th>
                <a href="<?php echo $siteURL."/company"; ?>" class="about_tab_a">
                    <img src="<?php bloginfo('template_url'); ?>/images/about/box1.jpg" class="about_tab_image">
                    <div class="about_tab_text_under_img"><?php echo $block1; ?></div>
                </a>
            </th>
            <th></th>
            <th>
                <a href="<?php echo $siteURL."/careers"; ?>" class="about_tab_a">
                    <img src="<?php bloginfo('template_url'); ?>/images/about/box2.jpg" class="about_tab_image">
                    <div class="about_tab_text_under_img"><?php echo $block2; ?></div>
                </a>
            </th>
            <th></th>
            <th>
                <a href="<?php echo $siteURL."/facts-and-figures/community"; ?>" class="about_tab_a">
                    <img src="<?php bloginfo('template_url'); ?>/images/about/box3.jpg" class="about_tab_image">
                    <div class="about_tab_text_under_img"><?php echo $block3; ?></div>
                </a>
            </th>
        </tr>
        <tr>
            <th colspan="5">
                <table id="inner_tab" style="background-image:url('<?php bloginfo('template_url'); ?>/images/about/map.jpg');">
                    <tr>
                        <th class="inner_tab_h_buffer" colspan="5"></th>
                    </tr>
                    <tr>
                        <th class="about_tab_th inner_tab_num" id="countries_num">0</th>
                        <th class="about_tab_v_buffer"></th>
                        <th class="about_tab_th inner_tab_num" id="offices_num">0</th>
                        <th class="about_tab_v_buffer"></th>
                        <th class="about_tab_th inner_tab_num" id="continents_num">0</th>
                    </tr>
                    <tr>
                        <th class="inner_tab_txt">COUNTRIES</th>
                        <th></th>
                        <th class="inner_tab_txt">OFFICES</th>
                        <th></th>
                        <th class="inner_tab_txt">CONTINENTS</th>
                    </tr>
                    <tr>
                        <th class="inner_tab_h_buffer" colspan="5"></th>
                    </tr>
                </table>
            </th>
        </tr>
    </table>



<!-- footer -->
<?php

    // bottom red line content
    $stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
    $bottomRedLineText = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $subFooter = new BottomRedLine($bottomRedLineText);
    $subFooter->printHTML();

    get_footer();
