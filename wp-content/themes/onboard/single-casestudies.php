<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include 'string_manipulation/StringManipulation.php';
include 'templates/BottomRedLine.php';

get_header();

// string manipulator
$stringManipulator = new StringManipulation();

$postContent = $post->post_content;

$postEssence = get_post_meta($post->ID, 'essence', true);
$postQuote = get_post_meta($post->ID, 'testimonial', true);
$postQuoteAuthor = get_post_meta($post->ID, 'testimonial_author', true);

$image = $stringManipulator->stringExtract($postContent, '{image}', '{/image}');
$imageURL = $stringManipulator->stringExtract($image, 'src="', '"');

?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">case-studies</div>



<!-- top banner of the page -->
<div id="single-cs-banner">
    <div id="single-cs-banner-leftdiv">
        <div id="single-cs-banner-triangle"></div>
        <a id="single-cs-banner-triangletext"  href="case-studies">
            <p>CASE STUDY</p>
            <p>&#8592;</p>
        </a>
    </div>
    <div id="single-cs-banner-right">
        <img src="<?php bloginfo('template_url'); ?>/images/topMenu/logo.png">
        <p id="single-cs-banner-title"><?php echo $post->post_title; ?></p>
    </div>
    <div id="single-cs-essence">
        <div id="single-cs-essence-text"><?php echo $postEssence; ?></div>
        <div id="single-cs-essence-line"></div>
    </div>
</div>

<!-- container of the main holder -->
<div id="single-cs-main">

    <!-- left column with main content -->
    <div id="single-cs-main-left">
        <i class="demo-icon icon-quote-left-alt" id="single-cs-main-left-quote-symbol">&#xe802;</i>
        <p id="single-cs-main-left-quote"><?php echo $postQuote; ?></p>
        <p id="single-cs-main-left-quoteauthor"><?php echo $postQuoteAuthor; ?></p>
<?php

    // print all sub content elements
    $numberOfSubTitles = preg_match_all('/\bsub_title\b/', $post->post_content);

    for($i = 0; $i < $numberOfSubTitles/2; $i++){
        $stringManipulator->stringExtractAndDelete($postContent, '{sub_title}', '{/sub_title}');
        $tempSubTitle = $stringManipulator->neededSubString;
        $postContent = $stringManipulator->reducedString;

        $stringManipulator->stringExtractAndDelete($postContent, '{content}', '{/content}');
        $tempContent = $stringManipulator->neededSubString;
        $postContent = $stringManipulator->reducedString;

        echo "<p class='single-cs-leftTitle'>".$tempSubTitle."</p>";
        echo "<p class='single-cs-leftContent'>".$tempContent."</p>";
    }

    // get pdf file path
    $pdf_file = $stringManipulator->stringExtract($postContent, '{pdf_file}', '{/pdf_file}');
    $pdf_url = $stringManipulator->stringExtract($postContent, 'href="', '"');
?>
        <div id="single-cs-left-pdf">
            <img src="<?php bloginfo('template_url'); ?>/images/icons/download_pdf.png" >
            <div data-link="<?php echo $pdf_url; ?>" id="download-pdf-button">DOWNLOAD FULL TEXT</div>
        </div>
    </div>

<!-- right column with main content -->
    <div id="single-cs-main-right">
        <img id="single-cs-main-right-img" src="<?php if($imageURL!=''){ echo $imageURL; }else{ echo bloginfo('template_url').'/images/home/partner.png'; } ?>" width="400" >
        <!-- solutions -->
        <p class='single-cs-leftTitle'>Solutions</p>
        <ul class="single-cs-main-right-ul">
<?php

    // print all solutions
    $allSolutions = get_post_meta($post->ID, 'solutions', true);
    $numberOfSolutions = preg_match_all('/\bsolution\b/', $allSolutions);

    for($i = 0; $i < $numberOfSolutions/2; $i++){
        $stringManipulator->stringExtractAndDelete($allSolutions, '{solution}', '{/solution}');
        $tempSolution = $stringManipulator->neededSubString;
        $allSolutions = $stringManipulator->reducedString;
?>
            <li>
                <img src="<?php echo bloginfo('template_url').'/images/icons/triangle_li.png'; ?>" >
                <p><?php echo $tempSolution; ?></p>
            </li>
<?php } ?>
        </ul>

        <!-- results -->
        <p class='single-cs-leftTitle'>Results</p>
        <ul class="single-cs-main-right-ul">
<?php

            // print all results
            $allResults = get_post_meta($post->ID, 'results', true);
            $numberOfResults = preg_match_all('/\bresult\b/', $allResults);

            for($i = 0; $i < $numberOfResults/2; $i++){
                $stringManipulator->stringExtractAndDelete($allResults, '{result}', '{/result}');
                $tempResult = $stringManipulator->neededSubString;
                $allResults = $stringManipulator->reducedString;
?>
                <li>
                    <img src="<?php echo bloginfo('template_url').'/images/icons/triangle_li.png'; ?>" >
                    <p><?php echo $tempResult; ?></p>
                </li>
<?php } ?>
        </ul>
    </div>
</div>


<?php

$subFooter = new BottomRedLine("Find out how we can help your business grow");
$subFooter->printHTML();

get_footer();