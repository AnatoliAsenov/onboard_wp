<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include 'string_manipulation/StringManipulation.php';
include 'templates/MiddlePageBoxes.php';
include 'templates/BottomRedLine.php';

get_header();

?>
    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>


    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/banners/whatwedo.jpg" id="topBanner">
<?php

// string manipulator
$stringManipulator = new StringManipulation();

$slug = basename(get_permalink());
$siteURL = get_site_url();
?>

<h1 id="whatWeDo_title">DATABASE SERVICES</h1>

<ul id="whatWeDo_textHolder">
    <li id="whatWeDo_textHolder_left">
<?php
// print all paragraphs
$postContent = $post->post_content;
$numberOfSubTitles = preg_match_all('/\bparagraph-delimiter\b/', $postContent);

for($i = 0; $i < $numberOfSubTitles/2; $i++) {
    $stringManipulator->stringExtractAndDelete($postContent, '{paragraph-delimiter}', '{/paragraph-delimiter}');
    $tempContent = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
?>
    <p><?php echo $tempContent; ?></p>
<?php } ?>

    </li>
    <li id="whatWeDo_textHolder_right">
        <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/data_services.png" >
    </li>
</ul>


    <!-- middle page boxes begin -->
<?php
    $stringManipulator->stringExtractAndDelete($postContent, '{left-slogan}', '{/left-slogan}');
    $leftSlogan = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{right-slogan}', '{/right-slogan}');
    $rightSlogan = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $middleBoxes = new MiddlePageBoxes();
    $middleBoxes->setLeftText($leftSlogan);
    $middleBoxes->setRightText($rightSlogan);
    $middleBoxes->printHTML();

// bottom red line content
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>
    <!-- middle page boxes end -->


<!-- case studies begin -->
    <div id="cases">
    <img src="<?php bloginfo('template_url'); ?>/images/icons/cases.png" id="casesTopIcon">
    <p id="casesTit">CASES</p>


<?php

// associated case studies
$args = array('post_type' => 'casestudies', 'post_per_page' => 3);
$loop = new WP_Query($args);

// do if there are posts
if($loop->have_posts()) {

    echo "<ul id='caseStudyUl'>";

    foreach($loop->posts as $caseStudy){
        $caseStudyType = wp_get_post_terms($caseStudy->ID, 'casestudytype');

        // get custom fields data for the current post
        $customFields_thumbnail =  get_post_meta($caseStudy->ID, 'thumbnail', true);

        $postContent = $caseStudy->post_content;
        $image = $stringManipulator->stringExtract($postContent, '{image}', '{/image}');
        $imageURL = $stringManipulator->stringExtract($image, 'src="', '"');

        // check for visibility
        $caseStudyVisibility =  get_post_meta($caseStudy->ID, 'database_services_category', true);
        if($caseStudyVisibility) {
            ?>
            <li data-type="<?php echo $caseStudyType[0]->name; ?>" class="mix <?php echo $caseStudyType[0]->name; ?>" >
                <a href="<?php echo $caseStudy->guid; ?>" class="aOfCaseStudy">
                    <div class="cs-content-item" <?php if($imageURL!=""){?> style="background:url('<?php echo $imageURL; ?>');background-repeat:no-repeat;"<?php } ?> >
                        <div class="cs-overlay"></div>
                        <div class="cs-corner-overlay-content">
                            <p class="cs_permanent_text">CASE STUDY</p>
                            <p><?php echo $customFields_thumbnail; ?></p>
                        </div>
                        <div class="cs-overlay-content">
                            <h2><?php echo $customFields_thumbnail; ?></h2>
                            <p><?php echo $caseStudy->post_title; ?></p>
                        </div>
                    </div>
                </a>
            </li>
            <?php
        }
    }

    echo "</ul>";
}
?>
        <a href="<?php echo $siteURL."/case-studies"; ?>">
            <img src="<?php bloginfo('template_url'); ?>/images/icons/plus2.png" id="casesBottIcon">
        </a>
    </div>
<!-- case studies end -->

<?php

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();