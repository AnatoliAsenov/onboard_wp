<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include "templates/BottomRedLine.php";

get_header();

set_post_thumbnail_size( 630, 350, true );
?>
<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">blog</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/blog/blog.jpg" id="topBanner">

<!-- main content of the blog -->
<div id="blog-mainContainer">

<!-- main content left container -->
    <div id="blog-mainContainer-left">
        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <?php //the_title(); ?>
        <h1 id="single-post-title"><?php the_title(); ?></h1>
        <span>
            <span class="blog-big_article_category"><?php echo get_the_category($post->ID)[0]->name; ?></span>
            <i class="demo-icon icon-calendar">&#xe803;</i>
            <span class="blog-big_article_date"><?php the_time('F j, Y'); ?></span>
        </span>
        <?php the_post_thumbnail(); ?>
        <?php the_content(); ?>
        <!--div id="single-post-content">
            <?php //echo $post->post_content; ?>
        </div -->
 
<!-- sharing buttons -->
        <ul id="sharing-buttons">
            <li>
                <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-facebook-square" aria-hidden="true" style="color: #3A5A97;"></i>
                </a>
            </li>
            <li>
                <a class="twitter" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-twitter-square" id="twitter" style="color: #1DA1F3;"></i>
                </a>
            </li>
            <li>
                <a class="google-plus" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank">
                    <i class="fa fa-google-plus-square" id="google-plus" style="color: #DC5043;"></i>
                </a>
            </li>
            <li>
                <a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&source=http://www.onboardcrm.com" target="_blank">
                    <i class="fa fa-linkedin-square" aria-hidden="true" style="color: #0077B4;"></i>
                </a>
            </li>
        </ul>

        <hr class="blog_hr">
<!-- tags -->
        <div id="tagsList">
            <i class="fa fa-bookmark" aria-hidden="true"></i>
            <p>TAGS</p>
            <ul>
    <?php
        $allTags = get_the_tags($post->ID);
        for($i = 0; count($allTags) > $i; $i++){
            if($allTags[$i]->name != "" && $allTags[$i]->name != " "){
                $blogURL = get_home_url()."/blog";
    ?>
                <li class="tagsList-li">
                    <a href="<?php echo add_query_arg( 'tag', $allTags[$i]->term_id, $blogURL ); ?>" class="tagsList-a">
                        <h3 class="tagsList-h3"><?php echo $allTags[$i]->name; ?></h3>
                    </a>
                </li>
    <?php
        }   }
    ?>
            </ul>
        </div>
        
        
            <?php endwhile; ?>
        <?php endif; ?>
    </div>


<!-- main content right container -->
    <div id="blog-mainContainer-right">
        <!-- top buttons -->
        <ul id="blog-mainContainer-right-buttons">
            <li id="blog-right-button-resent" class="blog-right-button-active">RESENT</li>
            <li id="blog-right-button-popular">POPULAR</li>
        </ul>


        <!-- list of RESENT posts (4th newest posts) -->
        <div id="blog-resent-publications">
            <?php
            if($cleanCategoryName == "all" || $cleanCategoryName == "ALL"){
                // get all posts
                $args = array(
                    'posts_per_page' => 4,
                    'paged' => $paged
                );
            }else{
                // get just posts from the category
                $args = array(
                    'posts_per_page' => 4,
                    'paged' => $paged,
                    'cat' => $categoryOfPage
                );
            }
            $custom_query = new WP_Query( $args );
            //set_post_thumbnail_size( 120, 120, true );

            while($custom_query->have_posts()) {
                $custom_query->the_post();
                $thumbnailDescription = get_post_meta($post->ID, 'thumbnail_description', true);
                $firstCharsFromDesc = substr($thumbnailDescription, 0, 80);
                ?>
                <div class="blog-right-postlist-element">
                    <a href="<?php the_permalink(); ?>" class="blog-right-postlist-thumbnail"  style="background-image: url('<?php bloginfo('template_url'); ?>/images/blog/not.png')">
                        <?php the_post_thumbnail( 'thumbnail' ); ?>
                    </a>
                    <div class="blog-right-postlist-allTexts">
                        <a href="<?php the_permalink(); ?>" class="blog-right-postlist-title"><?php the_title(); ?></a>
                        <p class="blog-right-postlist-text"><?php echo $firstCharsFromDesc; ?></p>
                    <span>
                        <span class="blog-big_article_category"><?php echo get_the_category($post->ID)[0]->name; ?></span>
                        <i class="demo-icon icon-calendar">&#xe803;</i>
                        <span class="blog-big_article_date"><?php the_time('F j, Y'); ?></span>
                    </span>
                    </div>
                </div>
            <?php } ?>
        </div>



        <!-- list of POPULAR posts -->
        <div id="blog-popular-publications">

            <?php while($custom_query->have_posts()) {
                $custom_query->the_post();
                $showInPopular = get_post_meta($post->ID, 'popular', true);
                if($showInPopular == "true"){
                    $thumbnailDescription = get_post_meta($post->ID, 'thumbnail_description', true);
                    $firstCharsFromDesc = substr($thumbnailDescription, 0, 80);
                    ?>
                    <div class="blog-right-postlist-element">
                        <a href="<?php the_permalink(); ?>" class="blog-right-postlist-thumbnail"  style="background-image: url('<?php bloginfo('template_url'); ?>/images/blog/not.png')">
                            <?php the_post_thumbnail( 'thumbnail' ); ?>
                        </a>
                        <div class="blog-right-postlist-allTexts">
                            <a href="<?php the_permalink(); ?>" class="blog-right-postlist-title"><?php the_title(); ?></a>
                            <p class="blog-right-postlist-text"><?php echo $firstCharsFromDesc; ?></p>
                        <span>
                            <span class="blog-big_article_category"><?php echo get_the_category($post->ID)[0]->name; ?></span>
                            <i class="demo-icon icon-calendar">&#xe803;</i>
                            <span class="blog-big_article_date"><?php the_time('F j, Y'); ?></span>
                        </span>
                        </div>
                    </div>
                <?php }   }?>
        </div>




        <!-- subscribe field -->
        <p id="blog-mainContainer-right-subscribe">SUBSCRIBE</p>
        <div>
            <input type="text" id="blog-mainContainer-right-subscribe-input" placeholder="Your Email Address"/>
            <div data-icon="n" class="icon" id="blog-subscribe-paperplane"></div>
        </div>
    </div>
    <!-- main container right end -->
</div>


<!-- footer -->
<?php

$subFooter = new BottomRedLine("We are the ultimate choice for your job");
$subFooter->printHTML();

get_footer();

