<?php

include "templates/PopUps.php";

$siteURL = get_site_url();

?>

	<!-- sub footer -->
	<div id="subFooter_container">
		<table id="subFooter">
			<tr class="subFooter_horizontalBuffer" colspan="5"></tr>
			<tr>
				<th><h2>WHAT WE DO</h2></th>
				<th><h2>INSIGHTS</h2></th>
				<th><h2>ABOUT</h2></th>
				<th><h2>FACTS</h2></th>
				<th><h4>Copyright &copy; <?php echo date("Y"); ?> Onboardcrm. All Rights reserved.</h4></th>
			</tr>
			<tr class="subFooter_horizontalBuffer2" colspan="5"></tr>
			<tr>
				<th>
					<ul>
						<li><a href="<?php echo $siteURL.'/what-we-do/worldwide-market-entry'; ?>">World Market Entry</a></li>
						<li><a href="<?php echo $siteURL.'/what-we-do/digital-marketing-services'; ?>">Digital Marketing Services</a></li>
						<li><a href="<?php echo $siteURL.'/what-we-do/database-services'; ?>">Database Services</a></li>
						<li><a href="<?php echo $siteURL.'/what-we-do/b2b-marketing-and-sales'; ?>">B2B Marketing & Sales</a></li>
						<li><a href="<?php echo $siteURL.'/what-we-do/business-process-enablement'; ?>">Business Process Enablement</a></li>
					</ul>
				</th>
				<th>
					<ul>
						<li><a href="<?php echo $siteURL.'/technology'; ?>">Technology</a></li>
						<li><a href="<?php echo $siteURL.'/case-studies'; ?>">Cases</a></li>
						<li><a href="<?php echo $siteURL.'/blog'; ?>">Blog</a></li>
					</ul>
				</th>
				<th>
					<ul>
						<li><a href="<?php echo $siteURL.'/company'; ?>">Company</a></li>
						<li><a href="<?php echo $siteURL.'/careers'; ?>">Careers</a></li>
					</ul>
				</th>
				<th>
					<ul>
						<li><a href="<?php echo $siteURL.'/facts-and-figures/why-onboard'; ?>">Why Onboard</a></li>
						<li><a href="<?php echo $siteURL.'/facts-and-figures/partnership-advantages'; ?>">Partnership Advantages</a></li>
						<li><a href="<?php echo $siteURL.'/facts-and-figures/community'; ?>">Community</a></li>
						<li><a href="<?php echo $siteURL.'/facts-and-figures/data-privacy'; ?>">Data Privacy</a></li>
						<li><a href="http://ec.europa.eu/ipg/basics/legal/cookies/index_en.htm#section_2" target="_blank">Cookies</a></li>
					</ul>
				</th>
				<th>
					<div class="footer_social_icons" id="footer-first-soc-icons">
						<a class="bottomLineLinks" target="_blank" href="https://www.facebook.com/OnboardCRM">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
						<a class="bottomLineLinks" target="_blank" href="https://www.linkedin.com/company/onboardcrm?trk=company_logo">
							<i class="fa fa-linkedin-square" aria-hidden="true"></i>
						</a>
						<a class="bottomLineLinks" target="_blank" href="https://twitter.com/Onboardcrm">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
						<a class="bottomLineLinks" target="_blank" href="https://www.instagram.com/onboardcrm/" style="margin-right:0;">
							<i class="fa fa-instagram" aria-hidden="true"></i>
						</a>
					</div>
				</th>
			</tr>
			<tr class="subFooter_horizontalBuffer3" colspan="5"></tr>
		</table>

		<div class="footer_social_icons" id="footer-second-soc-icons">
			<a class="bottomLineLinks" target="_blank" href="https://www.facebook.com/OnboardCRM">
				<i class="fa fa-facebook" aria-hidden="true"></i>
			</a>
			<a class="bottomLineLinks" target="_blank" href="https://www.linkedin.com/company/onboardcrm?trk=company_logo">
				<i class="fa fa-linkedin-square" aria-hidden="true"></i>
			</a>
			<a class="bottomLineLinks" target="_blank" href="https://twitter.com/Onboardcrm">
				<i class="fa fa-twitter" aria-hidden="true"></i>
			</a>
			<a class="bottomLineLinks" target="_blank" href="https://www.instagram.com/onboardcrm/" style="margin-right:0;">
				<i class="fa fa-instagram" aria-hidden="true"></i>
			</a>
		</div>

		<div id="bottom_angle"></div>
	</div>

<?php

$popUp = new PopUps();
$popUp->printHTML();

?>
	<!-- scripts -->

		<script src="<?php bloginfo('template_url'); ?>/js/lib/jquery-3.1.1.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/_getScreenDimensions.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/lib/jquery.placeholder.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/lib/jquery-color.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/lib/slick.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/topMenu/_topMenu.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/_bottomRedLine.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/_arrowUP.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/_middlePageBoxes.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/about/about-company.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/blog/_mainPage.js"></script>
	    <script src="<?php bloginfo('template_url'); ?>/js/careers/_careers.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/careers/_applyForJob.js"></script>
	    <script src="<?php bloginfo('template_url'); ?>/js/technology/_middleLineAnimation.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/_thePopUp.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/_social-listening.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/facts/_facts.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/home/_contactForm.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/contacts/_contacts.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/whatwedo/_web-development.js"></script>
<!-- script for "HOME" page -->
		<script src="<?php bloginfo('template_url'); ?>/js/home/_sliders.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/home/_circle.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/home/_activatingAnimations.js"></script>

<?php

	if(is_page('about')){ ?>
		<script src="<?php bloginfo('template_url'); ?>/js/lib/countUp.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/about/about.js"></script>
<?php
	}

	if(is_page('case-studies')){ ?>
		<script src="<?php bloginfo('template_url'); ?>/js/lib/mixitup.min.js"></script>
		<script src="<?php bloginfo('template_url'); ?>/js/casestudies/casestudies.js"></script>
<?php
	}
?>



<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1066581569;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
	<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1066581569/?guid=ON&amp;script=0"/>
	</div>
</noscript>





<!-- sharp spring tracking code -->
<!--<script type="text/javascript">-->
<!--	var _ss = _ss || [];-->
<!--	_ss.push(['_setDomain', 'https://koi-3QNB9GWLJ6.marketingautomation.services/net']);-->
<!--	_ss.push(['_setAccount', 'KOI-3UOYS7U91K']);-->
<!--	_ss.push(['_trackPageView']);-->
<!--	(function() {-->
<!--	    var ss = document.createElement('script');-->
<!--	    ss.type = 'text/javascript'; ss.async = true;-->
<!--	-->
<!--	    ss.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'koi-3QNB9GWLJ6.marketingautomation.services/client/ss.js?ver=1.1.1';-->
<!--	    var scr = document.getElementsByTagName('script')[0];-->
<!--	    scr.parentNode.insertBefore(ss, scr);-->
<!--	})();-->
<!--</script>-->
<!--<script type="text/javascript">-->
<!--	var __ss_noform = [];-->
<!--	__ss_noform.push(['baseURI', 'https://app-3QNB9GWLJ6.marketingautomation.services/webforms/receivePostback/MzawMDEzMjAzBwA/']);-->
<!--	__ss_noform.push(['submitType', 'manual']);-->
<!--	__ss_noform.push(['form', 'onboardForm', '01229ed3-1b4d-4bd1-9b47-f0993a4103f0']);-->
<!--</script>-->
<!--<script type="text/javascript" src="https://koi-3QNB9GWLJ6.marketingautomation.services/client/noform.js?ver=1.24" ></script>-->



<!-- albacross tracking code -->
<script type="text/javascript">
	(function(a,l,b,c,r,s){_nQc=c,r=a.createElement(l),s=a.getElementsByTagName(l)[0];r.async=1;
		r.src=l.src=("https:"==a.location.protocol?"https://":"http://")+b;s.parentNode.insertBefore(r,s);
	})(document,"script","serve.albacross.com/track.js","89960742");
</script>


<!-- Google Structured Data -->
<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Organization",
  "name": "Onboard",
  "url": "http://onboardcrm.com",
  "sameAs": [
    "http://www.facebook.com/profile.php?id=100004612067691",
    "http://www.linkedin.com/company-beta/391328/",
    "https://twitter.com/Onboardcrm"
  ]
}
</script>

	</body>
</html>
