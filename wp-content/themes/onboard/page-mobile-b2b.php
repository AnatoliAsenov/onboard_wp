<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(117);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(117);
$header->printHTML();

$siteURL = get_site_url();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/wwdo_768.jpg" id="topBanner">

    <div id="wwd-container">
        <div id="wwd_title">B2B MARKETING & SALES</div>

        <ul id="wwd_textHolder">
            <li id="wwd_textHolder_left">
                <?php
                // print all paragraphs
                $numberOfSubTitles = preg_match_all('/\bparagraph-delimiter\b/', $postContent);

                for($i = 0; $i < $numberOfSubTitles/2; $i++) {
                    $stringManipulator->stringExtractAndDelete($postContent, '{paragraph-delimiter}', '{/paragraph-delimiter}');
                    $tempContent = $stringManipulator->neededSubString;
                    $postContent = $stringManipulator->reducedString;
                    ?>
                    <p><?php echo $tempContent; ?></p>
                <?php } ?>

            </li>
            <li id="wwd_textHolder_right">
                <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/b2b_marketing.png" >
            </li>
        </ul>
    </div>


<?php
$stringManipulator->stringExtractAndDelete($postContent, '{left-slogan}', '{/left-slogan}');
$leftSlogan = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
echo "<div id='wwd-firstSlogan'>".$leftSlogan."</div>";

$stringManipulator->stringExtractAndDelete($postContent, '{right-slogan}', '{/right-slogan}');
$rightSlogan = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
echo "<div id='wwd-secondSlogan'>".$rightSlogan."</div>";
?>

    <div class="careers_bigicon_container">
        <div id="block2-quote">
            <i class="demo-icon icon-quote-right-alt">&#xe801;</i>
        </div>
    </div>
    <hr class="careers_fullHR">
    <div id="careers_onboard_container">
        <div id="block2_onboard_Text" class="wwd-mid-texts"></div>
    </div>

    <hr class="careers_partHR" id="first_partHR">

    <hr class="careers_fullHR" id="carees_second_fullHR">
    <div class="careers_bigicon_container">
        <div id="block2_gender" class="icon" data-icon="m"></div>
    </div>

    <div id="careers_employee_container">
        <div id="block2_employee_Text" class="wwd-mid-texts"></div>
        <hr class="careers_partHR" id="second_partHR">
    </div>



    <!-- case studies -->
    <img src="<?php bloginfo('template_url'); ?>/images/icons/cases.png" id="home-cs-book" />

    <div id="home-cs-tit">CASES</div>

<?php

// associated case studies
$args = array('post_type' => 'casestudies', 'post_per_page' => 3);
$loop = new WP_Query($args);

// do if there are posts
if($loop->have_posts()) {

    echo "<ul id='caseStudyUl'>";

    foreach($loop->posts as $caseStudy){
        $caseStudyType = wp_get_post_terms($caseStudy->ID, 'casestudytype');

        // get custom fields data for the current post
        $customFields_thumbnail =  get_post_meta($caseStudy->ID, 'thumbnail', true);

        $postContent = $caseStudy->post_content;
        $image = $stringManipulator->stringExtract($postContent, '{image}', '{/image}');
        $imageURL = $stringManipulator->stringExtract($image, 'src="', '"');

        // check for visibility
        $caseStudyVisibility =  get_post_meta($caseStudy->ID, 'database_services_category', true);
        if($caseStudyVisibility) {
            ?>
            <li data-type="<?php echo $caseStudyType[0]->name; ?>" class="mix <?php echo $caseStudyType[0]->name; ?>" >
                <a href="<?php echo $caseStudy->guid; ?>" class="aOfCaseStudy">
                    <div class="cs-content-item" <?php if($imageURL!=""){?> style="background:url('<?php echo $imageURL; ?>');background-repeat:no-repeat;"<?php } ?> >
                        <div class="cs-overlay"></div>
                        <div class="cs-corner-overlay-content">
                            <p class="cs_permanent_text">CASE STUDY</p>
                            <p><?php echo $customFields_thumbnail; ?></p>
                        </div>
                        <div class="cs-overlay-content">
                            <h2><?php echo $customFields_thumbnail; ?></h2>
                            <p><?php echo $caseStudy->post_title; ?></p>
                        </div>
                    </div>
                </a>
            </li>
            <?php
        }
    }

    echo "</ul>";
}
?>
    <a href="<?php echo $siteURL."/case-studies"; ?>" id="casesButt" >
        <img src="<?php bloginfo('template_url'); ?>/images/icons/plus2.png" id="casesButtIcon" />
    </a>
    <!-- case studies end -->







    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;


$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();