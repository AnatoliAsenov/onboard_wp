$(document).ready(function() {

    if ($("#wd_topButton").length > 0) {
        var bigContainer = $("#wd_banner-main");

        var topContactsBtn = $("#wd_topButton");
        var topContactsBtnElements = $(".wd_buttonElements");
        var closeBtn = $("#wd_topButton-close");

        var sideVerticalWords = $(".wd_bannerVerticalTexts");
        var contactsIsOpen = false;
        var contactsContainer = $("#contactsContainer");


        // set contacts container width
        contactsContainer.width( bigContainer.width() );

        // top button click event
        topContactsBtn.click(function(){
            if(contactsIsOpen){
                topContactsBtn.addClass('');
                sideVerticalWords.animate({paddingTop: '190px'}, 900);
                contactsContainer.animate({
                    height: 0
                }, 900, function(){
                    contactsContainer.css('display', 'none');
                    closeBtn.css('display', 'none');
                    topContactsBtnElements.css('display', 'block');
                });

                contactsIsOpen = false;
            }else {
                topContactsBtn.addClass('');
                sideVerticalWords.animate({paddingTop: 0}, 900);
                contactsContainer.css('display', 'block').animate({
                    height: (bigContainer.height() - 17) + 'px'
                }, 900, function(){
                    topContactsBtnElements.css('display', 'none');
                    closeBtn.css('display', 'block');
                });

                contactsIsOpen = true;
            }
        });
    }

});
