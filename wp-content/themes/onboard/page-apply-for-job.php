<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

get_header();

$postLocation = get_post_meta($post->ID, 'location', true);
$postLanguages = get_post_meta($post->ID, 'languages', true);
$postID = get_query_var("id");
$postLocation = get_post_meta($postID, 'location', true);
$postLanguages = get_post_meta($postID, 'languages', true);
$post = get_post( $postID );
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">careers</div>


<!-- single job container -->
<div id="single-job-container">
    <div id="single-job-container1">
        <h1 id="single-job-diagonal-tit">CAREERS</h1>
        <div id="single-job-diagonal"></div>
    </div>
    <div id="single-job-container2">
        <p><?php echo $post->post_title; ?></p>
        <ul>
            <li>
                <i class="fa fa-language" aria-hidden="true"></i>
                <p><?php echo $postLanguages; ?></p>
            </li>
            <li>
                <i class="fa fa-globe" aria-hidden="true"></i>
                <p><?php echo $postLocation; ?></p>
            </li>
        </ul>
    </div>
</div>


<!-- contact form -->
<form id="job-form" action='https://crm.zoho.com/crm/WebToLeadForm' name=WebToLeads1239484000010599025 method="post" enctype="multipart/form-data" onSubmit='javascript:document.charset="UTF-8"; return checkMandatory()' accept-charset='UTF-8'>

<!-- system hidden fields -->
    <input type='text' style='display:none;' name='xnQsjsdp' value='4d75f250d006eeddcf8d7602c119c5c612b473a9775fc1518fc3092147ffa642'/>
    <input type='hidden' name='zc_gad' id='zc_gad' value=''/>
    <input type='text' style='display:none;' name='xmIwtLD' value='e3945a2a9ad283d7b9d1a3c717b39e4558c166983dc00224ed335370d5727d18'/>
    <input type='text' style='display:none;' name='actionType' value='TGVhZHM='/>
    <input type='text' style='display:none;' name='returnURL' value='http&#x3a;&#x2f;&#x2f;onboardcrm.com&#x2f;careers' />
<!-- job title -->
    <input type='text' style='display:none;' name='LEADCF8' value='<?php echo $post->post_title; ?>'>
    <input type="text" style="display:none;" value='Job&#x20;Application' name='LEADCF4'/>

    <table id="job-form-table">
        <tr><th colspan="2" class="job-form-table-tit">SUBMIT YOUR APPLICATION</th></tr>
        <tr>
            <th class="th-has-padding-20-0"><p class="job-form-table-row-tit">Resume/CV</p></th>
            <th class="th-has-padding-20-25">
                <div id="job-form-table-attach-btn">
                    <i class="fa fa-paperclip" aria-hidden="true"></i>
                    <p>ATTACH RESUME/CV</p>
                </div>
                <input type='file' name='theFile' multiple class="job-form-table-row-input" id="job-form-table-attach-input" />
            </th>
        </tr>
        <tr>
            <th class="th-has-padding-20-0"><p class="job-form-table-row-tit">First name:<span>&#42;</span></p></th>
            <th class="th-has-padding-20-25"><input type="text" class="job-form-table-row-input" name='First Name' /></th>
        </tr>
        <tr>
            <th class="th-has-padding-20-0"><p class="job-form-table-row-tit">Last name:<span>&#42;</span></p></th>
            <th class="th-has-padding-20-25"><input type="text" class="job-form-table-row-input" name='Last Name' /></th>
        </tr>
        <tr>
            <th class="th-has-padding-20-0"><p class="job-form-table-row-tit">Email:<span>&#42;</span></p></th>
            <th class="th-has-padding-20-25"><input type="text" class="job-form-table-row-input" name='Email' /></th>
        </tr>
        <tr>
            <th class="th-has-padding-20-0"><p class="job-form-table-row-tit">Phone:<span>&#42;</span></p></th>
            <th class="th-has-padding-20-25"><input type="text" class="job-form-table-row-input" name='Phone' /></th>
        </tr>
        <tr>
            <th class="th-has-padding-20-0"><p class="job-form-table-row-tit">Current company:</p></th>
            <th class="th-has-padding-20-25"><input type="text" class="job-form-table-row-input" name='Company' /></th>
        </tr>
        <tr><th colspan="2" class="job-form-table-tit">LINKS</th></tr>
        <tr>
            <th class="th-has-padding-20-0"><p class="job-form-table-row-tit">LinkedIn URL:</p></th>
            <th class="th-has-padding-20-25"><input type="text" class="job-form-table-row-input" name='LEADCF5' /></th>
        </tr>
        <tr>
            <th class="th-has-padding-20-0"><p class="job-form-table-row-tit">Other Profile URL:</p></th>
            <th class="th-has-padding-20-25"><input type="text" class="job-form-table-row-input" name='LEADCF7' /></th>
        </tr>
        <tr><th colspan="2" class="job-form-table-tit">ADDITIONAL INFORMATION</th></tr>
        <tr>
            <th colspan="2" style="text-align: left;">
                <textarea id="job-form-table-textarea" name='Description' placeholder="Add a cover letter or anything else you want to share"></textarea>
            </th>
        </tr>
    </table>
    <div id="job-form-bottom-white-line">
        <div id="job-form-bottom-white-line-btn">SUBMIT YOUR APPLICATION</div>
        <input style='display:none;' type='submit' value='Submit' id="job-form-hidden-submit-button"/>
    </div>
</form>
    <script>
        var mndFileds=new Array('Company','Last Name');
        var fldLangVal=new Array('Company','Last Name');
        var name='';
        var email='';

        function checkMandatory() {
            for(i=0;i<mndFileds.length;i++) {
                var fieldObj=document.forms['WebToLeads1239484000010599025'][mndFileds[i]];
                if(fieldObj) {
                    if (((fieldObj.value).replace(/^\s+|\s+$/g, '')).length==0) {
                        if(fieldObj.type =='file')
                        {
                            alert('Please select a file to upload.');
                            fieldObj.focus();
                            return false;
                        }
                        alert(fldLangVal[i] +' cannot be empty.');
                        fieldObj.focus();
                        return false;
                    }  else if(fieldObj.nodeName=='SELECT') {
                        if(fieldObj.options[fieldObj.selectedIndex].value=='-None-') {
                            alert(fldLangVal[i] +' cannot be none.');
                            fieldObj.focus();
                            return false;
                        }
                    } else if(fieldObj.type =='checkbox'){
                        if(fieldObj.checked == false){
                            alert('Please accept  '+fldLangVal[i]);
                            fieldObj.focus();
                            return false;
                        }
                    }
                    try {
                        if(fieldObj.name == 'Last Name') {
                            name = fieldObj.value;
                        }
                    } catch (e) {}
                }
            }
            return validateFileUpload();

        }
        function validateFileUpload(){
            var uploadedFiles = document.getElementById('theFile');
            var totalFileSize =0;
            if(uploadedFiles.files.length >3){
                alert('You can upload a maximum of three files at a time.');
                return false;
            }
            if ('files' in uploadedFiles) {
                if (uploadedFiles.files.length != 0) {
                    for (var i = 0; i < uploadedFiles.files.length; i++) {
                        var file = uploadedFiles.files[i];
                        if ('size' in file) {
                            totalFileSize = totalFileSize + file.size;
                        }
                    }
                    if(totalFileSize > 20971520){
                        alert('Total file(s) size should not exceed 20MB.');
                        return false;
                    }
                }
            }
        }
    </script>

<?php

get_footer();