<?php

/**
 * Created by Onboard
 * User: Anatoli Asenov
 */
class MiddlePageBoxes
{
    private $leftText = "";
    private $rightText = "";

    public function __construct(){}

    public function setLeftText($text){
        $this->leftText = $text;
    }

    public function setRightText($text){
        $this->rightText = $text;
    }

    public function printHTML(){

        $allRightP = explode("*", $this->rightText);
        $allLeftP = explode("*", $this->leftText);
        $allSeparatedP = array();

        for($r = 0; $r < count($allRightP); $r++){
            $tempContent = "<p>".$allRightP[$r]."</p>";
            array_push($allSeparatedP, $tempContent);
        }
        $rightForPrint = implode(" ", $allSeparatedP);

        $allSeparatedP = [];
        for($r = 0; $r < count($allLeftP); $r++){
            $tempContent = "<p>".$allLeftP[$r]."</p>";
            array_push($allSeparatedP, $tempContent);
        }
        $leftForPrint = implode(" ", $allSeparatedP);

?>
    <div id="middleBoxes_bigContainer">
        <div id="middleBoxes_middleContainer">
            <div id="middleBoxes_middleContainer_quote">
                <i class="demo-icon icon-quote-right-alt">&#xe801;</i>
            </div>
            <div id="middleBoxes_middleContainer_box">
                <i class="demo-icon icon-dropbox">&#xe800;</i>
            </div>
        </div>
        <div id="middleBoxes">
            <div id="middleBoxes_leftContainer" class="middleBoxes_text_holders middleBoxes_containers">
                <div id="middleBoxes_leftTopBorder"></div>
                <div id="middleBoxes_leftLeftBorder"></div>
                <div id="middleBoxes_leftText">
                    <?php echo $leftForPrint; ?>
                </div>
                <div id="middleBoxes_leftRightBorder"></div>
                <div id="middleBoxes_leftBottomBorder"></div>
            </div>
            <div id="middleBoxes_rightContainer" class="middleBoxes_text_holders middleBoxes_containers">
                <div id="middleBoxes_rightTopBorder"></div>
                <div id="middleBoxes_rightLeftBorder"></div>
                <div id="middleBoxes_rightText">
                    <?php echo $rightForPrint; ?>
                </div>
                <div id="middleBoxes_rightRightBorder"></div>
                <div id="middleBoxes_rightBottomBorder"></div>
            </div>
        </div>
    </div>
<?php
    }
}