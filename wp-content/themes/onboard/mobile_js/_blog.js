$(document).ready(function() {
    //=============================================
    //====== set the current post category ========
    //=============================================
    if ($("#allPostCategories").length) {
        var actualCategory = $("#thePostCategory").text();
        if (actualCategory == "all") {
            $("#allPostCategories_allElement").addClass("categories-item-active");
        } else {
            $(".categories-item a").each(function () {
                var theElement = $(this);
                var actualElementId = theElement.data("id");
                if (actualElementId == actualCategory) {
                    theElement.addClass("categories-item-active");
                }
            });
        }
    }


    //=======================================
    //========= center pagination ===========
    //=======================================
    if($(".pagination").length){
        var mainDivWith = $(".pagination").width();
        var previousWidth = 0;
        if($(".prev").length){
            previousWidth = $(".prev").width();
        }
        var allA = $(".page-numbers");
        var allA_withoutPrevNext = [];
        allA.each(function(){
            if( $(this).hasClass("page-numbers") && !$(this).hasClass("next") && !$(this).hasClass("prev") ) {
                allA_withoutPrevNext.push($(this));
            }
        });
        var firstA_left = allA_withoutPrevNext[0].offset().left;
        var lastA_left = allA_withoutPrevNext[allA_withoutPrevNext.length - 1].offset().left;
        var lastA_width = allA_withoutPrevNext[allA_withoutPrevNext.length - 1].width();
        allA_withoutPrevNext[0].css("marginLeft", ((mainDivWith - ((lastA_left+lastA_width) - firstA_left))/2 - previousWidth) + "px");
    }

    //===========================================================
    //====== click functions of RESENT and POPULAR buttons ======
    //===========================================================
    if($("#blog-recent-popular-buttons").length){

        // click on RECENT button
        $("#blog-right-button-resent").click(function(){
            var thisBtn = $(this);
            if( !thisBtn.hasClass("blog-right-button-active") ){
                $("#blog-recent-popular-buttons li").removeClass("blog-right-button-active");
                thisBtn.addClass("blog-right-button-active");
                $("#blog-popular-publications").animate({
                    opacity: 0
                }, 400, function(){
                    $(this).css("display", "none");
                    $("#blog-resent-publications").css("display", "block").animate({
                        opacity: 1
                    }, 400);
                });
            }
        });

        // click on POPULAR button
        $("#blog-right-button-popular").click(function(){
            var thisBtn = $(this);
            if( !thisBtn.hasClass("blog-right-button-active") ){
                $("#blog-recent-popular-buttons li").removeClass("blog-right-button-active");
                thisBtn.addClass("blog-right-button-active");
                $("#blog-resent-publications").animate({
                    opacity: 0
                }, 400, function(){
                    $(this).css("display", "none");
                    $("#blog-popular-publications").css("display", "block").animate({
                        opacity: 1
                    }, 400);
                });
            }
        });

    }

});
