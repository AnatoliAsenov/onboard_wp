$(document).ready(function() {

    if($("#about_tab").length) {
        var once_count = false;
        $(window).scroll(function () {

            var middlePixelHeight = $("#trackingPixel_middle").offset().top;
            var innerTabOffset = $("#about_tab").offset().top;

            if (innerTabOffset <= middlePixelHeight && once_count == false) {
                var options = {
                    useEasing: true,
                    useGrouping: true,
                    separator: ',',
                    decimal: '.',
                    prefix: '',
                    suffix: ''
                };
                var demo = new CountUp("countries_num", 0, 50, 0, 3.5, options);
                demo.start();

                var demo1 = new CountUp("offices_num", 0, 44, 0, 3.5, options);
                demo1.start();

                var demo2 = new CountUp("continents_num", 0, 6, 0, 2, options);
                demo2.start();

                once_count = true;
            }

        });
    }

    if($("#about-company-main").length){

    }

});
