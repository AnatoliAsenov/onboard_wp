

$(document).ready(function() {
    // check if bottom red line exist
    // check for screen width
    if($("#bottomJobLine_txt").length && $(window).width() > 1200 ) {
        var areMarginsOfHolderRemoved = false;
        var marginLeftIsRemoved = false;
        var marginRightIsRemoved = false;
        var fontSizeCount = 1;
        while($("#bottomJobLine_txt").width() > 840){
            if(areMarginsOfHolderRemoved){
                // reduce font size
                $("#bottomJobLine_txt").css("fontSize", (28-fontSizeCount) + "px");
                fontSizeCount++;
            }else{
                // remove margins of text holder
                if(marginLeftIsRemoved){
                    if(marginRightIsRemoved == false){
                        // remove margin right
                        $("#bottomJobLine_btn").css("marginRight", "0");
                        marginRightIsRemoved = true;
                    }
                }{
                    // remove margin left
                    $("#bottomJobLine_txt").css("marginLeft", "0");
                    marginLeftIsRemoved = true;
                }
                if(marginLeftIsRemoved && marginRightIsRemoved){
                    // set areMarginsOfHolderRemoved to true and logic goes to next step
                    areMarginsOfHolderRemoved = true;
                }
            }
        }
    }


    // position the angled line in the footer
    if($("#bottom_angle").length){
        const theElement = $('#bottom_angle');
        const theTab = document.querySelector('#subFooter');
        theElement.css("marginLeft", theTab.offsetLeft + 685 + "px");
    }

});

// step by step
// 1. remove margin left of text
// 2. remove margin right of button
// 3. reduce font size of the text
