<?php
/**
 * Created by Onboard
 * User: AnatoliAsenov
 */
include 'SocialMetaboxes.php';


//  ================================================
//  custom meta boxes for "Post" (regular) post type
//  ================================================

add_filter( 'rwmb_meta_boxes', 'regular_post_meta_boxes' );
function regular_post_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Additional post options', 'textdomain' ),
        'post_types' => 'post',
        'fields'     => array(
            array(
                'id'   => 'thumbnail_description',
                'name' => __( 'Thumbnail description:', 'textdomain' ),
                'type' => 'textarea',
            ),
            array(
                'id'      => 'popular',
                'name'    => __( 'Is the post in popular list:', 'textdomain' ),
                'type'    => 'radio',
                'options' => array(
                    'true' => __( 'Yes', 'textdomain' ),
                    'false' => __( 'No', 'textdomain' ),
                ),
            ),
        ),
    );
    return $meta_boxes;
}

// Facebook and LinkedIn Google+ meta box
add_filter( 'rwmb_meta_boxes', 'facebookPostPostType_meta_boxes' );
function facebookPostPostType_meta_boxes( $meta_boxes ) {
    $socialMetaBoxes = new SocialMetaboxes();
    $meta_boxes[] = $socialMetaBoxes->fb_linkedin_googlePlus('post');
    return $meta_boxes;
}

// Twitter meta box
add_filter( 'rwmb_meta_boxes', 'twitterPostPostType_meta_boxes' );
function twitterPostPostType_meta_boxes( $meta_boxes )
{
    $socialMetaBoxes = new SocialMetaboxes();
    $meta_boxes[] = $socialMetaBoxes->twitter('post');
    return $meta_boxes;
}



//  ===========================================
//  custom meta boxes for CaseStudies post type
//  ===========================================
add_filter( 'rwmb_meta_boxes', 'case_study_meta_boxes' );
function case_study_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Additional Case Study Options', 'textdomain' ),
        'post_types' => 'casestudies',
        'fields'     => array(
            array(
                'id'   => 'thumbnail',
                'name' => __( 'Text visible in preview mode:', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'      => 'visibility',
                'name'    => __( 'Visibility:', 'textdomain' ),
                'type'    => 'radio',
                'options' => array(
                    'visible' => __( 'visible', 'textdomain' ),
                    'hidden' => __( 'hidden', 'textdomain' ),
                ),
            ),
            array(
                'id'   => 'essence',
                'name' => __( 'Essence:', 'textdomain' ),
                'type' => 'text',
            ),
            array(
                'id'   => 'testimonial',
                'name' => __( 'Testimonial:', 'textdomain' ),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'testimonial_author',
                'name' => __( 'Testimonial Author:', 'textdomain' ),
                'type' => 'textarea',
            ),
            array(
                'id'      => 'testimonial_visibility',
                'name'    => __( 'Testimonial visibility on landing page:', 'textdomain' ),
                'type'    => 'radio',
                'options' => array(
                    'visible' => __( 'visible', 'textdomain' ),
                    'hidden' => __( 'hidden', 'textdomain' ),
                ),
            ),
            array(
                'id'   => 'solutions',
                'name' => __( 'Solutions:', 'textdomain' ),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'results',
                'name' => __( 'Results:', 'textdomain' ),
                'type' => 'textarea',
            )

        ),
    );
    return $meta_boxes;
}

add_filter( 'rwmb_meta_boxes', 'case_study_second_meta_boxes' );
function case_study_second_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Associated with <span style="color:red;">WHAT WE DO</span> categories', 'textdomain' ),
        'post_types' => 'casestudies',
        'fields'     => array(
            array(
                'id'      => 'database_services_category',
                'name'    => __( 'Associated with <span style="color:red;">Database Services</span>:', 'textdomain' ),
                'type'    => 'checkbox'
            ),
            array(
                'id'      => 'worldwide_market_entry_category',
                'name'    => __( 'Associated with <span style="color:red;">Worldwide Market Entry</span>:', 'textdomain' ),
                'type'    => 'checkbox'
            ),
            array(
                'id'      => 'digital_marketing_services_category',
                'name'    => __( 'Associated with <span style="color:red;">Digital Marketing Services</span>:', 'textdomain' ),
                'type'    => 'checkbox'
            ),
            array(
                'id'      => 'b2b_marketing_and_sales_category',
                'name'    => __( 'Associated with <span style="color:red;">B2B Marketing & Sales</span>:', 'textdomain' ),
                'type'    => 'checkbox'
            ),
            array(
                'id'      => 'business_process_enablement_platform_category',
                'name'    => __( 'Associated with <span style="color:red;">Business Process Enablement Platform</span>:', 'textdomain' ),
                'type'    => 'checkbox'
            ),

        ),
    );
    return $meta_boxes;
}

// Facebook and LinkedIn Google+ meta box
add_filter( 'rwmb_meta_boxes', 'facebookCaseStudiesPostType_meta_boxes' );
function facebookCaseStudiesPostType_meta_boxes( $meta_boxes ) {
    $socialMetaBoxes = new SocialMetaboxes();
    $meta_boxes[] = $socialMetaBoxes->fb_linkedin_googlePlus('casestudies');
    return $meta_boxes;
}

// Twitter meta box
add_filter( 'rwmb_meta_boxes', 'twitterCaseStudiesPostType_meta_boxes' );
function twitterCaseStudiesPostType_meta_boxes( $meta_boxes )
{
    $socialMetaBoxes = new SocialMetaboxes();
    $meta_boxes[] = $socialMetaBoxes->twitter('casestudies');
    return $meta_boxes;
}


//  ========================================================
//  custom meta boxes for EmployeeQuote post type - disabled
//  ========================================================
//add_filter( 'rwmb_meta_boxes', 'employeeQuote_meta_boxes' );
function employeeQuote_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Additional Employee Quote Options', 'textdomain' ),
        'post_types' => 'employeequote',
        'fields'     => array(
            array(
                'id'      => 'employeeuote_gender',
                'name'    => __( 'Employee gender:', 'textdomain' ),
                'type'    => 'radio',
                'options' => array(
                    'male' => __( 'male', 'textdomain' ),
                    'female' => __( 'female', 'textdomain' ),
                ),
            ),
            array(
                'id'      => 'visibility',
                'name'    => __( 'Visibility:', 'textdomain' ),
                'type'    => 'radio',
                'options' => array(
                    'visible' => __( 'visible', 'textdomain' ),
                    'hidden' => __( 'hidden', 'textdomain' ),
                ),
            ),
        ),
    );
    return $meta_boxes;
}


//  =========================================
//  custom meta boxes for Jobs post type
//  =========================================
add_filter( 'rwmb_meta_boxes', 'jobs_meta_boxes' );
function jobs_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Additional Jobs Options', 'textdomain' ),
        'post_types' => 'jobs',
        'fields'     => array(
            array(
                'id'   => 'languages',
                'name' => __( 'Languages:', 'textdomain' ),
                'type' => 'textarea',
            ),
            array(
                'id'   => 'location',
                'name' => __( 'Location:', 'textdomain' ),
                'type' => 'textarea',
            ),
            array(
                'id'      => 'visibility',
                'name'    => __( 'Visibility:', 'textdomain' ),
                'type'    => 'radio',
                'options' => array(
                    'visible' => __( 'visible', 'textdomain' ),
                    'hidden' => __( 'hidden', 'textdomain' ),
                ),
            ),
        ),
    );
    return $meta_boxes;
}

// Facebook and LinkedIn Google+ meta box
add_filter( 'rwmb_meta_boxes', 'facebookJobsPostType_meta_boxes' );
function facebookJobsPostType_meta_boxes( $meta_boxes ) {
    $socialMetaBoxes = new SocialMetaboxes();
    $meta_boxes[] = $socialMetaBoxes->fb_linkedin_googlePlus("jobs");
    return $meta_boxes;
}

// Twitter meta box
add_filter( 'rwmb_meta_boxes', 'twitterJobsPostType_meta_boxes' );
function twitterJobsPostType_meta_boxes( $meta_boxes )
{
    $socialMetaBoxes = new SocialMetaboxes();
    $meta_boxes[] = $socialMetaBoxes->twitter("jobs");
    return $meta_boxes;
}


//  =============================================
//  custom meta boxes for "Page" post type
//  =============================================

// Facebook and LinkedIn Google+ meta box
add_filter( 'rwmb_meta_boxes', 'facebookPagePostType_meta_boxes' );
function facebookPagePostType_meta_boxes( $meta_boxes )
{
    $socialMetaBoxes = new SocialMetaboxes();
    $meta_boxes[] = $socialMetaBoxes->fb_linkedin_googlePlus("page");
    return $meta_boxes;
}

// Twitter meta box
add_filter( 'rwmb_meta_boxes', 'twitterPagePostType_meta_boxes' );
function twitterPagePostType_meta_boxes( $meta_boxes )
{
    $socialMetaBoxes = new SocialMetaboxes();
    $meta_boxes[] = $socialMetaBoxes->twitter("page");
    return $meta_boxes;
}

// popUp customization
add_filter( 'rwmb_meta_boxes', 'popup_meta_boxes' );
function popup_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Pop-Up settings for this page', 'textdomain' ),
        'post_types' => 'page',
        'fields'     => array(
            array(
                'id'      => 'visibility',
                'name'    => __( 'Visibility:', 'textdomain' ),
                'type'    => 'radio',
                'options' => array(
                    'hidden' => __( 'hidden', 'textdomain' ),
                    'visible' => __( 'visible', 'textdomain' ),
                ),
            ),
            array(
                'id'   => 'time_to_show',
                'name' => __( 'Show after (in milliseconds <span style="color: #ff0000;">1sec=1000mSec</span>):', 'textdomain' ),
                'type' => 'text',
            ),
        ),
    );
    return $meta_boxes;
}