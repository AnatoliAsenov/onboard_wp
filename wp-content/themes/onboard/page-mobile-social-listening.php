<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$siteURL = get_site_url();

$pageContent = get_post(737);
$postContent = $pageContent->post_content;

$header = new MobileHeader(737);
$header->printHTML();

$stringManipulator = new StringManipulation();

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{block1-title}', '{/block1-title}');
$block1title = $stringManipulator->neededSubString;
//$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{block1-content}', '{/block1-content}');
$block1content = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

// bottom red line content
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/wwdo_768.jpg" id="topBanner">


    <div class="careers-block-title"><?php echo $block1title; ?></div>
    <div class="careers-block-text" style="text-align: justify;"><?php echo $block1content; ?></div>



    <!-- -->
    <style>
        @media screen and (max-width: 320px) {
            #sl-block1-button{
                background-color: #EB5C4E;
                color: #fff;
                font-size: 18px;
                padding: 10px 0;
                text-align: center;
                width: 150px;
                margin: 50px auto;

            }
            #sl_bg{
                display: block;
                margin: 0 auto;
                width: 300px;
            }
            #sl_benefits{
                z-index: 100;
                margin-top: -120px;
            }
            #sl_benefits_tit {
                width: 100%;
                text-align: center;
                vertical-align: middle;
                color: #fff;
                opacity: 0.75;
                font-size: 30px;
                text-shadow: 1px 1px 4px #a0a0a0;
            }
            #sl_benefits_ul{
                list-style: none;
                padding: 0;
                margin: 0 auto;
                width: 200px;
                text-align: center;
            }
            #sl_benefits_ul li{
                opacity: 0.9;
                padding: 28px 0;
            }
            #sl_benefits_ul li:nth-child(even){
                background-color: rgba(240, 240, 240, 0.97);
            }
            #sl_benefits_ul li:nth-child(odd){
                background-color: rgba(246, 246, 246, 0.97);
            }
            #sl_benefits_ul img{
                width: 50px;
                height: auto;
                margin: 0 auto;
                display: block;
            }
            .sl_benefits_subtitles{
                font-size: 16px;
                line-height: 1.5;
                font-weight: 300;
                color: #3c4246;
                font-family: 'Roboto', sans-serif;
                margin: 15px 0 10px;
            }
            .sl_benefits_subtext{
                font-size: 14px;
                color: #818181;
                font-weight: 100;
            }
        }
        @media screen and (max-width: 478px) and (min-width: 320px) {
            #sl-block1-button{
                background-color: #EB5C4E;
                color: #fff;
                font-size: 18px;
                padding: 10px 0;
                text-align: center;
                width: 150px;
                margin: 50px auto;

            }
            #sl_bg{
                display: block;
                margin: 0 auto;
                width: 300px;
            }
            #sl_benefits{
                z-index: 100;
                margin-top: -120px;
            }
            #sl_benefits_tit {
                width: 100%;
                text-align: center;
                vertical-align: middle;
                color: #fff;
                opacity: 0.75;
                font-size: 30px;
                text-shadow: 1px 1px 4px #a0a0a0;
            }
            #sl_benefits_ul{
                list-style: none;
                padding: 0;
                margin: 0 auto;
                width: 200px;
                text-align: center;
            }
            #sl_benefits_ul li{
                opacity: 0.9;
                padding: 28px 0;
            }
            #sl_benefits_ul li:nth-child(even){
                background-color: rgba(240, 240, 240, 0.97);
            }
            #sl_benefits_ul li:nth-child(odd){
                background-color: rgba(246, 246, 246, 0.97);
            }
            #sl_benefits_ul img{
                width: 50px;
                height: auto;
                margin: 0 auto;
                display: block;
            }
            .sl_benefits_subtitles{
                font-size: 16px;
                line-height: 1.5;
                font-weight: 300;
                color: #3c4246;
                font-family: 'Roboto', sans-serif;
                margin: 15px 0 10px;
            }
            .sl_benefits_subtext{
                font-size: 14px;
                color: #818181;
                font-weight: 100;
            }
        }
        @media screen and (max-width: 766px) and (min-width: 478px) {
            #sl-block1-button{
                background-color: #EB5C4E;
                color: #fff;
                font-size: 18px;
                padding: 10px 0;
                text-align: center;
                width: 150px;
                margin: 50px auto;

            }
            #sl_bg{
                display: block;
                margin: 0 auto;
                width: 450px;
            }
            #sl_benefits{
                z-index: 100;
                margin-top: -150px;
            }
            #sl_benefits_tit {
                width: 100%;
                text-align: center;
                vertical-align: middle;
                color: #fff;
                opacity: 0.75;
                font-size: 32px;
                text-shadow: 1px 1px 4px #a0a0a0;
                margin-bottom: 14px;
            }
            #sl_benefits_ul{
                list-style: none;
                padding: 0;
                margin: 0 auto;
                width: 450px;
                text-align: center;
            }
            #sl_benefits_ul li{
                opacity: 0.9;
                padding: 28px 0;
                display: inline-block;
                width: 218px;
                min-height: 160px;
                margin: 0;
                vertical-align: top;
            }
            #sl_benefits_ul li:nth-child(1){
                background-color: rgba(240, 240, 240, 0.97);
            }
            #sl_benefits_ul li:nth-child(2){
                background-color: rgba(240, 240, 240, 0.97);
            }
            #sl_benefits_ul li:nth-child(3){
                background-color: rgba(246, 246, 246, 0.97);
            }
            #sl_benefits_ul li:nth-child(4){
                background-color: rgba(246, 246, 246, 0.97);
            }
            #sl_benefits_ul img{
                width: 50px;
                height: auto;
                margin: 0 auto;
                display: block;
            }
            .sl_benefits_subtitles{
                font-size: 16px;
                line-height: 1.5;
                font-weight: 300;
                color: #3c4246;
                font-family: 'Roboto', sans-serif;
                margin: 15px 0 10px;
            }
            .sl_benefits_subtext{
                font-size: 14px;
                color: #818181;
                font-weight: 100;
            }
        }
        @media screen and (max-width: 1022px) and (min-width: 766px){
            #sl-block1-button{
                background-color: #EB5C4E;
                color: #fff;
                font-size: 18px;
                padding: 10px 0;
                text-align: center;
                width: 150px;
                margin: 50px auto;

            }
            #sl_bg{
                display: block;
                margin: 0 auto;
                width: 750px;
            }
            #sl_benefits{
                z-index: 100;
                margin-top: -220px;
            }
            #sl_benefits_tit {
                width: 100%;
                text-align: center;
                vertical-align: middle;
                color: #fff;
                opacity: 0.75;
                font-size: 38px;
                text-shadow: 1px 1px 4px #a0a0a0;
                margin-bottom: 14px;
            }
            #sl_benefits_ul{
                list-style: none;
                padding: 0;
                margin: 0 auto;
                width: 450px;
                text-align: center;
            }
            #sl_benefits_ul li{
                opacity: 0.9;
                padding: 28px 0;
                display: inline-block;
                width: 218px;
                min-height: 160px;
                margin: 0;
                vertical-align: top;
            }
            #sl_benefits_ul li:nth-child(1){
                background-color: rgba(240, 240, 240, 0.97);
            }
            #sl_benefits_ul li:nth-child(2){
                background-color: rgba(240, 240, 240, 0.97);
            }
            #sl_benefits_ul li:nth-child(3){
                background-color: rgba(246, 246, 246, 0.97);
            }
            #sl_benefits_ul li:nth-child(4){
                background-color: rgba(246, 246, 246, 0.97);
            }
            #sl_benefits_ul img{
                width: 50px;
                height: auto;
                margin: 0 auto;
                display: block;
            }
            .sl_benefits_subtitles{
                font-size: 17px;
                line-height: 1.5;
                font-weight: 300;
                color: #3c4246;
                font-family: 'Roboto', sans-serif;
                margin: 15px 0 10px;
            }
            .sl_benefits_subtext{
                font-size: 14px;
                color: #818181;
                font-weight: 100;
            }
        }
    </style>

    <div id="sl-block1-button">FREE DEMO</div>

    <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/mobile_bg.jpg" id="sl_bg" />

    <div id="sl_benefits">
        <div id="sl_benefits_tit">SOCIAL LISTENING BENEFITS</div>
        <ul id="sl_benefits_ul">
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/binoculars.png" />
                <div class="sl_benefits_subtitles">INTELLIGENCE</div>
                <div class="sl_benefits_subtext">It's never been easier to gain relevant segment insights</div>
            </li>
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/chess.png" />
                <div class="sl_benefits_subtitles">COMPETITION</div>
                <div class="sl_benefits_subtext">Hard to outplay them if you don't know them</div>
            </li>
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/magnet.png" />
                <div class="sl_benefits_subtitles">INFLUENCE</div>
                <div class="sl_benefits_subtext">Get closer to the experts, learn & engage</div>
            </li>
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/social_listening/atomic.png" />
                <div class="sl_benefits_subtitles">OPPORTUNITIES</div>
                <div class="sl_benefits_subtext">People actually look for product & services suggestions on social media</div>
            </li>
        </ul>
    </div>




<style>

    #socialListeningUL textarea::-webkit-input-placeholder {
        color: #808080 !important;
    }
    #socialListeningUL textarea:-moz-placeholder { /* Firefox 18- */
        color: #808080 !important;
    }
    #socialListeningUL textarea::-moz-placeholder {  /* Firefox 19+ */
        color: #808080 !important;
    }
    #socialListeningUL textarea:-ms-input-placeholder {
        color: #808080 !important;
    }
    #socialListeningUL textarea:focus {
        outline: none !important;
        border: 2px solid #EB5C4E;
    }


    #socialListeningUL input::-webkit-input-placeholder { /* Chrome/Opera/Safari */
        color: #808080 !important;
    }
    #socialListeningUL input::-moz-placeholder { /* Firefox 19+ */
        color: #808080 !important;
    }
    #socialListeningUL input:-ms-input-placeholder { /* IE 10+ */
        color: #808080 !important;
    }
    #socialListeningUL input:-moz-placeholder { /* Firefox 18- */
        color: #808080 !important;
    }
    #socialListeningUL input:focus {
        outline: none !important;
        border: 2px solid #EB5C4E;
    }

    @media screen and (max-width: 320px){
        #sl-form{
            list-style: none;
            padding: 20px 0;
            margin: 20px auto;
            width: 310px;
            background-color: #F9F9F9;
        }
        #sl-form li{
            margin: 30px 0;
        }
        .sl-form-subtit{
            font-size: 16px;
            line-height: 1.5;
            font-weight: 700;
            color: #3c4246;
            font-family: 'Roboto', sans-serif;
        }
        #sl-form input{
            width: 284px;
            display: block;
            height: 30px;
            border: 2px solid #E4E4E4;
            padding-left: 10px;
            font-size: 14px;
            color: #3c4246;
            vertical-align: top;
            margin: 2px auto;
        }

        /* objectives */
        #sl-form-objectives{
            width: 290px;
            height: 25px;
            border: 2px solid #E4E4E4;
            text-align: center;
            font-size: 14px;
            font-weight: 400;
            color: #818181;
            vertical-align: top;
            padding-top: 8px;
            margin: 2px auto 0;
        }
        #social_listening-plus{
            color: #EB5C4E;
            font-size: 25px;
            font-weight: bold;
            vertical-align: top;
            margin-top: -8px;
            margin-right: 15px;
            float: right;
        }
        .social_listening-plusRotate{
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
            transition: 0.4s;
        }
        #socialListening-achieveDropdown{
            background-color: #EB5C4E;
            position: absolute;
            width: 453px;
            display: none;
            height: 0;
        }

        /* sources */
        .social_listening-sources{
            width: 148px;
            height: 27px;
            border: 2px solid #E4E4E4;
            text-align: center;
            font-size: 16px;
            color: #212121;
            vertical-align: top;
            padding-top: 8px;
            margin-top: 2px;
            display: inline-block;
        }
        #social_listening-sources-1{

        }
        #social_listening-sources-2{

        }
        .socialListeningSourcesActive{
            border-color: #EB5C4E;
            background-color: #EB5C4E;
            color: #fff;
        }

        /* textarea */
        #sl-form textarea{
            width: 290px;
            height: 100px;
            display: block;
            margin: 0 auto;
        }

        /* submit button */
        #socialListeningForm-submit{
            width: 220px;
            margin: 0 auto 50px;
            background-color: #EB5C4E;
            color: #fff;
            text-align: center;
            padding: 10px 0;
        }
    }
    @media screen and (max-width: 478px) and (min-width: 320px){
        #sl-form{
            list-style: none;
            padding: 20px 0;
            margin: 20px auto;
            width: 310px;
            background-color: #F9F9F9;
        }
        #sl-form li{
            margin: 30px 0;
        }
        .sl-form-subtit{
            font-size: 16px;
            line-height: 1.5;
            font-weight: 700;
            color: #3c4246;
            font-family: 'Roboto', sans-serif;
        }
        #sl-form input{
            width: 284px;
            display: block;
            height: 30px;
            border: 2px solid #E4E4E4;
            padding-left: 10px;
            font-size: 14px;
            color: #3c4246;
            vertical-align: top;
            margin: 2px auto;
        }

        /* objectives */
        #sl-form-objectives{
            width: 290px;
            height: 25px;
            border: 2px solid #E4E4E4;
            text-align: center;
            font-size: 14px;
            font-weight: 400;
            color: #818181;
            vertical-align: top;
            padding-top: 8px;
            margin: 2px auto 0;
        }
        #social_listening-plus{
            color: #EB5C4E;
            font-size: 25px;
            font-weight: bold;
            vertical-align: top;
            margin-top: -8px;
            margin-right: 15px;
            float: right;
            transition: 0.4s;
        }
        .social_listening-plusRotate{
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
            transition: 0.4s;
        }
        #socialListening-achieveDropdown{
            background-color: #EB5C4E;
            position: absolute;
            width: 294px;
            display: none;
            height: 0;
            padding: 0;
            margin-left: 8px;
        }
        #socialListening-achieveDropdown-other{
            width: 236px !important;
        }
        #socialListening-achieveDropdown-ok{
            background-color: #fff;
            border-radius: 3px;
            color: #818181;
            width: 70px;
            text-align: center;
            padding: 5px 0;
            margin: 0 auto;
        }
        #socialListening-achieveDropdown li{
            border-bottom: 1px solid #fff;
            margin: 0;
            padding: 10px 20px;
            color: #fff;
            font-size: 16px;
            font-weight: 400;
            background-color: #EB5C4E;
            transition: 0.3s;
            display: none;
        }
        #socialListening-achieveDropdown li:hover{
            cursor: pointer;
            background-color: #fff;
            color: #818181;
            transition: 0.3s;
        }

        /* sources */
        .social_listening-sources{
            width: 148px;
            height: 27px;
            border: 2px solid #E4E4E4;
            text-align: center;
            font-size: 16px;
            color: #212121;
            vertical-align: top;
            padding-top: 8px;
            margin-top: 2px;
            display: inline-block;
        }
        #social_listening-sources-1{

        }
        #social_listening-sources-2{

        }
        .socialListeningSourcesActive{
            border-color: #EB5C4E;
            background-color: #EB5C4E;
            color: #fff;
        }

        /* textarea */
        #sl-form textarea{
            width: 290px;
            height: 100px;
            display: block;
            margin: 0 auto;
        }

        /* submit button */
        #socialListeningForm-submit{
            width: 220px;
            margin: 0 auto 50px;
            background-color: #EB5C4E;
            color: #fff;
            text-align: center;
            padding: 10px 0;
        }
    }
    @media screen and (max-width: 766px) and (min-width: 478px){
        #sl-form{
            list-style: none;
            padding: 20px 0;
            margin: 20px auto;
            width: 310px;
            background-color: #F9F9F9;
        }
        #sl-form li{
            margin: 30px 0;
        }
        .sl-form-subtit{
            font-size: 16px;
            line-height: 1.5;
            font-weight: 700;
            color: #3c4246;
            font-family: 'Roboto', sans-serif;
        }
        #sl-form input{
            width: 284px;
            display: block;
            height: 30px;
            border: 2px solid #E4E4E4;
            padding-left: 10px;
            font-size: 14px;
            color: #3c4246;
            vertical-align: top;
            margin: 2px auto;
        }

        /* objectives */
        #sl-form-objectives{
            width: 290px;
            height: 25px;
            border: 2px solid #E4E4E4;
            text-align: center;
            font-size: 14px;
            font-weight: 400;
            color: #818181;
            vertical-align: top;
            padding-top: 8px;
            margin: 2px auto 0;
        }
        #social_listening-plus{
            color: #EB5C4E;
            font-size: 25px;
            font-weight: bold;
            vertical-align: top;
            margin-top: -8px;
            margin-right: 15px;
            float: right;
            transition: 0.4s;
        }
        .social_listening-plusRotate{
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
            transition: 0.4s;
        }
        #socialListening-achieveDropdown{
            background-color: #EB5C4E;
            position: absolute;
            width: 294px;
            display: none;
            height: 0;
            padding: 0;
            margin-left: 8px;
        }
        #socialListening-achieveDropdown-other{
            width: 236px !important;
        }
        #socialListening-achieveDropdown-ok{
            background-color: #fff;
            border-radius: 3px;
            color: #818181;
            width: 70px;
            text-align: center;
            padding: 5px 0;
            margin: 0 auto;
        }
        #socialListening-achieveDropdown li{
            border-bottom: 1px solid #fff;
            margin: 0;
            padding: 10px 20px;
            color: #fff;
            font-size: 16px;
            font-weight: 400;
            background-color: #EB5C4E;
            transition: 0.3s;
            display: none;
        }
        #socialListening-achieveDropdown li:hover{
            cursor: pointer;
            background-color: #fff;
            color: #818181;
            transition: 0.3s;
        }

        /* sources */
        .social_listening-sources{
            width: 148px;
            height: 27px;
            border: 2px solid #E4E4E4;
            text-align: center;
            font-size: 16px;
            color: #212121;
            vertical-align: top;
            padding-top: 8px;
            margin-top: 2px;
            display: inline-block;
        }
        #social_listening-sources-1{

        }
        #social_listening-sources-2{

        }
        .socialListeningSourcesActive{
            border-color: #EB5C4E;
            background-color: #EB5C4E;
            color: #fff;
        }

        /* textarea */
        #sl-form textarea{
            width: 290px;
            height: 100px;
            display: block;
            margin: 0 auto;
        }

        /* submit button */
        #socialListeningForm-submit{
            width: 220px;
            margin: 0 auto 50px;
            background-color: #EB5C4E;
            color: #fff;
            text-align: center;
            padding: 10px 0;
        }
    }
    @media screen and (max-width: 1022px) and (min-width: 766px){
        #sl-form{
            list-style: none;
            padding: 20px 0;
            margin: 20px auto;
            width: 740px;
            background-color: #F9F9F9;
        }
        #sl-form li{
            margin: 30px 0;
        }
        .sl-form-subtit{
            font-size: 16px;
            line-height: 1.5;
            font-weight: 700;
            color: #3c4246;
            font-family: 'Roboto', sans-serif;
            display: inline-block;
            width: 300px;
            padding-right: 15px;
            text-align: right;
            vertical-align: top;
            margin-top: 3px;
        }
        .sl-form-subfields{
            display: inline-block;
            vertical-align: top;
        }
        #sl-form input{
            width: 294px;
            display: block;
            height: 30px;
            border: 2px solid #E4E4E4;
            padding-left: 10px;
            font-size: 14px;
            color: #3c4246;
            vertical-align: top;
            margin: 4px auto;
        }

        /* objectives */
        #sl-form-objectives{
            width: 303px;
            height: 25px;
            border: 2px solid #E4E4E4;
            text-align: center;
            font-size: 14px;
            font-weight: 400;
            color: #818181;
            vertical-align: top;
            padding-top: 8px;
            margin: 2px auto 0;
        }
        #social_listening-plus{
            color: #EB5C4E;
            font-size: 25px;
            font-weight: bold;
            vertical-align: top;
            margin-top: -8px;
            margin-right: 15px;
            float: right;
            transition: 0.4s;
        }
        .social_listening-plusRotate{
            -ms-transform: rotate(45deg); /* IE 9 */
            -webkit-transform: rotate(45deg); /* Chrome, Safari, Opera */
            transform: rotate(45deg);
            transition: 0.4s;
        }
        #socialListening-achieveDropdown{
            background-color: #EB5C4E;
            position: absolute;
            width: 306px;
            margin-left: 319px;
            display: none;
            height: 0;
            padding: 0;
        }
        #socialListening-achieveDropdown-other{
            width: 252px !important;
        }
        #socialListening-achieveDropdown-ok{
            background-color: #fff;
            border-radius: 3px;
            color: #818181;
            width: 70px;
            text-align: center;
            padding: 5px 0;
            margin: 0 auto;
        }
        #socialListening-achieveDropdown li{
            border-bottom: 1px solid #fff;
            margin: 0;
            padding: 10px 20px;
            color: #fff;
            font-size: 16px;
            font-weight: 400;
            background-color: #EB5C4E;
            transition: 0.3s;
            display: none;
        }
        #socialListening-achieveDropdown li:hover{
            cursor: pointer;
            background-color: #fff;
            color: #818181;
            transition: 0.3s;
        }

        /* sources */
        .social_listening-sources{
            width: 148px;
            height: 27px;
            border: 2px solid #E4E4E4;
            text-align: center;
            font-size: 16px;
            color: #212121;
            vertical-align: top;
            padding-top: 8px;
            margin-top: 2px;
            display: inline-block;
        }
        #social_listening-sources-1{

        }
        #social_listening-sources-2{

        }
        .socialListeningSourcesActive{
            border-color: #EB5C4E;
            background-color: #EB5C4E;
            color: #fff;
        }

        /* textarea */
        #sl-form textarea{
            width: 303px;
            height: 100px;
            display: block;
            margin: 0 auto;
        }

        /* submit button */
        #socialListeningForm-submit{
            width: 220px;
            margin: 0 auto 50px;
            background-color: #EB5C4E;
            color: #fff;
            text-align: center;
            padding: 12px 0;
        }
    }
</style>

    <div class="careers-block-title" id="sl-the-tit">REQUEST DEMO</div>

    <form>
    <ul id="sl-form">
        <li>
            <div class="sl-form-subtit">OBJECTIVES</div>
            <div class="sl-form-subfields" id="sl-form-objectives">
                <input type="hidden" name="objectives" id="objectivesHiddenInput" />
                <span id="socialListening-achieveText">WHAT DO YOU WISH TO ACHIEVE?</span>
                <span id="social_listening-plus">+</span>
            </div>
            <ul id="socialListening-achieveDropdown">
                <li class="socialListening-achieveLi">Find new business</li>
                <li class="socialListening-achieveLi">Monitor & boost brand success</li>
                <li class="socialListening-achieveLi">Monitor competition</li>
                <li class="socialListening-achieveLi">Measure campaign effectiveness</li>
                <li>
                    <input type="text" placeholder="Other..." value="" id="socialListening-achieveDropdown-other"/>
                </li>
                <li id="socialListening-achieveDropdown-ok-row">
                    <div id="socialListening-achieveDropdown-ok">OK</div>
                </li>
            </ul>
        </li>
        <li>
            <div class="sl-form-subtit">TARGET AUDIENCE</div>
            <div class="sl-form-subfields">
                <input type="text" placeholder="What do you want to listen to?" name="target-what" />
                <input type="text" placeholder="What industry are you interested in?" name="target-industry" />
            </div>
        </li>
        <li>
            <div class="sl-form-subtit">INFLUENCERS & SOURCES</div>
            <div class="sl-form-subfields">
                <input type="hidden" id="socialListeningSourcesInput" name="sources" value="not selected" />
                <div class="social_listening-sources" id="social_listening-sources-2">Social accounts</div>
                <div class="social_listening-sources" id="social_listening-sources-1">URLs</div>
            </div>
        </li>
        <li>
            <div class="sl-form-subtit">HOT TOPICS & KEYWORDS</div>
            <div class="sl-form-subfields">
                <input type="text" placeholder="What is important in your line of business?" name="keywords-important" />
                <input type="text" placeholder="What language does your audience speak?" name="keywords-language" />
            </div>
        </li>
        <li>
            <div class="sl-form-subtit">SOCIAL PRESENCE</div>
            <div class="sl-form-subfields">
                <input type="text" placeholder="Where are you active?" name="presence-where" />
                <input type="text" placeholder="What content are you creating?" name="presence-content" />
                <input type="text" placeholder="From where is the social conversation?" name="presence-conversation" />
            </div>
        </li>
        <li>
            <div class="sl-form-subtit">ADDITIONAL INFORMATION</div>
            <div class="sl-form-subfields">
                <textarea name="additional-info" placeholder="Add more details..."></textarea>
            </div>
        </li>
    </ul>

        <input type="submit" style="display:none;" id="socialListeningFormSend"/>
        <div id="socialListeningForm-submit">SUBMIT YOUR REQUEST</div>

    </form>


    <!-- footer -->
<?php

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();