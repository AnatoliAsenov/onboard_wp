<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$postID = get_query_var( 'id' );
$singlePost = get_post( $postID );

$header = new MobileHeader($postID);
$header->printHTML();

$siteURL = get_site_url();

$postContent = $singlePost->post_content;
$postLocation = get_post_meta($postID, 'location', true);
$postLanguages = get_post_meta($postID, 'languages', true);

$stringManipulator = new StringManipulation();

//var_dump($postContent);
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">careers</div>


<!-- top banner -->
<style>
    @media screen and (max-width: 320px) {
        #singleJobBanner_container{
            width: 320px;
            margin: 80px auto 0 auto;
        }
        #singleJobBanner{
            display: block;
            content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/singleJob_tit_320.png");
            height: auto;
            width: 225px;
            margin-right: 95px;
        }
        #singleJobTitleContainer{
            width: 290px;
            margin: -66px auto 0 auto;
        }
        #singleJobTitle{
            width: 200px;
            margin-left: 90px;
            font-size: 18px;
            color: grey;
            text-align: right;
        }
        /*-- language, location and apply btn --*/
        #singleJobUl{
            width: 290px;
            margin: 40px auto;
            padding: 0;
        }
        #singleJobUl p{
            display: inline-block;
            font-size: 13px;
            color: #EB5C4E;
        }
        #singleJobUl i{
            display: inline-block;
            color: #EB5C4E;
            font-size: 24px;
        }
        #singleJobUl1{
            display: inline-block;
        }
        #singleJobUl2{
            display: inline-block;
            float: right;
        }

        /*-- main content --*/
        #apply-tab{
            background-color: #F1F1F1;
            width: 290px;
            margin: 20px auto 0 auto;
            padding: 30px 20px;
        }
        #apply-tab h2{
            font-size: 18px;
            font-weight: 600;
            text-align: left;
            color: #2a2a2a;
            margin-top: 60px;
        }
        #apply-tab p{
            font-size: 17px;
            font-weight: 300;
            text-align: left;
            color: #2a2a2a;
            margin: 14px 0 1px 0;
        }
        #apply-tab span{
            color: #EB5C4E;
            font-size: 18px;
            margin: 0 0 0 2px;
        }
        #apply-tab input[type='text']{
            width: 245px;
            height: 30px;
            font-size: 17px;
            font-weight: 300;
            text-align: center;
            color: #2a2a2a;
            border: 1px solid #ccc;
        }
        #apply-tab input[type='text']:focus{
            outline: none !important;
            border: 1px solid #EB5C4E;
        }
        #apply-tab input[type='file']{
            display: none;
        }
        #apply-table-attach-btn{
            border: 2px solid #E4E4E4;
            width: 242px;
            height: 40px;
            text-align: center;
        }
        #apply-table-attach-btn:hover{
            border-color: #EB5C4E;
            cursor: pointer;
            transition: 0.3s;
        }
        #apply-table-attach-btn p{
            font-size: 15px;
            color: #1a1a1a;
            display: inline-block;
            font-weight: 400;
            margin-top: 13px;
        }
        #apply-table-attach-btn i{
            font-size: 24px;
            color: #EB5C4E;
            display: inline-block;
            margin-right: 10px;
        }

        #apply-tab input[type='submit']{
            display: none;
        }
        #apply-tab textarea{
            width: 240px;
            height: 100px;
        }
        #apply-tab textarea:focus{
            outline: none !important;
            border: 1px solid #EB5C4E;
        }
        #apply-tab-submit{
            color: #fff;
            background-color: #EB5C4E;
            border: 2px solid #EB5C4E;
            padding: 5px 0;
            font-size: 17px;
            font-weight: 300;
            width: 250px;
            margin: 50px auto;
            text-align: center;
        }
        #apply-tab-submit:hover{
            background-color: #fff;
            color: #EB5C4E;
        }
    }
    @media screen and (max-width: 478px) and (min-width: 320px) {
        #singleJobBanner_container{
            width: 320px;
            margin: 80px auto 0 auto;
        }
        #singleJobBanner{
            display: block;
            content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/singleJob_tit_320.png");
            height: auto;
            width: 225px;
            margin-right: 95px;
        }
        #singleJobTitleContainer{
            width: 290px;
            margin: -66px auto 0 auto;
        }
        #singleJobTitle{
            width: 200px;
            margin-left: 90px;
            font-size: 18px;
            color: grey;
            text-align: right;
        }
        /*-- language, location and apply btn --*/
        #singleJobUl{
            width: 290px;
            margin: 40px auto;
            padding: 0;
        }
        #singleJobUl p{
            display: inline-block;
            font-size: 13px;
            color: #EB5C4E;
        }
        #singleJobUl i{
            display: inline-block;
            color: #EB5C4E;
            font-size: 24px;
        }
        #singleJobUl1{
            display: inline-block;
        }
        #singleJobUl2{
            display: inline-block;
            float: right;
        }

        /*-- main content --*/
        #apply-tab{
            background-color: #F1F1F1;
            width: 290px;
            margin: 20px auto 0 auto;
            padding: 30px 20px;
        }
        #apply-tab h2{
            font-size: 18px;
            font-weight: 600;
            text-align: left;
            color: #2a2a2a;
            margin-top: 60px;
        }
        #apply-tab p{
            font-size: 17px;
            font-weight: 300;
            text-align: left;
            color: #2a2a2a;
            margin: 14px 0 1px 0;
        }
        #apply-tab span{
            color: #EB5C4E;
            font-size: 18px;
            margin: 0 0 0 2px;
        }
        #apply-tab input[type='text']{
            width: 245px;
            height: 30px;
            font-size: 17px;
            font-weight: 300;
            text-align: center;
            color: #2a2a2a;
            border: 1px solid #ccc;
        }
        #apply-tab input[type='text']:focus{
            outline: none !important;
            border: 1px solid #EB5C4E;
        }
        #apply-tab input[type='file']{
            display: none;
        }
        #apply-table-attach-btn{
            border: 2px solid #E4E4E4;
            width: 242px;
            height: 40px;
            text-align: center;
        }
        #apply-table-attach-btn:hover{
            border-color: #EB5C4E;
            cursor: pointer;
            transition: 0.3s;
        }
        #apply-table-attach-btn p{
            font-size: 15px;
            color: #1a1a1a;
            display: inline-block;
            font-weight: 400;
            margin-top: 13px;
        }
        #apply-table-attach-btn i{
            font-size: 24px;
            color: #EB5C4E;
            display: inline-block;
            margin-right: 10px;
        }

        #apply-tab input[type='submit']{
            display: none;
        }
        #apply-tab textarea{
            width: 240px;
            height: 100px;
        }
        #apply-tab textarea:focus{
            outline: none !important;
            border: 1px solid #EB5C4E;
        }
        #apply-tab-submit{
            color: #fff;
            background-color: #EB5C4E;
            border: 2px solid #EB5C4E;
            padding: 5px 0;
            font-size: 17px;
            font-weight: 300;
            width: 250px;
            margin: 50px auto;
            text-align: center;
        }
        #apply-tab-submit:hover{
            background-color: #fff;
            color: #EB5C4E;
        }
    }
    @media screen and (max-width: 766px) and (min-width: 478px) {

        #singleJobBanner_container{
            width: 450px;
            margin: 80px auto 0 auto;
        }
        #singleJobBanner{
            display: block;
            content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/singleJob_tit_480.png");
            height: auto;
            width: 225px;
            margin-right: 95px;
        }
        #singleJobTitleContainer{
            width: 450px;
            margin: -88px auto 0 auto;
        }
        #singleJobTitle{
            width: 250px;
            margin-left: 200px;
            font-size: 24px;
            color: grey;
            text-align: right;
        }
        /*-- language, location and apply btn --*/
        #singleJobUl{
            width: 450px;
            margin: 40px auto;
            padding: 0;
        }
        #singleJobUl p{
            display: inline-block;
            font-size: 18px;
            color: #EB5C4E;
        }
        #singleJobUl i{
            display: inline-block;
            color: #EB5C4E;
            font-size: 30px;
            margin-right: 5px;
        }
        #singleJobUl1{
            display: inline-block;
        }
        #singleJobUl2{
            display: inline-block;
            float: right;
        }

        /*-- main content --*/
        #apply-tab{
            background-color: #F1F1F1;
            width: 450px;
            margin: 20px auto 0 auto;
            padding: 30px 20px;
        }
        #apply-tab h2{
            font-size: 18px;
            font-weight: 600;
            text-align: left;
            color: #2a2a2a;
            margin-top: 60px;
        }
        #apply-tab p{
            font-size: 17px;
            font-weight: 300;
            text-align: left;
            color: #2a2a2a;
            margin: 14px 0 1px 0;
            display: inline-block;
            float: left;
        }
        #apply-tab span{
            color: #EB5C4E;
            font-size: 18px;
            margin: 0 0 0 2px;
        }
        #apply-tab input[type='text']{
            width: 245px;
            height: 30px;
            font-size: 17px;
            font-weight: 300;
            text-align: center;
            color: #2a2a2a;
            border: 1px solid #ccc;
            display: inline-block;
            float: right;
        }
        #apply-tab input[type='text']:focus{
            outline: none !important;
            border: 1px solid #EB5C4E;
        }
        #apply-tab input[type='file']{
            display: none;
        }
        #apply-table-attach-btn{
            border: 2px solid #E4E4E4;
            width: 242px;
            height: 40px;
            text-align: center;
            display: inline-block;
            float: right;
        }
        #apply-table-attach-btn:hover{
            border-color: #EB5C4E;
            cursor: pointer;
            transition: 0.3s;
        }
        #apply-table-attach-btn p{
            font-size: 15px;
            color: #1a1a1a;
            display: inline-block;
            font-weight: 400;
            margin-top: 13px;
        }
        #apply-table-attach-btn i{
            font-size: 24px;
            color: #EB5C4E;
            display: inline-block;
            float: left;
            margin: 5px 20px 0 20px;
        }

        #apply-tab input[type='submit']{
            display: none;
        }
        #apply-tab textarea{
            width: 395px;
            height: 100px;
        }
        #apply-tab textarea:focus{
            outline: none !important;
            border: 1px solid #EB5C4E;
        }
        #apply-tab-submit{
            color: #fff;
            background-color: #EB5C4E;
            border: 2px solid #EB5C4E;
            padding: 5px 0;
            font-size: 17px;
            font-weight: 300;
            width: 250px;
            margin: 50px auto;
            text-align: center;
        }
        #apply-tab-submit:hover{
            background-color: #fff;
            color: #EB5C4E;
        }
    }
    @media screen and (max-width: 1022px) and (min-width: 766px){
        #singleJobBanner_container{
            width: 750px;
            margin: 155px auto 0 auto;
        }
        #singleJobBanner{
            display: block;
            content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/singleJob_tit_768.png");
            height: auto;
            width: 312px;
            margin-right: 438px;
        }
        #singleJobTitleContainer{
            width: 750px;
            margin: -74px auto 0 auto;
        }
        #singleJobTitle{
            width: 500px;
            margin-left: 250px;
            font-size: 30px;
            color: grey;
            text-align: right;
        }
        /*-- language, location and apply btn --*/
        #singleJobUl{
            width: 750px;
            margin: 40px auto;
            padding: 0;
        }
        #singleJobUl p{
            display: inline-block;
            font-size: 18px;
            color: #EB5C4E;
        }
        #singleJobUl i{
            display: inline-block;
            color: #EB5C4E;
            font-size: 45px;
            margin-right: 8px;
            vertical-align: middle;
        }
        #singleJobUl1{
            display: inline-block;
            margin-left: 250px;
        }
        #singleJobUl2{
            display: inline-block;
            margin-left: 108px;
        }

        /*-- main content --*/
        #apply-tab{
            background-color: #F1F1F1;
            width: 734px;
            margin: 20px auto 0 auto;
            padding: 30px 20px;
        }
        #apply-tab h2{
            font-size: 18px;
            font-weight: 600;
            text-align: left;
            color: #2a2a2a;
            margin-top: 60px;
        }
        #apply-tab p{
            font-size: 17px;
            font-weight: 300;
            text-align: left;
            color: #2a2a2a;
            margin: 14px 0 1px 0;
            display: inline-block;
            float: left;
        }
        #apply-tab span{
            color: #EB5C4E;
            font-size: 18px;
            margin: 0 0 0 2px;
        }
        #apply-tab input[type='text']{
            width: 495px;
            height: 30px;
            font-size: 17px;
            font-weight: 300;
            text-align: center;
            color: #2a2a2a;
            border: 1px solid #ccc;
            display: inline-block;
            float: right;
        }
        #apply-tab input[type='text']:focus{
            outline: none !important;
            border: 1px solid #EB5C4E;
        }
        #apply-tab input[type='file']{
            display: none;
        }
        #apply-table-attach-btn{
            border: 2px solid #E4E4E4;
            width: 242px;
            height: 40px;
            text-align: center;
            display: inline-block;
            float: left;
            margin-left: 100px;
        }
        #apply-table-attach-btn:hover{
            border-color: #EB5C4E;
            cursor: pointer;
            transition: 0.3s;
        }
        #apply-table-attach-btn p{
            font-size: 15px;
            color: #1a1a1a;
            display: inline-block;
            font-weight: 400;
            margin-top: 13px;
        }
        #apply-table-attach-btn i{
            font-size: 24px;
            color: #EB5C4E;
            display: inline-block;
            float: left;
            margin: 5px 20px 0 20px;
        }

        #apply-tab input[type='submit']{
            display: none;
        }
        #apply-tab textarea{
            width: 680px;
            height: 120px;
        }
        #apply-tab textarea:focus{
            outline: none !important;
            border: 1px solid #EB5C4E;
        }
        #apply-tab-submit{
            color: #fff;
            background-color: #EB5C4E;
            border: 2px solid #EB5C4E;
            padding: 8px 0;
            font-size: 18px;
            font-weight: 300;
            width: 260px;
            margin: 50px auto;
            text-align: center;
        }
        #apply-tab-submit:hover{
            background-color: #fff;
            color: #EB5C4E;
        }
    }
</style>
<div id="singleJobBanner_container">
    <img id="singleJobBanner" src="" alt="careers" />
</div>

<div id="singleJobTitleContainer">
    <div id="singleJobTitle"><?php echo $singlePost->post_title; ?></div>
</div>

<ul id="singleJobUl">
    <li id="singleJobUl1">
        <i class="fa fa-language" aria-hidden="true"></i>
        <p><?php echo $postLanguages; ?></p>
    </li>
    <li id="singleJobUl2">
        <i class="fa fa-globe" aria-hidden="true"></i>
        <p><?php echo $postLocation; ?></p>
    </li>
</ul>

<table id="apply-tab">
    <tr>
        <th><h2>SUBMIT YOUR APPLICATION</h2></th>
    </tr>
<!-- personal info -->
    <tr>
        <th>
            <p>Resume/CV</p>
            <div id="apply-table-attach-btn">
                <i class="fa fa-paperclip" aria-hidden="true"></i>
                <p>ATTACH RESUME/CV</p>
            </div>
            <input type="file" name="uploaded_file" />
        </th>
    </tr>
    <tr>
        <th>
            <p>First name<span>*</span></p>
            <input type="text" name="FirstName" />
        </th>
    </tr>
    <tr>
        <th>
            <p>Last name<span>*</span></p>
            <input type="text" name="LastName" />
        </th>
    </tr>
    <tr>
        <th>
            <p>Email<span>*</span></p>
            <input type="text" name="email" />
        </th>
    </tr>
    <tr>
        <th>
            <p>Phone<span>*</span></p>
            <input type="text" name="phone" />
        </th>
    </tr>
    <tr>
        <th>
            <p>Current company</p>
            <input type="text" name="company" />
        </th>
    </tr>
<!-- social links -->
    <tr>
        <th><h2>LINKS</h2></th>
    </tr>
    <tr>
        <th>
            <p>LinkedIn URL</p>
            <input type="text" name="linkedin" />
        </th>
    </tr>
    <tr>
        <th>
            <p>Other profile URL</p>
            <input type="text" name="social" />
        </th>
    </tr>
    <tr>
        <th><h2>ADDITIONAL INFORMATION</h2></th>
    </tr>
    <tr>
        <th><textarea placeholder="Add a cover letter or anything else you want to share."></textarea></th>
    </tr>
</table>

<div id="apply-tab-submit">SUBMIT YOUR APPLICATION</div>


    <!-- footer -->
<?php

$footer = new MobileFooter();
$footer->printHTML();