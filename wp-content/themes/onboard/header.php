<!DOCTYPE html>
<html <?php language_attributes(); ?> >

	<head>
<?php
		$slug = basename(get_permalink());
		$siteURL = get_site_url();
?>
		<script>
			if( document.documentElement.clientWidth < 1024 ){
				window.location = "<?php echo $siteURL; ?>" + "/mobile/mobile-home" + "<?php //echo $slug; ?>";
			}
		</script>

		<!-- Google Universal Analytics -->
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
						(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-2082072-3', 'auto');
			ga('require', 'GTM-TWHXKK3');
			ga('send', 'pageview');

		</script>

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
					new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
					j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
					'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-T9QNRDX');</script>
		<!-- End Google Tag Manager -->

		<style>.async-hide { opacity: 0 !important} </style>
		<script>(function(a,s,y,n,c,h,i,d,e){s.className+=' '+y;h.start=1*new Date;
				h.end=i=function(){s.className=s.className.replace(RegExp(' ?'+y),'')};
				(a[n]=a[n]||[]).hide=h;setTimeout(function(){i();h.end=null},c);h.timeout=c;
			})(window,document.documentElement,'async-hide','dataLayer',4000,
					{'GTM-TWHXKK3':true});</script>




		<meta content='text/html' charset='UTF-8' http-equiv='Content-Type' />
		<meta name="viewport" content="width=device-width">

		<!-- title -->
		<title><?php bloginfo('name'); ?></title>

		<!-- meta tags -->
		<meta name="description" content="<?php bloginfo('description'); ?>" />
		<meta name="keywords" content="B2B sales, Salesforce.com, CRM Consultancy, telemarketing, digital marketing services, customer relationship, outsourcing, data-driven marketing, channel sales, data quality services, global coverage, CRM automation, business development, market entry, Lead generation, Demand generation, B2B sales cycle, Marketing technology, Business process platform" />
		<meta name="robots" content="index, follow">

		<!-- fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic&subset=latin,cyrillic-ext,latin-ext,cyrillic' rel='stylesheet' type='text/css' >
		<!--link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Condensed" rel="stylesheet" --> 

		<!-- fav icon -->
		<link rel="icon" type="image/x-icon" href="<?php bloginfo('template_url'); ?>/images/favicon.ico" />

		<!-- styles -->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_loadingAnimation.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/font-awesome.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_fonts.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_topMenu.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_bottomRedLine.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_footer.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_about.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_arrowUP.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_about-company.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_caseStudy.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/what_we_do/_whatwedo.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/what_we_do/_social-listening.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/what_we_do/_web-development.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/what_we_do/_channel-marketing.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/what_we_do/_lead-generation.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_caseStudiesInOtherPages.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_middlePageBoxes.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/blog/_mainPage.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/blog/_singlePost.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/careers/_mainPage.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/careers/_singlePage.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/careers/_applyForm.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/contacts/_contacts.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/technology/_mainPage.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/technology/_universal.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/technology/_middleLine.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/technology/_salesForce.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/technology/_isoPages.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/facts/_partnership.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_thePopUp.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_alotof-text.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/_404.css" />
		<!-- "HOME" page style -->
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/lib/slick.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/lib/slick-theme.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/home/_sliders.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/home/_contactForm.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/home/_expertise.css" />
		<link rel="stylesheet" type="text/css" href="<?php bloginfo('template_url'); ?>/css/home/_services.css" />

		<!-- Google+ id -->
		<link rel="publisher" href="https://plus.google.com/+your_business_google_plus_id">

		<!-- FaceBook Open Graph -->
		<meta property="og:url" content="<?php echo get_post_meta($post->ID, 'fb_url', true); ?>" />
		<meta property="og:type" content="<?php echo get_post_meta($post->ID, 'fb_type', true); ?>" />
		<meta property="og:title" content="<?php echo get_post_meta($post->ID, 'fb_title', true); ?>" />
		<meta property="og:image" content="<?php echo get_post_meta($post->ID, 'fb_image_url', true); ?>" />
		<meta property="og:description" content="<?php echo get_post_meta($post->ID, 'fb_description', true); ?>" />


		<!-- Twitter Card -->
		<meta name="twitter:card" content="<?php echo get_post_meta($post->ID, 'tw_type', true); ?>">
		<meta name="twitter:site" content="<?php echo get_post_meta($post->ID, 'tw_site', true); ?>">
		<meta name="twitter:title" content="<?php echo get_post_meta($post->ID, 'tw_title', true); ?>" />
		<meta name="twitter:description" content="<?php echo get_post_meta($post->ID, 'tw_description', true); ?>">
		<meta name="twitter:image" content="<?php echo get_post_meta($post->ID, 'tw_image_url', true); ?>" />
<?php if( get_post_meta($post->ID, 'tw_type', true) == "summary_large_image" ){ ?>
		<meta name="twitter:creator" content="<?php echo get_post_meta($post->ID, 'tw_creator', true); ?>">
		<meta name="twitter:image:alt" content="<?php echo get_post_meta($post->ID, 'tw_image_alt', true); ?>">
<?php }

if( get_post_meta($post->ID, 'tw_type', true) == "app" ){ ?>
		<meta name="twitter:app:country" content="<?php echo get_post_meta($post->ID, 'tw_country', true); ?>">
		<meta name="twitter:app:name:iphone" content="<?php echo get_post_meta($post->ID, 'tw_name_iphone', true); ?>">
		<meta name="twitter:app:id:iphone" content="<?php echo get_post_meta($post->ID, 'tw_id_iphone', true); ?>">
		<meta name="twitter:app:url:iphone" content="<?php echo get_post_meta($post->ID, 'tw_url_iphone', true); ?>">
		<meta name="twitter:app:name:ipad" content="<?php echo get_post_meta($post->ID, 'tw_name_ipad', true); ?>">
		<meta name="twitter:app:id:ipad" content="<?php echo get_post_meta($post->ID, 'tw_id_ipad', true); ?>">
		<meta name="twitter:app:url:ipad" content="<?php echo get_post_meta($post->ID, 'tw_url_ipad', true); ?>">
		<meta name="twitter:app:name:googleplay" content="<?php echo get_post_meta($post->ID, 'tw_name_google', true); ?>">
		<meta name="twitter:app:id:googleplay" content="<?php echo get_post_meta($post->ID, 'tw_id_google', true); ?>">
		<meta name="twitter:app:url:googleplay" content="<?php echo get_post_meta($post->ID, 'tw_url_google', true); ?>">
<?php }

if( get_post_meta($post->ID, 'tw_type', true) == "player" ){ ?>
		<meta name="twitter:player" content="<?php echo get_post_meta($post->ID, 'tw_video_url', true); ?>" />
		<meta name="twitter:player:width" content="<?php echo get_post_meta($post->ID, 'tw_video_width', true); ?>" />
		<meta name="twitter:player:height" content="<?php echo get_post_meta($post->ID, 'tw_video_height', true); ?>" />
		<meta name="twitter:player:stream" content="<?php echo get_post_meta($post->ID, 'tw_video_stream', true); ?>" />
		<meta name="twitter:player:stream:content_type" content="<?php echo get_post_meta($post->ID, 'tw_video_stream_content', true); ?>" />
<?php } ?>


		<!-- TiDiO -->
		<script src="//code.tidio.co/f1o91z5fmeru5tyurnbveyamblxy8m3j.js"></script>

	</head>

	<body <?php body_class(); ?> >


	<!-- Google Tag Manager (noscript) -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9QNRDX" height="0" width="0" style="display:none;visibility:hidden"></iframe>
	</noscript>
	<!-- End Google Tag Manager (noscript) -->


		<!-- loading animation -->
		<div id="loadingContainer">
			<img src="<?php bloginfo('template_url'); ?>/images/logos/onboard_logo.png">
		</div>


		<!-- popup settings -->
		<div id="popup-settings" data-visibility="<?php echo get_post_meta($post->ID, 'visibility', true); ?>" data-time="<?php echo get_post_meta($post->ID, 'time_to_show', true); ?>"></div>


		<!-- tracking pixels -->
		<div id="pageNameDiv" data-pagename="<?php echo $slug; ?>"><?php echo $slug; ?></div>
		<div id="trackingPixel_middle"></div>
		<div id="trackingPixel_bottom"></div>


		<!-- What We Do dropdown menu -->
		<ul id="whatWeDo_dropdown">
			<li><a href="<?php echo $siteURL."/what-we-do/worldwide-market-entry"; ?>">Worldwide Market Entry</a></li>
			<li><a href="<?php echo $siteURL."/what-we-do/digital-marketing-services"; ?>">Digital Marketing Services</a></li>
			<li><a href="<?php echo $siteURL."/what-we-do/database-service"; ?>">Database Services</a></li>
			<li><a href="<?php echo $siteURL."/what-we-do/b2b-marketing-and-sales"; ?>">B2B Marketing & Sales</a></li>
			<li><a href="<?php echo $siteURL."/what-we-do/business-process-enablement-platform"; ?>">Business Process Platform</a></li>
		</ul>


		<!-- TECHNOLOGY dropdown menu -->
		<ul id="technology_dropdown">
			<li><a href="<?php echo $siteURL."/technology/salesforce"; ?>">SalesForce</a></li>
			<li><a href="<?php echo $siteURL."/technology/gsuite"; ?>">G Suite</a></li>
			<li><a href="<?php echo $siteURL."/technology/eloqua"; ?>">Eloqua</a></li>
			<li><a href="<?php echo $siteURL."/technology/salesmanago"; ?>">Sales Manago</a></li>
			<li><a href="<?php echo $siteURL."/technology/marketingxpress"; ?>">MarketingXpress</a></li>
			<li><a href="<?php echo $siteURL."/technology/credentials"; ?>">Credentials</a></li>
		</ul>


		<!-- TOP MENU -->
		<header id="header">

			<div id="header_top_black_line">
				<div id="header_top_black_line-in">
					<div id="inner-left">
						<a href="<?php echo $siteURL."/technology/credentials/"; ?>">ISO 9001:2008 / 27001:2005</a>
					</div>

					<div class="inner">
						<a class="callUsAndChat inner-call-chat-icons" href="#" id="message-topmenu-btn">
							<i class="fa fa-envelope-o" aria-hidden="true"></i>
							<p style="display: inline-block;">Write us</p>
						</a>
						<a class="callUsAndChat inner-call-chat-icons" href="#" id="live-chat-topmenu-btn">
							<div data-icon="b" class="icon" style="display: inline-block;"></div>
							<p style="display:inline-block;color:blue;">Live chat</p>
						</a>
						<a class="callUsAndChat inner-call-chat-icons" href="<?php echo $siteURL."/contacts"; ?>">
							<div data-icon="g" class="contacts_icons icon" style="display:inline-block;"></div>
							<p style="display:inline-block;">Call us</p>
						</a>
						<a class="topLineLinks" target="_blank" href="https://www.facebook.com/OnboardCRM">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
						<a class="topLineLinks" target="_blank" href="https://www.linkedin.com/company/onboardcrm?trk=company_logo">
							<i class="fa fa-linkedin-square" aria-hidden="true"></i>
						</a>
						<a class="topLineLinks" target="_blank" href="https://twitter.com/Onboardcrm">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
						<a class="topLineLinks" target="_blank" href="https://www.instagram.com/onboardcrm/">
							<i class="fa fa-instagram" aria-hidden="true"></i>
						</a>
					</div>
				</div>
			</div>

			<div class="content-wrapper">
				<div class="content">
					<a class="logo" href="<?php echo $siteURL."/home"; ?>">Onboard</a>
					<nav id="primary_nav">
						<ul>
							<li><a href="<?php echo $siteURL."/home"; ?>" data-pagename="home">HOME</a></li>
							<li><a href="<?php echo $siteURL."/what-we-do"; ?>" data-pagename="whatwedo" id="whatWeDoElement">WHAT WE DO</a></li>
							<li><a href="<?php echo $siteURL."/technology"; ?>" data-pagename="technology" id="technologyElement">TECHNOLOGY</a></li>
							<li><a href="<?php echo $siteURL."/case-studies"; ?>" data-pagename="case-studies" id="caseStudiesElement">CASES</a></li>
							<li><a href="<?php echo $siteURL."/blog"; ?>" data-pagename="blog">BLOG</a></li>
							<li><a href="<?php echo $siteURL."/careers"; ?>" data-pagename="careers">CAREERS</a></li>
							<li><a href="<?php echo $siteURL."/about"; ?>" data-pagename="about">ABOUT</a></li>
							<li><a href="<?php echo $siteURL."/contacts"; ?>" data-pagename="contacts">CONTACTS</a></li>
						</ul>
					</nav>
				</div>
			</div>

		</header>


		<!-- the arrow up -->
		<div id="arrowUP"><i class="fa fa-angle-up" aria-hidden="true"></i></div>