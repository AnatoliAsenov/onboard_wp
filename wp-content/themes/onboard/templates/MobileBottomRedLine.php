<?php


class MobileBottomRedLine
{
    private $content = "";

    public function __construct($content){
        $this->content = $content;
    }

    public function printHTML(){
        ?>

        <div id="bottomRedLine">
            <p id="bottomJobLine_txt"><?php echo $this->content; ?></p>
            <a id="bottomJobLine_btn">CONTACT US</a>
        </div>

        <?php
    }
}