<?php
include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(168);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(168);
$header->printHTML();

$siteURL = get_site_url();

$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
$block4 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{middle-line-text}', '{/middle-line-text}');
$middleLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/wwdo_768.jpg" id="topBanner">

    <div id="wwd-container">

        <div id="wwd_title">SALESFORCE</div>

        <ul id="technology-intro-ul">
            <li id="technology-sf1-img1">
                <img src="<?php bloginfo('template_url'); ?>/images/mobile/technology/sf_1.png" />
            </li>
            <li>
                <p><?php echo $block1; ?></p>
                <p><?php echo $block2; ?></p>
            </li>
            <li id="technology-sf1-img2">
                <img src="<?php bloginfo('template_url'); ?>/images/mobile/technology/sf_1.png" />
            </li>
        </ul>

        <ul id="technology-middle-line">
            <li><i class="demo-icon icon-quote-right-alt">&#xe801;</i></li>
            <li><p><?php echo $middleLineText; ?></p></li>
        </ul>

        <img src="<?php bloginfo('template_url'); ?>/images/mobile/technology/sf_2.png" id="technology-sf2-img" />

        <ul id="technology-second-text-block">
            <li>
                <p><?php echo $block3; ?></p>
                <p><?php echo $block4; ?></p>
            </li>
            <li>
                <div id="technology-talk">
                    <div data-icon="a" class="icon" id="technology-talk_to_expert"></div>
                    <div id="technology-talk-inner">
                        <p id="technology-talk-red-row">Talk to a CRM expert</p>
                        <p id="technology-talk-grey-row">Drop us a line...</p>
                    </div>
                </div>
                <a style="display:block;text-decoration:none;" id="technology-free-trial-btn" href="https://www.salesforce.com/form/signup/freetrial-sales.jsp" >FREE TRIAL</a>
            </li>
        </ul>

        <h2 id="technology-inner-title">WHAT WE DO</h2>

        <ul id="technology-wwd">
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/rocket-ship.png">
                <div id="technology-wwd-text">CRM CONSULTANCY & SALESFORCE IMPLEMENTATION</div>
            </li>
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/tablet.png">
                <div id="technology-wwd-text">SALES CLOUD USER TRAINING</div>
            </li>
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/browser.png">
                <div id="technology-wwd-text">SALESFORCE ADMIN SUPPORT</div>
            </li>
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/Layer-32.png">
                <div id="technology-wwd-text">DATABASE SERVICES</div>
            </li>
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/marketing.png">
                <div id="technology-wwd-text">MARKETING OPERATIONS</div>
            </li>
            <li>
                <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/analytics.png">
                <div id="technology-wwd-text">BUSINESS INTELLIGENCE</div>
            </li>
        </ul>

        <div id="sf-banners-container">
            <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/sf-admin.png">
            <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/sf-consultant.png">
        </div>

    </div>

    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;


$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();