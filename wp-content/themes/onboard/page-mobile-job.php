<?php
include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$siteURL = get_site_url();

$postID = get_query_var( 'id' );
$singlePost = get_post( $postID );

$header = new MobileHeader($postID);
$header->printHTML();

$postContent = $singlePost->post_content;
$postLocation = get_post_meta($postID, 'location', true);
$postLanguages = get_post_meta($postID, 'languages', true);

$stringManipulator = new StringManipulation();

//var_dump($postContent);
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">careers</div>


    <!-- top banner -->
    <style>
        @media screen and (max-width: 320px) {
            #singleJobBanner_container{
                width: 320px;
                margin: 80px auto 0 auto;
            }
            #singleJobBanner{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/singleJob_tit_320.png");
                height: auto;
                width: 225px;
                margin-right: 95px;
            }
            #singleJobTitleContainer{
                width: 290px;
                margin: -66px auto 0 auto;
            }
            #singleJobTitle{
                width: 200px;
                margin-left: 90px;
                font-size: 18px;
                color: grey;
                text-align: right;
            }
            /*-- language, location and apply btn --*/
            #singleJobUl{
                width: 290px;
                margin: 40px auto;
                padding: 0;
            }
            #singleJobUl p{
                display: inline-block;
                font-size: 13px;
                color: #EB5C4E;
            }
            #singleJobUl i{
                display: inline-block;
                color: #EB5C4E;
                font-size: 24px;
            }
            #singleJobUl1{
                display: inline-block;
            }
            #singleJobUl2{
                display: inline-block;
                float: right;
            }
            #singleJobUl3{
                display: block;
            }
            .single-job-applynow{
                display: block;
                text-decoration: none;
                width: 140px;
                margin: 20px auto 0 auto;
                border: 2px solid #EB5C4E;
                background-color: #EB5C4E;
                color: #ffffff;
                font-size: 17px;
                font-weight: 500;
                text-align: center;
                padding: 6px 0;
            }
            #single-job-applynow-2{
                margin-top: 50px;
            }
            /*-- main content --*/
            #singleJobContent{
                width: 290px;
                text-align: justify;
                font-size: 18px;
                color: #a3a3a3;
                margin: 20px auto 0 auto;
                font-weight: 300;
            }
            #singleJobContent h1,h2,h3{
                font-size: 24px;
                font-weight: 700;
                color: #EB5C4E;
            }
            #singleJobContent ul{
                padding-left: 20px;
            }
            /*-- share --*/
            #share_job_outer{
                width: 290px;
                margin: 40px auto 50px auto;
            }
            #share_job_inner{
                width: 132px;
                margin: 10px auto 0 auto;
            }
            .single-job-share-p{
                color: #a3a3a3;
                font-size: 18px;
                text-align: center;
            }
            .single-post-share-div{
                background-color: #808080;
                border-radius: 50%;
                width: 28px;
                height: 28px;
                text-align: center;
                vertical-align: middle;
                display: inline-block;
            }
            .single-post-share-div:hover{
                cursor: pointer;
                background-color: #EB5C4E;
            }
            .single-post-share-div i{
                color: #ffffff;
                font-size: 18px;
                vertical-align: middle;
                vertical-align: -webkit-baseline-middle;
            }
        }
        @media screen and (max-width: 478px) and (min-width: 320px) {
            #singleJobBanner_container{
                width: 320px;
                margin: 80px auto 0 auto;
            }
            #singleJobBanner{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/singleJob_tit_320.png");
                height: auto;
                width: 225px;
                margin-right: 95px;
            }
            #singleJobTitleContainer{
                width: 290px;
                margin: -66px auto 0 auto;
            }
            #singleJobTitle{
                width: 200px;
                margin-left: 90px;
                font-size: 18px;
                color: grey;
                text-align: right;
            }
            /*-- language, location and apply btn --*/
            #singleJobUl{
                width: 290px;
                margin: 40px auto;
                padding: 0;
            }
            #singleJobUl p{
                display: inline-block;
                font-size: 13px;
                color: #EB5C4E;
            }
            #singleJobUl i{
                display: inline-block;
                color: #EB5C4E;
                font-size: 24px;
            }
            #singleJobUl1{
                display: inline-block;
            }
            #singleJobUl2{
                display: inline-block;
                float: right;
            }
            #singleJobUl3{
                display: block;
            }
            .single-job-applynow{
                display: block;
                text-decoration: none;
                width: 140px;
                margin: 20px auto 0 auto;
                border: 2px solid #EB5C4E;
                background-color: #EB5C4E;
                color: #ffffff;
                font-size: 17px;
                font-weight: 500;
                text-align: center;
                padding: 6px 0;
            }
            #single-job-applynow-2{
                margin-top: 50px;
            }
            /*-- main content --*/
            #singleJobContent{
                width: 290px;
                text-align: justify;
                font-size: 18px;
                color: #a3a3a3;
                margin: 20px auto 0 auto;
                font-weight: 300;
            }
            #singleJobContent h1,h2,h3{
                font-size: 24px;
                font-weight: 700;
                color: #EB5C4E;
            }
            #singleJobContent ul{
                padding-left: 20px;
            }
            /*-- share --*/
            #share_job_outer{
                width: 290px;
                margin: 40px auto 50px auto;
            }
            #share_job_inner{
                width: 132px;
                margin: 10px auto 0 auto;
            }
            .single-job-share-p{
                color: #a3a3a3;
                font-size: 18px;
                text-align: center;
            }
            .single-post-share-div{
                background-color: #808080;
                border-radius: 50%;
                width: 28px;
                height: 28px;
                text-align: center;
                vertical-align: middle;
                display: inline-block;
            }
            .single-post-share-div:hover{
                cursor: pointer;
                background-color: #EB5C4E;
            }
            .single-post-share-div i{
                color: #ffffff;
                font-size: 18px;
                vertical-align: middle;
                vertical-align: -webkit-baseline-middle;
            }
        }
        @media screen and (max-width: 766px) and (min-width: 478px) {
            #singleJobBanner_container{
                width: 450px;
                margin: 80px auto 0 auto;
            }
            #singleJobBanner{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/singleJob_tit_480.png");
                height: auto;
                width: 286px;
                margin-right: 164px;
            }
            #singleJobTitleContainer{
                width: 450px;
                margin: -88px auto 0 auto;
            }
            #singleJobTitle{
                width: 250px;
                margin-left: 200px;
                font-size: 24px;
                color: grey;
                text-align: right;
            }
            /*-- language, location and apply btn --*/
            #singleJobUl{
                width: 450px;
                margin: 40px auto;
                padding: 0;
            }
            #singleJobUl p{
                display: inline-block;
                font-size: 18px;
                color: #EB5C4E;
            }
            #singleJobUl i{
                display: inline-block;
                color: #EB5C4E;
                font-size: 30px;
                margin-right: 5px;
            }
            #singleJobUl1{
                display: inline-block;
            }
            #singleJobUl2{
                display: inline-block;
                float: right;
            }
            #singleJobUl3{
                display: block;
            }
            .single-job-applynow{
                display: block;
                text-decoration: none;
                width: 140px;
                margin: 30px auto 0 auto;
                border: 2px solid #EB5C4E;
                background-color: #EB5C4E;
                color: #ffffff;
                font-size: 18px;
                font-weight: 500;
                text-align: center;
                padding: 6px 0;
            }
            #single-job-applynow-2{
                margin-top: 60px;
            }
            /*-- main content --*/
            #singleJobContent{
                width: 450px;
                text-align: justify;
                font-size: 18px;
                color: #a3a3a3;
                margin: 20px auto 0 auto;
                font-weight: 300;
            }
            #singleJobContent h1,h2,h3{
                font-size: 24px;
                font-weight: 700;
                color: #EB5C4E;
            }
            #singleJobContent ul{
                padding-left: 20px;
            }
            /*-- share --*/
            #share_job_outer{
                width: 290px;
                margin: 40px auto 50px auto;
            }
            #share_job_inner{
                width: 132px;
                margin: 10px auto 0 auto;
            }
            .single-job-share-p{
                color: #a3a3a3;
                font-size: 18px;
                text-align: center;
            }
            .single-post-share-div{
                background-color: #808080;
                border-radius: 50%;
                width: 28px;
                height: 28px;
                text-align: center;
                vertical-align: middle;
                display: inline-block;
            }
            .single-post-share-div:hover{
                cursor: pointer;
                background-color: #EB5C4E;
            }
            .single-post-share-div i{
                color: #ffffff;
                font-size: 18px;
                vertical-align: middle;
                vertical-align: -webkit-baseline-middle;
            }
        }
        @media screen and (max-width: 1022px) and (min-width: 766px){
            #singleJobBanner_container{
                width: 750px;
                margin: 155px auto 0 auto;
            }
            #singleJobBanner{
                display: block;
                content: url("<?php bloginfo('template_url'); ?>/images/mobile/careers/singleJob_tit_768.png");
                height: auto;
                width: 312px;
                margin-right: 438px;
            }
            #singleJobTitleContainer{
                width: 750px;
                margin: -74px auto 0 auto;
            }
            #singleJobTitle{
                width: 500px;
                margin-left: 250px;
                font-size: 30px;
                color: grey;
                text-align: right;
            }
            /*-- language, location and apply btn --*/
            #singleJobUl{
                width: 750px;
                margin: 40px auto;
                padding: 0;
            }
            #singleJobUl p{
                display: inline-block;
                font-size: 18px;
                color: #EB5C4E;
            }
            #singleJobUl i{
                display: inline-block;
                color: #EB5C4E;
                font-size: 45px;
                margin-right: 8px;
                vertical-align: middle;
            }
            #singleJobUl1{
                display: inline-block;
            }
            #singleJobUl2{
                display: inline-block;
                margin-left: 108px;
            }
            #singleJobUl3{
                display: inline-block;
                margin-left: 108px;
            }
            .single-job-applynow{
                display: block;
                text-decoration: none;
                width: 140px;
                border: 2px solid #EB5C4E;
                background-color: #EB5C4E;
                color: #ffffff;
                font-size: 18px;
                font-weight: 500;
                text-align: center;
                padding: 6px 0;
            }
            #single-job-applynow-2{
                margin: 50px auto 0 auto;
            }
            /*-- main content --*/
            #singleJobContent{
                width: 750px;
                text-align: justify;
                font-size: 18px;
                color: #a3a3a3;
                margin: 50px auto 0 auto;
                font-weight: 300;
            }
            #singleJobContent h1,h2,h3{
                font-size: 24px;
                font-weight: 700;
                color: #EB5C4E;
            }
            #singleJobContent ul{
                padding-left: 20px;
            }
            /*-- share --*/
            #share_job_outer{
                width: 290px;
                margin: 40px auto 50px auto;
            }
            #share_job_inner{
                width: 132px;
                display: inline-block;
            }
            .single-job-share-p{
                color: #a3a3a3;
                font-size: 18px;
                text-align: center;
                display: inline-block;
                vertical-align: middle;
                margin-right: 15px;
            }
            .single-post-share-div{
                background-color: #808080;
                border-radius: 50%;
                width: 28px;
                height: 28px;
                text-align: center;
                vertical-align: middle;
                display: inline-block;
            }
            .single-post-share-div:hover{
                cursor: pointer;
                background-color: #EB5C4E;
            }
            .single-post-share-div i{
                color: #ffffff;
                font-size: 18px;
                vertical-align: middle;
                vertical-align: -webkit-baseline-middle;
            }
        }
    </style>
    <div id="singleJobBanner_container">
        <img id="singleJobBanner" src="" alt="careers" />
    </div>

    <div id="singleJobTitleContainer">
        <div id="singleJobTitle"><?php echo $singlePost->post_title; ?></div>
    </div>

    <ul id="singleJobUl">
        <li id="singleJobUl1">
            <i class="fa fa-language" aria-hidden="true"></i>
            <p><?php echo $postLanguages; ?></p>
        </li>
        <li id="singleJobUl2">
            <i class="fa fa-globe" aria-hidden="true"></i>
            <p><?php echo $postLocation; ?></p>
        </li>
        <li id="singleJobUl3">
            <a class="single-job-applynow" href="<?php echo add_query_arg(array("id" => $postID), $siteURL."/mobile/mobile-apply" ); ?>">APPLY NOW</a>
        </li>
    </ul>

    <div id="singleJobContent">
        <?php echo $postContent; ?>
    </div>

    <a id="single-job-applynow-2" class="single-job-applynow" href="<?php echo add_query_arg(array("id" => $postID), $siteURL."/mobile/mobile-apply" ); ?>">APPLY NOW</a>

    <div id="share_job_outer">
        <p class="single-job-share-p">SHARE THIS JOB</p>
        <div id="share_job_inner">
            <a class="single-post-share-div" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>id=<?php echo $postID; ?>" target="_blank">
                <i class="fa fa-facebook" aria-hidden="true"></i>
            </a>
            <a style="margin: 0 20px;" class="single-post-share-div" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>id=<?php echo $postID; ?>&title=<?php the_title(); ?>&source=http://www.onboardcrm.com" target="_blank">
                <i class="fa fa-linkedin-square" aria-hidden="true"></i>
            </a>
            <a class="single-post-share-div" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>id=<?php echo $postID; ?>" target="_blank">
                <i class="fa fa-twitter" aria-hidden="true"></i>
            </a>
        </div>
    </div>

<!-- footer -->
<?php

$footer = new MobileFooter();
$footer->printHTML();