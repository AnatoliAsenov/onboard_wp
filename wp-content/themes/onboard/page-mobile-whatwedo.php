<?php
include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(178);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(178);
$header->printHTML();

$siteURL = get_site_url();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/wwdo_768.jpg" id="topBanner">


    <style>
    @media screen and (max-width: 320px) {
        #whatWeDoMainContentUl {
            background-image: url("<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/bg_320.png");
        }
    }
    @media screen and (max-width: 478px) and (min-width: 320px) {
        #whatWeDoMainContentUl {
            background-image: url("<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/bg_320.png");
        }
    }
    @media screen and (max-width: 766px) and (min-width: 478px) {
        #whatWeDoMainContentUl {
            background-image: url("<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/bg_480.png");
        }
    }
    @media screen and (max-width: 1022px) and (min-width: 766px){
        #whatWeDoMainContentUl {
            background-image: url("<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/bg_768.png");
        }
    }
</style>
    <ul id="whatWeDoMainContentUl" style="">

<?php

// print all paragraphs
$numberOfSubTitles = preg_match_all('/\bparagraph-delimiter\b/', $postContent);

for($i = 0; $i < $numberOfSubTitles/2; $i++) {
    // get paragraph
    $stringManipulator->stringExtractAndDelete($postContent, '{paragraph-delimiter}', '{/paragraph-delimiter}');
    $tempParagraph = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    // get paragraph title
    $stringManipulator->stringExtractAndDelete($tempParagraph, '{paragraph-title}', '{/paragraph-title}');
    $tempParagraphTitle = $stringManipulator->neededSubString;
    $tempParagraph = $stringManipulator->reducedString;

    // get paragraph text
    $stringManipulator->stringExtractAndDelete($tempParagraph, '{paragraph-text}', '{/paragraph-text}');
    $tempParagraphText = $stringManipulator->neededSubString;
    $tempParagraph = $stringManipulator->reducedString;

    // get paragraph link
    $stringManipulator->stringExtractAndDelete($tempParagraph, '{paragraph-link}', '{/paragraph-link}');
    $tempParagraphLink = $stringManipulator->neededSubString;
    $tempParagraph = $stringManipulator->reducedString;

    // get paragraph number
    $theNum = $i + 1;

    // array with links
    $allLinks = array(
        "mobile-market-entry",
        "mobile-marketing-services",
        "mobile-database-services",
        "mobile-b2b",
        "mobile-business-process-enablement"
    );
?>
        <li style="background-image: url(<?php bloginfo('template_url'); ?>/images/whatwedo/box_back.png);">
            <p class="wwd-title"><?php echo $tempParagraphTitle; ?></p>
            <p class="wwd-sub-title"><?php echo $tempParagraphText; ?></p>
            <p class="wwd-num"><?php echo "0{$theNum}"; ?></p>
            <a href="<?php echo $siteURL."/mobile/mobile-whatwedo/".$allLinks[$i]; ?>"><div class="icon icon-slim-right"></div></a>
        </li>
<?php } ?>

    </ul>

    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();