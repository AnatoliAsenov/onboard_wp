
$(document).ready(function() {

    var animationBlock2Active = false;
    
    if ($("#careers-block1-picwithicons").length > 0) {

        //=============================================
        //========== block2 table animation ===========
        //=============================================
        $(window).scroll(function() {
            var middlePixelHeight = $("#trackingPixel_middle").offset().top;
            var tableTopOffset = $("#careers-block1-picwithicons").offset().top;
            if((middlePixelHeight - 100) > tableTopOffset && animationBlock2Active == false){
                activateAnimation();
                animationBlock2Active = true;
            }
        });

         function activateAnimation() {
            $("#careers-block1-picwithicons-title").animate({
                height: "220px"
            }, 1200, function () {
                $(".careers-block1-picwithicons-img").animate({
                    width: "57px",
                    height: "57px"
                }, 150, function () {
                    $(".careers-block1-picwithicons-img").animate({
                        width: "50px",
                        height: "50px"
                    }, 250);
                });
            });
        }


        //=============================================
        //========== Employee Quote slider ============
        //=============================================
        (function(){

            if( $("#careers-block2-title").length > 0) {
                // get all content
                var mainContent = $("#questions_and_answers").text();
                var singleQuestion = "";

                function getSubstring(content, begin, end) {
                    var beginStartIndex = content.indexOf(begin);
                    var beginLength = begin.length;
                    var endStartIndex = content.indexOf(end);
                    var endLength = end.length;
                    return content.slice((beginStartIndex + beginLength), endStartIndex);
                }

                function getPropertiesOfQuestion(content, begin, end, isWholeQuestion) {
                    var beginStartIndex = content.indexOf(begin);
                    var beginLength = begin.length;
                    var endStartIndex = content.indexOf(end);
                    var endLength = end.length;
                    var forReturn = content.slice((beginStartIndex + beginLength), endStartIndex);
                    if (isWholeQuestion) {
                        mainContent = content.slice(endStartIndex + endLength);
                    } else {
                        singleQuestion = content.slice(endStartIndex + endLength);
                    }
                    return forReturn;
                }

                function numberOfOccurrences(content, substring) {
                    var regExp = new RegExp(substring, "gi");
                    return (content.match(regExp) || []).length;
                }

                // number of questions
                const numberOfQuestions = numberOfOccurrences(mainContent, "{question}");

                // generate questions array
                var questions = [];
                var answer = [];

                for (var x = 0; x < numberOfQuestions; x++) {
                    var tempQuestion = {};
                    // get whole question
                    var tempContent = getPropertiesOfQuestion(mainContent, "{question}", "{/question}", true);
                    // get visibility property
                    tempQuestion.visibility = getPropertiesOfQuestion(tempContent, "{visibility}", "{/visibility}", false);
                    // get title of question
                    tempQuestion.questionTitle = getPropertiesOfQuestion(tempContent, "{title}", "{/title}", false);
                    // get the question content
                    tempQuestion.questionContent = getPropertiesOfQuestion(tempContent, "{content}", "{/content}", false);

                    // get all employees opinions
                    function getPropertiesOfEmployee(content, begin, end) {
                        var beginStartIndex = content.indexOf(begin);
                        var beginLength = begin.length;
                        var endStartIndex = content.indexOf(end);
                        var endLength = end.length;
                        var forReturn = content.slice((beginStartIndex + beginLength), endStartIndex);
                        tempContent = content.slice(endStartIndex + endLength);
                        return forReturn;
                    }

                    tempQuestion.numberOfAnswers = numberOfOccurrences(tempContent, "{employee}");
                    for (var v = 0; v < tempQuestion.numberOfAnswers; v++) {
                        // get single answer
                        var employeeContent = getPropertiesOfEmployee(tempContent, "{employee}", "{/employee}", false);

                        var tempEmployee = {};
                        tempEmployee.name = getSubstring(employeeContent, "{name}", "{/name}", false);
                        tempEmployee.quote = getSubstring(employeeContent, "{quote}", "{/quote}", false);
                        tempEmployee.gender = getSubstring(employeeContent, "{gender}", "{/gender}", false);
                        answer.push(tempEmployee);
                    }
                    tempQuestion.answers = answer;
                    answer = [];
                    if (tempQuestion.visibility == "visible") {
                        questions.push(tempQuestion);
                    }

                }
            }

            //===================================
            //======== render the texts =========
            //===================================
            var questionCount = 0;
            var iterateOverAnswers = false;
            var answerCount = 0;
            var tempQuestionCount;
            renderQuestion();
            setInterval(renderQuestion, 3500);

            function renderQuestion(){

                //render question
                if(!iterateOverAnswers){
                    // print
                    loadQuestion(questionCount);

                    // count number of printed questions
                    tempQuestionCount = questionCount;
                    questionCount++;
                    iterateOverAnswers = true;
                    if(questions != 'undefined' && questions != null && questions != false) {
                        if (questionCount > questions.length - 1) {
                            questionCount = 0;
                            //tempQuestionCount = 0;
                            iterateOverAnswers = true;
                        }
                    }
                }

                if(iterateOverAnswers){
                    // print
                    loadQuote(questions[tempQuestionCount].answers[answerCount]);

                    // count number of printed answers
                    answerCount++;
                    if (answerCount >= questions[tempQuestionCount].numberOfAnswers) {
                        answerCount = 0;
                        iterateOverAnswers = false;
                    }

                }

            }

            function loadQuestion(n){
                var textContainer = $("#block2-middleBoxes_leftText");
                var authorName = $("#block2-middleBoxes_leftText div");
                var authorText = $("#block2-middleBoxes_leftText p");
                textContainer.animate({opacity: 0}, 300, function(){
                    authorName.text(questions[n].questionTitle);
                    authorText.text(questions[n].questionContent.slice(0, 90));
                    textContainer.animate({opacity: 1}, 300);
                });
            }

            function loadQuote(a){
                // change icon male/female
                var male = $("#male-hipster");
                var female = $("#female-hipster");

                if(a.gender == "f"){
                    male.animate({
                        opacity: 0
                    }, 300, function(){
                        male.css("display", "none");
                        female.css("display", "block").animate({opacity: 1}, 300);
                    });
                }else{
                    female.animate({
                        opacity: 0
                    }, 300, function(){
                        female.css("display", "none");
                        male.css("display", "block").animate({opacity: 1}, 300);
                    });
                }

                // replace the texts
                var textContainer = $("#block2-middleBoxes_rightText");
                var authorName = $("#block2-middleBoxes_rightText div");
                var authorText = $("#block2-middleBoxes_rightText p");
                textContainer.animate({opacity: 0}, 300, function(){
                    authorName.text(a.name);
                    authorText.text(a.quote.slice(0, 90));
                    textContainer.animate({opacity: 1}, 300);
                });
            }

        })();
    }

    //===========================================================
    //=============== "see where you fit" button ================
    //===========================================================
    if ($("#careers-block1-button").length > 0) {
        var compensationHeight = 256;

        $("#careers-block1-button").click(function(){
            if(animationBlock2Active){
                compensationHeight = 0;
            }
            var titleTopOffset = $("#careers-block3-title").offset().top;
            $("html, body").animate({
                scrollTop: (titleTopOffset - compensationHeight)
            }, 1500);
        });
    }


    //===========================================================
    //========= style all 'H' elements in post content ==========
    //===========================================================
    if($("#single-job-main-content").length > 0){
        var children = $("#single-job-main-content").children();
        children.each(function(){
            if($(this).is("h1") || $(this).is("h2") || $(this).is("h3") || $(this).is("h4") || $(this).is("h5") || $(this).is("h6")){
                $(this).css({
                    'color': '#EB5C4E',
                    'font-size': '20px',
                    'margin': '18px 0'
                });
            }
        });
    }


});