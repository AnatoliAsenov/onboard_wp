<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include 'templates/BottomRedLine.php';
include 'string_manipulation/StringManipulation.php';

get_header();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">facts</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/technology/technology.jpg" id="topBanner">

<?php
$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{principles}', '{/principles}');
$principles = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>
<!-- title -->
<div id="technology-title"><?php echo $title; ?></div>

<div id="facts-main-container-why_onboard">
    <div id="facts-L">
        <p><?php echo $block1; ?></p>
        <p><?php echo $block2; ?></p>
        <ul>
            <li><img src="<?php bloginfo('template_url'); ?>/images/facts/partnership/icon1.png" ></li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/facts/partnership/icon2.png" ></li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/facts/partnership/icon3.png" ></li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/facts/partnership/icon4.png" ></li>
            <li><img src="<?php bloginfo('template_url'); ?>/images/facts/partnership/icon5.png" ></li>
        </ul>
    </div>
    <ul id="facts-R">
<?php
$numberOfPrinciples = preg_match_all('/\bsingle-principle\b/', $principles);

for($c = 0; $numberOfPrinciples/2 > $c; $c++){
    $stringManipulator->stringExtractAndDelete($principles, '{single-principle}', '{/single-principle}');
    $tempPrinciple = $stringManipulator->neededSubString;
    $principles = $stringManipulator->reducedString;
    echo '<li><div class="list-triangle facts-R-list-triangle-inline"></div><p>'.$tempPrinciple.'</p></li>';
}
?>
    </ul>
</div>


<?php

// bottom red line content
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();