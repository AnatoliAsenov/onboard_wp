<?php
/**
 * Template Name: Basic Mobile Template
 */

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$header = new MobileHeader($post->ID);
$header->printHTML();

?>


    <style>
        @media screen and (max-width: 320px){
            #basicMobileContainer {
                width: 290px;
                margin: 70px auto 40px;
            }
            #basicMobileContainer img{
                max-width: 290px !important;
            }
        }
        @media screen and (max-width: 478px) and (min-width: 320px){
            #basicMobileContainer {
                width: 290px;
                margin: 70px auto 40px;
            }
            #basicMobileContainer img{
                max-width: 290px !important;
            }
        }
        @media screen and (max-width: 766px) and (min-width: 478px){
            #basicMobileContainer {
                width: 450px;
                margin: 70px auto 40px;
            }
            #basicMobileContainer img{
                max-width: 450px !important;
            }
        }
        @media screen and (max-width: 1022px) and (min-width: 766px){
            #basicMobileContainer {
                width: 725px;
                margin: 80px auto 40px;
            }
            #basicMobileContainer img{
                max-width: 725px !important;
            }
        }
    </style>

    <div id="basicMobileContainer">

<?php

echo $post->post_content;

echo "</div>";

// bottom red line content
$stringManipulator = new StringManipulation();
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();