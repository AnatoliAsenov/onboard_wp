<?php

include "templates/PopUps.php";

$siteURL = get_site_url();

?>

	<!-- sub footer -->
	<div id="subFooter_container">
		<table id="subFooter">
			<tr class="subFooter_horizontalBuffer" colspan="5"></tr>
			<tr>
				<th><h2>WHAT WE DO</h2></th>
				<th><h2>INSIGHTS</h2></th>
				<th><h2>ABOUT</h2></th>
				<th><h2>FACTS</h2></th>
				<th><h4>Copyright &copy; 2017 Onboardcrm. All Rights reserved.</h4></th>
			</tr>
			<tr class="subFooter_horizontalBuffer2" colspan="5"></tr>
			<tr>
				<th>
					<ul>
						<li><a href="<?php echo $siteURL.'/what-we-do/worldwide-market-entry'; ?>">World Market Entry</a></li>
						<li><a href="<?php echo $siteURL.'/what-we-do/digital-marketing-services'; ?>">Digital Marketing Services</a></li>
						<li><a href="<?php echo $siteURL.'/what-we-do/database-services'; ?>">Database Services</a></li>
						<li><a href="<?php echo $siteURL.'/what-we-do/b2b-marketing-and-sales'; ?>">B2B Marketing & Sales</a></li>
						<li><a href="<?php echo $siteURL.'/what-we-do/business-process-enablement'; ?>">Business Process Enablement</a></li>
					</ul>
				</th>
				<th>
					<ul>
						<li><a href="<?php echo $siteURL.'/technology'; ?>">Technology</a></li>
						<li><a href="<?php echo $siteURL.'/case-studies'; ?>">Cases</a></li>
						<li><a href="<?php echo $siteURL.'/blog'; ?>">Blog</a></li>
					</ul>
				</th>
				<th>
					<ul>
						<li><a href="<?php echo $siteURL.'/company'; ?>">Company</a></li>
						<li><a href="<?php echo $siteURL.'/careers'; ?>">Careers</a></li>
					</ul>
				</th>
				<th>
					<ul>
						<li><a href="<?php echo $siteURL.'/facts-and-figures/why-onboard'; ?>">Why Onboard</a></li>
						<li><a href="<?php echo $siteURL.'/facts-and-figures/partnership-advantages'; ?>">Partnership Advantages</a></li>
						<li><a href="<?php echo $siteURL.'/facts-and-figures/community'; ?>">Community</a></li>
						<li><a href="<?php echo $siteURL.'/facts-and-figures/data-privacy'; ?>">Data Privacy</a></li>
					</ul>
				</th>
				<th>
					<div style="text-align:right">
						<a class="bottomLineLinks" target="_blank" href="https://www.facebook.com/OnboardCRM">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
						<a class="bottomLineLinks" target="_blank" href="https://www.linkedin.com/company/onboardcrm?trk=company_logo">
							<i class="fa fa-linkedin-square" aria-hidden="true"></i>
						</a>
						<a class="bottomLineLinks" target="_blank" href="https://twitter.com/Onboardcrm">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
						<a class="bottomLineLinks" target="_blank" href="https://www.instagram.com/onboardcrm/" style="margin-right:0;">
							<i class="fa fa-instagram" aria-hidden="true"></i>
						</a>
					</div>
				</th>
			</tr>
			<tr class="subFooter_horizontalBuffer3" colspan="5"></tr>
		</table>

		<div id="bottom_angle"></div>
	</div>

<?php

$popUp = new PopUps();
$popUp->printHTML();

?>
<!-- scripts -->
<script src="<?php bloginfo('template_url'); ?>/js/build/script-lib.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/build/script-user.min.js"></script>

	</body>
</html>
