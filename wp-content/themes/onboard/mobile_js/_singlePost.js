$(document).ready(function() {

    //=============================================
    //====== set the current post category ========
    //=============================================
    if($("#postContainer").length){
        var allImages = $("#postContainer").find("img");
        allImages.each(function(){
            var imgH = $(this).attr("height");
            var imgW = $(this).attr("width");
            var imgWidthRatio = parseInt(imgW)/mainContainerWidth;
            //------- set new dimensions --------
            $(this).attr("width", mainContainerWidth);
            $(this).attr("height", parseInt(imgH)/imgWidthRatio);
        });
    }
});