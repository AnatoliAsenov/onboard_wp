<?php

/**
 * Created by Onboard
 * User: Anatoli Asenov
 */
class BottomRedLine
{
    private $content = "";

    public function __construct($content){
        $this->content = $content;
    }

    public function printHTML(){
?>

        <div id="bottomRedLine">
            <div id="bottomRedLine_holder">
                <p id="bottomJobLine_txt"><?php echo $this->content; ?></p>
                <a id="bottomJobLine_btn">CONTACT US</a>
            </div>
        </div>

<?php
    }
}