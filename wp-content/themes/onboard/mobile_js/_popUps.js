$(document).ready(function() {

    //===============================================
    //========== show "how can help you?" ===========
    //===============================================
    if($("#popup-settings").length) {

        var settingsDiv = $("#popup-settings");
        var popUpSettings = {};
        popUpSettings.timeToShow = settingsDiv.data("time");
        popUpSettings.visibility = settingsDiv.data("visibility");

        function showContactUs() {
            $("#popup-how-can").css("display", "block");
            $("#popUps").css("display", "block").animate({
                opacity: 0.95
            }, 500);
        }

        function hideContactUs(){
            $("#popUps").animate({
                opacity: 0
            }, 400, function () {
                $(this).css("display", "none");
            });
            $("#popup-let-us").css("display", "none");
            $("#popup-how-can").css("display", "none").css('opacity', "1");
            $("#thankYouPopUp").css("display", "none").css('opacity', "0");
            $("#popup-download-pdf").css("display", "none");
        }

        // click on "CONTACT US" button of the bottom red line
        $("#bottomJobLine_btn").click(function () {
            showContactUs();
        });

        // click on "CONTACT US" button of the top banner
        $("#contactUs-homeBanner").click(function () {
            showContactUs();
        });

        // click on "CONTACT US" button of the home page bottom grey line
        $("#home-how-can-btn").click(function () {
            showContactUs();
        });

        // show Contact Us after "timeToShow" time
        if(popUpSettings.visibility == "visible" && popUpSettings.timeToShow != "" && popUpSettings.timeToShow != " ") {
            setTimeout(showContactUs, parseInt(popUpSettings.timeToShow));
        }

        // click event for 404 error page
        if($("#error_page_contact_us_btn").length){
            $("#error_page_contact_us_btn").click(function(){
                showContactUs();
            });
        }

        // separate fullName field to firstName and lastName after click on Send
        $("#popup-how-can-send").click(function(){
            //var fullName = $("#how-can-we-help-fullname").val();
            //var lastName = "no";
            //var separatedNames = fullName.split(" ");
            //if(fullName.length > 2){
            //    if(separatedNames[1]){
            //        lastName = separatedNames[1];
            //    }
            //    $("#how-can-we-help-firstname").attr("value", separatedNames[0]);
            //    $("#how-can-we-help-lastname").attr("value", lastName);
                $('#contact_us_submit_btn').trigger('click');
                $("#popup-how-can").animate({opacity: 0}, 500, function(){
                    $("#thankYouPopUp").css("display", "block").animate({opacity: 1}, 2000, function(){
                        hideContactUs();
                    });
                });
            //}else{
            //    alert("Your name is too short!")
            //}
        });

    }


    //===============================================
    //============= download PDF file ===============
    //===============================================
    $("#download-pdf-button").click(function () {
        // get the link
        var pdfLink = $(this).data("link");
        // show the pop up
        $("#popup-download-pdf").css("display", "block");
        $("#popUps").css("display", "block").animate({
            opacity: 0.95
        }, 400);
        // open pdf in new tab
        $("#popup-download-pdf-send").click(function () {
            var win = window.open(pdfLink, '_blank');
            win.focus();
            $('#popup-download-pdf-form').trigger('click');
            // separate fullName field to firstName and lastName after click on Send
            //var fullName = $("#download-pdf-form-fullname").val();
            //var lastName = "no";
            //var separatedNames = fullName.split(" ");
            //if(fullName.length > 2){
            //    // open new tab with the pdf
            //
            //
            //    if(separatedNames[1]){
            //        lastName = separatedNames[1];
            //    }
            //    //$("#download-pdf-form-firstname").attr("value", separatedNames[0]);
            //    //$("#download-pdf-form-lastname").attr("value", lastName);
            //    $('#download-pdf-submit').trigger('click');
            //}else{
            //    alert("Your name is too short!")
            //}
        });

    });


    //=================================================
    //================ Let us call you ================
    //=================================================
    $("#let_us_call_you_btn").click(function(){
        //$("#popup-let-us").css("display", "block");
        //$("#popUps").css("display", "block").animate({
        //    opacity: 0.95
        //}, 400);
        showContactUs();
    });
    $("#popup-let-us-send").click(function(){
        $('#popup-let-us-form').trigger('click');
    });


    //=================================================
    //============= click on close button =============
    //=================================================
    $(".closePopUp").click(function () {
        hideContactUs();
    });

});
