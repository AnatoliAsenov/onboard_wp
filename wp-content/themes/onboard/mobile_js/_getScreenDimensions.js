/**
 * Get screen dimensions
 */



var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
var screenHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var mainContainerWidth = 290;

if (screenWidth <= 320){
    mainContainerWidth = 290;
}else{
    if (screenWidth <= 478){
        mainContainerWidth = 290;
    }else{
        if (screenWidth <= 766){
            mainContainerWidth = 450;
        }else{
            if (screenWidth <= 1022){
                mainContainerWidth = 725;
            }else{

            }
        }
    }
}


var screenWidthOnLoad = document.documentElement.clientWidth;
$(window).resize(function() {
    if ($(window).width() != screenWidthOnLoad) {
        location.reload();
    }
});
