<?php
include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(170);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(170);
$header->printHTML();

$siteURL = get_site_url();

$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{middle-line-text}', '{/middle-line-text}');
$middleLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/wwdo_768.jpg" id="topBanner">

    <div id="wwd-container">

        <div id="wwd_title">SALESmanago</div>

        <ul id="technology-intro-ul" class="technology-gsuite-last-text">
            <li id="technology-sf1-img1">
                <img src="<?php bloginfo('template_url'); ?>/images/technology/sales-manago/logo.png" />
            </li>
            <li>
                <p><?php echo $block1; ?></p>
                <p><?php echo $block2; ?></p>
            </li>
            <li id="technology-sf1-img2">
                <img src="<?php bloginfo('template_url'); ?>/images/technology/sales-manago/logo.png" />
            </li>
        </ul>

        <ul id="technology-middle-line" class="technology-salesmanago-middle-line">
            <li><i class="demo-icon icon-quote-right-alt">&#xe801;</i></li>
            <li><p><?php echo $middleLineText; ?></p></li>
        </ul>

        <img src="<?php bloginfo('template_url'); ?>/images/technology/sales-manago/robot.png" id="technology-sf2-img" />

        <ul id="technology-second-text-block" class="technology-gsuite-last-text">
            <li>
                <p><?php echo $block3; ?></p>
            </li>
        </ul>

    </div>

    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;


$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();