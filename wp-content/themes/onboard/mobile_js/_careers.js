
$(document).ready(function() {


//=============================================
//========== Employee Quote slider ============
//=============================================
    if($("#emp_short_quo").length) {
        (function () {

            // get all content
            var mainContent = $("#questions_and_answers").text();
            var singleQuestion = "";

            function getSubstring(content, begin, end) {
                var beginStartIndex = content.indexOf(begin);
                var beginLength = begin.length;
                var endStartIndex = content.indexOf(end);
                var endLength = end.length;
                return content.slice((beginStartIndex + beginLength), endStartIndex);
            }

            function getPropertiesOfQuestion(content, begin, end, isWholeQuestion) {
                var beginStartIndex = content.indexOf(begin);
                var beginLength = begin.length;
                var endStartIndex = content.indexOf(end);
                var endLength = end.length;
                var forReturn = content.slice((beginStartIndex + beginLength), endStartIndex);
                if (isWholeQuestion) {
                    mainContent = content.slice(endStartIndex + endLength);
                } else {
                    singleQuestion = content.slice(endStartIndex + endLength);
                }
                return forReturn;
            }

            function numberOfOccurrences(content, substring) {
                var regExp = new RegExp(substring, "gi");
                return (content.match(regExp) || []).length;
            }

            // number of questions
            const numberOfQuestions = numberOfOccurrences(mainContent, "{question}");

            // generate questions array
            var questions = [];
            var answer = [];

            for (var x = 0; x < numberOfQuestions; x++) {
                var tempQuestion = {};
                // get whole question
                var tempContent = getPropertiesOfQuestion(mainContent, "{question}", "{/question}", true);
                // get visibility property
                tempQuestion.visibility = getPropertiesOfQuestion(tempContent, "{visibility}", "{/visibility}", false);
                // get title of question
                tempQuestion.questionTitle = getPropertiesOfQuestion(tempContent, "{title}", "{/title}", false);
                // get the question content
                tempQuestion.questionContent = getPropertiesOfQuestion(tempContent, "{content}", "{/content}", false);

                // get all employees opinions
                function getPropertiesOfEmployee(content, begin, end) {
                    var beginStartIndex = content.indexOf(begin);
                    var beginLength = begin.length;
                    var endStartIndex = content.indexOf(end);
                    var endLength = end.length;
                    var forReturn = content.slice((beginStartIndex + beginLength), endStartIndex);
                    tempContent = content.slice(endStartIndex + endLength);
                    return forReturn;
                }

                tempQuestion.numberOfAnswers = numberOfOccurrences(tempContent, "{employee}");
                for (var v = 0; v < tempQuestion.numberOfAnswers; v++) {
                    // get single answer
                    var employeeContent = getPropertiesOfEmployee(tempContent, "{employee}", "{/employee}", false);

                    var tempEmployee = {};
                    tempEmployee.name = getSubstring(employeeContent, "{name}", "{/name}", false);
                    tempEmployee.quote = getSubstring(employeeContent, "{quote}", "{/quote}", false);
                    tempEmployee.gender = getSubstring(employeeContent, "{gender}", "{/gender}", false);
                    answer.push(tempEmployee);
                }
                tempQuestion.answers = answer;
                answer = [];
                if (tempQuestion.visibility == "visible") {
                    questions.push(tempQuestion);
                }

            }

            //===================================
            //======== render the texts =========
            //===================================
            var questionCount = 0;
            var iterateOverAnswers = false;
            var answerCount = 0;
            var tempQuestionCount;
            renderQuestion();
            setInterval(renderQuestion, 3500);

            function renderQuestion() {

                //render question
                if (!iterateOverAnswers) {
                    // print
                    loadQuestion(questionCount);

                    // count number of printed questions
                    tempQuestionCount = questionCount;
                    questionCount++;
                    iterateOverAnswers = true;
                    if (questionCount > questions.length - 1) {
                        questionCount = 0;
                        //tempQuestionCount = 0;
                        iterateOverAnswers = true;
                    }

                }

                if (iterateOverAnswers) {
                    // print
                    loadQuote(questions[tempQuestionCount].answers[answerCount]);

                    // count number of printed answers
                    answerCount++;
                    if (answerCount >= questions[tempQuestionCount].numberOfAnswers) {
                        answerCount = 0;
                        iterateOverAnswers = false;
                    }

                }

            }

            function loadQuestion(n) {
                var textContainer = $("#block2_onboard_Text");
                var authorName = $("#block2_onboard_Text div");
                var authorText = $("#block2_onboard_Text p");
                textContainer.animate({opacity: 0}, 300, function () {
                    authorName.text(questions[n].questionTitle);
                    authorText.text(questions[n].questionContent.slice(0, 90));
                    textContainer.animate({opacity: 1}, 300);
                });
            }

            function loadQuote(a) {
                // change icon male/female
                var theIconDiv = $("#block2_gender");

                theIconDiv.animate({
                    color: "#fff"
                }, 400, function () {
                    if (a.gender == "f") {
                        theIconDiv.attr("data-icon", "k").animate({color: "#EB5C4E"}, 400);
                    } else {
                        theIconDiv.attr("data-icon", "h").animate({color: "#EB5C4E"}, 400);
                    }
                });


                // replace the texts
                var textContainer = $("#block2_employee_Text");
                var authorName = $("#block2_employee_Text div");
                var authorText = $("#block2_employee_Text p");
                textContainer.animate({opacity: 0}, 300, function () {
                    authorName.text(a.name);
                    authorText.text(a.quote.slice(0, 90));
                    textContainer.animate({opacity: 1}, 300);
                });
            }

        })();
    }

    //===========================================================
    //=============== "see where you fit" button ================
    //===========================================================
    if ($("#careers-block1-button").length) {
        var compensationHeight = 65;

        $("#careers-block1-button").click(function(){

            var titleTopOffset = $("#careers-joblist-title").offset().top;
            $("html, body").animate({
                scrollTop: (titleTopOffset - compensationHeight)
            }, 1500);
        });
    }


});