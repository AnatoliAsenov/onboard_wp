var isValidationSuccessful = false;

$(document).ready(function() {

    function hidePopUp(){
        $("#thePopUp").animate({
            opacity: 0
        }, 400, function () {
            $(this).css("display", "none");
            $("#onboardForm").css('opacity', "1");
            $("#thankYouPopUp").css("display", "none").css('opacity', "0");
        });
    }

    //=================================================
    //============= click on close button =============
    //=================================================
    $(".how-can-we-help-close").click(function () {
        hidePopUp();
        //$("#how-can-we-help-form").css("display", "none");
        //$("#download-pdf-form").css("display", "none");
        //$("#let_us_call_you_form").css("display", "none");
    });

    // get popup settings
    if($("#popup-settings").length) {

        var settingsDiv = $("#popup-settings");
        var popUpSettings = {};
        popUpSettings.timeToShow = settingsDiv.data("time");
        popUpSettings.visibility = settingsDiv.data("visibility");
    }


    // show big form
    function showBigForm(){
        //$("#how-can-we-help-form").css("display", "block");
        $("#thePopUp").css("display", "block").animate({
            opacity: 0.98
        }, 500);
    }

    // attach click event to buttons
    $("#home-how-can-btn").click(function(){
        showBigForm();
    });
    $("#message-topmenu-btn p").click(function(){
        showBigForm();
    });
    $("#message-topmenu-btn i").click(function(){
        showBigForm();
    });
    $("#bottomJobLine_btn").click(function(){
        showBigForm();
    });
    $("#let_us_call_you_btn").click(function(){
        showBigForm();
    });
    $("#talkToExpert").click(function(){
        showBigForm();
    });

    // click submit button
    // $("#form_submit_btn").click(function(){
    //     if( formValidation() ){
    //         // submit the form
    //         $("#contact_us_submit_btn").click();

    //         $("#onboardForm").animate({opacity: 0}, 500, function(){
    //             $("#thankYouPopUp").css("display", "block").animate({opacity: 1}, 2000, function(){
    //                 hidePopUp();
    //             });
    //         });
    //     }
    // });



    // form validation
    function formValidation(){

        // email validation
        var $email = $('#thePopUp input[name="Email');
        var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        if ($email.val() == '' || !re.test($email.val()))
        {
            alert('Please enter a valid email address.');
            return false;
        }

        // validate "Names" and "Company" fields
        var charTestFields = [$('#thePopUp input[name="First Name"]'), $('#thePopUp input[name="Last Name"]'), $('#thePopUp input[name="Company"]')];
        for(var charTestField of charTestFields){
            var charTestFieldContent = charTestField.val();
            if(charTestFieldContent.length < 3){
                alert('Please enter a valid '+charTestField.attr("name")+"!");
                return false;
            }
        }

        // validate "Request type" and "Country"
        var dropdowns = $('#thePopUp select');
        isValidationSuccessful = false;
        dropdowns.each(function(){
            if( $(this)[0].name == "LEADCF2" || $(this)[0].name == "LEADCF4" ){
                if( $(this).val() == "-None-" ){
                    alert('Please enter "Country" and "Request type"!');
                    isValidationSuccessful = false;
                    return false;
                }
                isValidationSuccessful = true;
            }
        });

        // validation not successful
        if (!isValidationSuccessful){
            return false;
        }

        // request the Google captcha
        var msg_captch = " You need to check Captcha";
        var captch = jQuery('#g-recaptcha-response').val();
        if(!captch)
        {
            //alert(msg_captch);
            //return false;
        }

        // validation successful
        return true;
    }

    function notInUse(){
        //===============================================
        //========== show "how can help you?" ===========
        //===============================================
        if($("#popup-settings").length) {

            var settingsDiv = $("#popup-settings");
            var popUpSettings = {};
            popUpSettings.timeToShow = settingsDiv.data("time");
            popUpSettings.visibility = settingsDiv.data("visibility");

            function showContactUs() {
                $("#how-can-we-help-form").css("display", "block");
                $("#thePopUp").css("display", "block").animate({
                    opacity: 0.95
                }, 500);
            }

            // click on "CONTACT US" button of the bottom red line
            $("#bottomJobLine_btn").click(function () {
                showContactUs();
            });

            // show Contact Us after "timeToShow" time
            if(popUpSettings.visibility == "visible" && popUpSettings.timeToShow != "" && popUpSettings.timeToShow != " ") {
                setTimeout(showContactUs, parseInt(popUpSettings.timeToShow));
            }

            // click event for 404 error page
            if($("#error_page_contact_us_btn").length){
                $("#error_page_contact_us_btn").click(function(){
                    showContactUs();
                });
            }

            // separate fullName field to firstName and lastName after click on Send
            $("#how-can-we-help-send").click(function(){
                var fullName = $("#how-can-we-help-fullname").val();
                var lastName = "no";
                var separatedNames = fullName.split(" ");
                if(fullName.length > 2){
                    if(separatedNames[1]){
                        lastName = separatedNames[1];
                    }
                    $("#how-can-we-help-firstname").attr("value", separatedNames[0]);
                    $("#how-can-we-help-lastname").attr("value", lastName);
                    $('#how-can-we-help-submit').trigger('click');
                }else{
                    alert("Your name is too short!")
                }
            });

        }


        //===============================================
        //============= download PDF file ===============
        //===============================================
        $("#download-pdf-button").click(function () {
            // get the link
            var pdfLink = $(this).data("link");
            // show the pop up
            $("#download-pdf-form").css("display", "block");
            $("#thePopUp").css("display", "block").animate({
                opacity: 0.95
            }, 400);
            $("#download-pdf-download").click(function () {
                var win = window.open(pdfLink, '_blank');
                win.focus();
                $('#download-pdf-submit').trigger('click');
                // separate fullName field to firstName and lastName after click on Send
                //var fullName = $("#download-pdf-form-fullname").val();
                //var lastName = "no";
                //var separatedNames = fullName.split(" ");
                //if(fullName.length > 2){
                //    // open new tab with the pdf
                //
                //
                //    if(separatedNames[1]){
                //        lastName = separatedNames[1];
                //    }
                //    //$("#download-pdf-form-firstname").attr("value", separatedNames[0]);
                //    //$("#download-pdf-form-lastname").attr("value", lastName);
                //    $('#download-pdf-submit').trigger('click');
                //}else{
                //    alert("Your name is too short!")
                //}
            });

        });


        //===============================================
        //============== Let us call you ================
        //===============================================
        $("#let_us_call_you_btn").click(function(){
            $("#let_us_call_you_form").css("display", "block");
            $("#thePopUp").css("display", "block").animate({
                opacity: 0.95
            }, 400);
        });
        $("#let-us-call-you-send").click(function(){
            $('#let-us-call-you-hidden-send').trigger('click');
        });
    }

});
