<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(176);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(176);
$header->printHTML();

$siteURL = get_site_url();
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">facts</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/ff_768.jpg" id="topBanner">

<?php
    $stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
    $title = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
    $block1 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
    $block2 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
    $block3 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
    $block4 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{list}', '{/list}');
    $list = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
?>

<div id="ff-whyonboard-container">
    <!-- title -->
    <div id="ff-whyonboard-title"><?php echo $title; ?></div>

    <img src="<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/why1.png" class="ff-whyonboard-img" />

    <ul class="ff-whyonboard-texts">
        <li><?php echo $block1; ?></li>
        <li><?php echo $block2; ?></li>
    </ul>

    <ul id="ff-whyonboard-list">
<?php
    $numberOfListItems = preg_match_all('/\blist-item\b/', $list);
    for($z = 0; $numberOfListItems/2 > $z; $z++) {
        $stringManipulator->stringExtractAndDelete($list, '{list-item}', '{/list-item}');
        $tempParagraph = $stringManipulator->neededSubString;
        $list = $stringManipulator->reducedString;
?>
    <li>
        <div class="red-triangle-list-element"></div>
        <p class="ff-whyonboard-paragraph"><?php echo $tempParagraph; ?></p>
    </li>
<?php
    }
?>
    </ul>

    <img src="<?php bloginfo('template_url'); ?>/images/mobile/factsfigures/why2.png" class="ff-whyonboard-img" />

    <ul class="ff-whyonboard-texts">
        <li><?php echo $block3; ?></li>
        <li><?php echo $block4; ?></li>
    </ul>
</div>

    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();
