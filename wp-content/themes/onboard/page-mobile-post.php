<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$postID = get_query_var( 'id' );
$singlePost = get_post( $postID );

$header = new MobileHeader($postID);
$header->printHTML();

$siteURL = get_site_url();
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">blog</div>

<!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/blog/blog_768.jpg" id="topBanner">


<?php

//var_dump($singlePost);

?>

<div id="postContainer">
    <h1 id="singlePostTitle"><?php echo $singlePost->post_title; ?></h1>
    <span id="singlePostDate">
        <span class="blog-big_article_category"><?php echo get_the_category($postID)[0]->name; ?></span>
        <i class="demo-icon icon-calendar">&#xe803;</i>
        <span class="blog-big_article_date"><?php the_time('F j, Y'); ?></span>
    </span>
    <?php echo get_the_post_thumbnail($postID); ?>
    <div id="singlePostContent"><?php echo $singlePost->post_content; ?></div>
</div>

<!-- sharing buttons -->
<ul id="sharing-buttons">
    <li>
        <a class="facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>?id=<?php echo $postID; ?>" target="_blank">
            <i class="fa fa-facebook-square" aria-hidden="true" style="color: #3A5A97;"></i>
        </a>
    </li>
    <li>
        <a class="twitter" href="https://twitter.com/intent/tweet?url=<?php the_permalink(); ?>" target="_blank">
            <i class="fa fa-twitter-square" id="twitter" style="color: #1DA1F3;"></i>
        </a>
    </li>
    <li>
        <a class="google-plus" href="https://plus.google.com/share?url=<?php the_permalink(); ?>" target="_blank">
            <i class="fa fa-google-plus-square" id="google-plus" style="color: #DC5043;"></i>
        </a>
    </li>
    <li>
        <a class="linkedin" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&source=http://www.onboardcrm.com" target="_blank">
            <i class="fa fa-linkedin-square" aria-hidden="true" style="color: #0077B4;"></i>
        </a>
    </li>
</ul>

<hr class="blog_hr">

<!-- tags -->
<div id="tagsList">
    <i class="fa fa-bookmark" aria-hidden="true"></i>
    <p>TAGS</p>
    <ul>
    <?php
        $allTags = get_the_tags($postID);
        for($i = 0; count($allTags) > $i; $i++){
            if($allTags[$i]->name != "" && $allTags[$i]->name != " "){
                $blogURL = get_home_url()."/blog";
    ?>
        <li class="tagsList-li">
            <a href="<?php echo add_query_arg( 'tag', $allTags[$i]->term_id, $blogURL ); ?>" class="tagsList-a">
                <h3 class="tagsList-h3"><?php echo $allTags[$i]->name; ?></h3>
            </a>
        </li>
    <?php
        }   }
    ?>
    </ul>
</div>


<!-- subscribe field -->
<p id="blog-subscribe">SUBSCRIBE</p>
<div id="blog-subscribe-input">
    <input type="text" placeholder="Your Email Address"/>
    <div class="icon" data-icon="p"></div>
</div>


<!-- footer -->
<?php
// bottom red line content
//$stringManipulator = new StringManipulation();
//$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
//$bottomRedLineText = $stringManipulator->neededSubString;
//$postContent = $stringManipulator->reducedString;

$bottomRedLine = new MobileBottomRedLine("We are the ultimate choice for your job");
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();
