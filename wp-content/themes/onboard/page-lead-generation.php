<?php


include 'string_manipulation/StringManipulation.php';
include 'templates/MiddlePageBoxes.php';
include 'templates/BottomRedLine.php';

get_header();
$siteURL = get_site_url();

?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/banners/whatwedo.jpg" id="topBanner">


    <div id="whatWeDo_title">LEAD GENERATION</div>

<?php

// string manipulator
$stringManipulator = new StringManipulation();

$postContent = $post->post_content;


//====== block 1 =======

//echo $postContent;
$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph1}', '{/paragraph1}');
$paragraph1 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph2}', '{/paragraph2}');
$paragraph2 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph3}', '{/paragraph3}');
$paragraph3 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($block1, '{paragraph4}', '{/paragraph4}');
$paragraph4 = $stringManipulator->neededSubString;
$block1 = $stringManipulator->reducedString;

?>


    <div id="leadGenerationContainer">

        <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/lead_generation/main.png" id="leadGenerationImg1" />

        <div class="channelMarketingSubTitle">THE GORILLA ONBOARD SOLUTION</div>

        <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/lead_generation/graph.png" id="leadGenerationImg2" />

        <div class="channelMarketingSubTitle">THE GORILLA ONBOARD BENEFITS</div>

        <ul id="leadGenerationUl">
            <li>
                <div class="list-triangle facts-R-list-triangle-inline"></div>
                <p class="leadGenerationTriangleTexts"><?= $paragraph1 ?></p>
            </li>
            <li>
                <div class="list-triangle facts-R-list-triangle-inline"></div>
                <p class="leadGenerationTriangleTexts"><?= $paragraph2 ?></p>
            </li>
            <li>
                <div class="list-triangle facts-R-list-triangle-inline"></div>
                <p class="leadGenerationTriangleTexts"><?= $paragraph3 ?></p>
            </li>
            <li>
                <p><?= $paragraph4 ?></p>
            </li>
        </ul>

    </div>



    <!-- footer -->
<?php

// bottom red line content
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();
