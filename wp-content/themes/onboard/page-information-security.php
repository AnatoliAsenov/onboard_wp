<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

get_header();
$domainURL = get_site_url();

?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">technology</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/technology/technology.jpg" id="topBanner">

    <!-- title of page -->
    <div id="credentials-iso-title">INFORMATION SECURITY POLICY</div>

    <div id="credentials-iso" class="page-with-alotof-text"><?php echo $post->post_content; ?></div>




<?php

get_footer();