/**
 * Created by Anatoli Asenov
 */

$(document).ready(function() {

   if($("#middleBoxes_middleContainer").length){

       const changeColorTime = 800;
       const bordersAnimationTime = 700;
       const horizontalLinesWidth = 442;
       const marginOfHorizontalLines = 50;
       const marginOfVerticalLines = 40;

       // get texts containers height
       const leftTextContainerHeight = document.querySelector('#middleBoxes_leftText').clientHeight;
       const rightTextContainerHeight = document.querySelector('#middleBoxes_rightText').clientHeight;
       var verticalLeftLineHeight = leftTextContainerHeight - marginOfVerticalLines;
       var verticalRightLineHeight = rightTextContainerHeight - marginOfVerticalLines;


       // animating
       var animatingFunc = function() {

           $("#middleBoxes_middleContainer_box").animate({
               backgroundColor: "#ffffff"
           }, changeColorTime);
           $("#middleBoxes_middleContainer_box i").animate({
               color: "#EB5C4E"
           }, changeColorTime);
           $("#middleBoxes_middleContainer_quote i").animate({
               color: "#ffffff"
           }, changeColorTime);
           $("#middleBoxes_middleContainer_quote").animate({
               backgroundColor: "#EB5C4E"
           }, changeColorTime, function () {
               //left container
               $("#middleBoxes_leftTopBorder").animate({
                   width: horizontalLinesWidth + "px",
                   marginLeft: marginOfHorizontalLines + "px"
               }, bordersAnimationTime);
               $("#middleBoxes_leftRightBorder").animate({
                   height: verticalLeftLineHeight + "px"
               }, bordersAnimationTime);
               $("#middleBoxes_leftLeftBorder").animate({
                   height: verticalLeftLineHeight + "px",
                   marginTop: marginOfVerticalLines + "px"
               }, bordersAnimationTime);
               $("#middleBoxes_leftBottomBorder").animate({
                   width: horizontalLinesWidth + "px"
               }, bordersAnimationTime);
               // right container
               $("#middleBoxes_rightBottomBorder").animate({
                   width: horizontalLinesWidth + "px",
                   marginLeft: marginOfHorizontalLines + "px"
               }, bordersAnimationTime);
               $("#middleBoxes_rightLeftBorder").animate({
                   height: verticalRightLineHeight + "px"
               }, bordersAnimationTime);
               $("#middleBoxes_rightRightBorder").animate({
                   height: verticalRightLineHeight + "px",
                   marginTop: marginOfVerticalLines + "px"
               }, bordersAnimationTime);
               $("#middleBoxes_rightTopBorder").animate({
                   width: horizontalLinesWidth + "px"
               }, bordersAnimationTime, function () {
                   // move up the bottom boxes
                   $(".middleBoxes_text_holders").animate({
                       marginTop: "-87px"
                   }, 900);
               });
           });
       };

       // activate animations
       var isAnimationActivated = false;

       $(window).scroll(function() {
           if(isAnimationActivated == false) {
               var middlePixelHeight = $("#trackingPixel_middle").offset().top;
               var boxesTopOffset = $("#middleBoxes_bigContainer").offset().top;
                if(middlePixelHeight >= boxesTopOffset){
                    animatingFunc();
                    isAnimationActivated = true;
                }
           }
       });

   }
});
