<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(164);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(164);
$header->printHTML();

$siteURL = get_site_url();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">whatwedo</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/whatwedo/wwdo_768.jpg" id="topBanner">

    <div id="wwd-container">

        <div id="wwd_title">TECH & EXPERTISE</div>

        <ul id="technology-list">
            <li>
                <a href="<?php echo $siteURL."/mobile/mobile-salesforce"; ?>" >
                    <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce_link.png" >
                </a>
            </li>
            <li>
                <a href="<?php echo $siteURL."/mobile/mobile-eloqua"; ?>" >
                    <img src="<?php bloginfo('template_url'); ?>/images/technology/eloqua_link.png" >
                </a>
            </li>
            <li>
                <a href="<?php echo $siteURL."/mobile/mobile-gsuite"; ?>" >
                    <img src="<?php bloginfo('template_url'); ?>/images/technology/gsuite_link.png" >
                </a>
            </li>
            <li>
                <a href="<?php echo $siteURL."/mobile/mobile-salesmanago"; ?>" >
                    <img src="<?php bloginfo('template_url'); ?>/images/technology/salesmanago_link.png" >
                </a>
            </li>
            <li>
                <a href="<?php echo $siteURL."/mobile/mobile-credentials"; ?>" >
                    <img src="<?php bloginfo('template_url'); ?>/images/technology/credentials_link.png" >
                </a>
            </li>
        </ul>

        <div id="technology-texts">
        <?php
        // print all paragraphs
        $numberOfSubTitles = preg_match_all('/\bparagraph-delimiter\b/', $postContent);

        for($i = 0; $i < $numberOfSubTitles/2; $i++) {
            $stringManipulator->stringExtractAndDelete($postContent, '{paragraph-delimiter}', '{/paragraph-delimiter}');
            $tempContent = $stringManipulator->neededSubString;
            $postContent = $stringManipulator->reducedString;
            ?>
            <p><?php echo $tempContent; ?></p>
        <?php } ?>
        </div>


        <ul id="technology-subscribe-list">
<?php
$numberOfParagraphs = preg_match_all('/\blist-item\b/', $postContent);

for($z = 0; $numberOfParagraphs/2 > $z; $z++) {
    $stringManipulator->stringExtractAndDelete($postContent, '{list-item}', '{/list-item}');
    $tempParagraph = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
    echo "<li><div class='red-triangle-list-element'></div><p>".$tempParagraph."</p></li>";
} ?>
        </ul>


    </div>

    <!-- footer -->
<?php
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;


$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();