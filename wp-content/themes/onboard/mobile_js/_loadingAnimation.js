
//______________ hide loading animation _______________
var hideLoadingAnimation = function(){
    $("#loadingContainer").animate({
        opacity: 0
    }, 300, function(){
        $(this).css("display", "none");
    });
};

$(document).ready(function() {
    setTimeout(hideLoadingAnimation, 600);
});