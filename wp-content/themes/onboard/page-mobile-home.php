<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(46);
$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();

$header = new MobileHeader(46);
$header->printHTML();

$siteURL = get_site_url();


?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">home</div>

    <!-- top banner -->
    <div style="background-image: url(<?php bloginfo('template_url'); ?>/images/mobile/home/banner.png);" id="homeBanner" >
        <h1>Your Business Growth Agency</h1>
        <div id="contactUs-homeBanner">CONTACT US</div>
    </div>

    <img src="<?php bloginfo('template_url'); ?>/images/mobile/home/services.png" id="home-services-banner" />

    <!-- services begin -->
    <div id="newServices">

        <svg overflow="visible" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
            <defs></defs>
            <path data-label="Worldwide Market Entry"/>
            <path data-label="Digital Marketing"/>
            <path data-label="Database Services"/>
            <path data-label="B2B Marketing & Sales"/>
            <path data-label="Business Processes"/>
        </svg>

        <div id="home-circle-inner">
            <div id="innerText_1">
                <p>Change the Game</p>
                <p>Power Your Tech</p>
            </div>
            <p id="innerText_2">CRM Consultancy Services - Salesforce.com - Oracle Marketing Cloud - G Suite</p>
            <a id="innerText" href="<?php echo $siteURL.'/mobile/mobile-whatwedo/mobile-business-process-enablement/'; ?>" data-siteurl="<?php echo $siteURL; ?>">
                <div class="icon icon-slim-right"></div>
            </a>
        </div>

        <div id="services-text-holder">
            <?php
            $stringManipulator->stringExtractAndDelete($postContent, '{content}', '{/content}');
            $allTheText = $stringManipulator->neededSubString;
            $numberOfParagraph = preg_match_all('/\bparagraph-delimiter\b/', $allTheText);
            for($a = 0; $numberOfParagraph/2 > $a; $a++){
                $stringManipulator->stringExtractAndDelete($allTheText, '{paragraph-delimiter}', '{/paragraph-delimiter}');
                $tempText = $stringManipulator->neededSubString;
                $allTheText = $stringManipulator->reducedString;
                echo "<p>".$tempText."</p>";
            }
            ?>
        </div>

    </div>
    <!-- services end -->


    <!-- expertise -->
    <img src="" id="home-expertise"/>
    <!-- tables for 320px and 480px view -->
    <table id="home-expertise-tab">
        <tr>
            <th></th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/mobile-technology'; ?>" id="home-expertise-tab-arrow">
                    <div class="icon icon-slim-right"></div>
                </a>
            </th>
        </tr>
        <tr>
            <th><h1>EXPERTISE</h1></th>
            <th></th>
        </tr>
        <tr>
            <th>
                <a href="<?php echo $siteURL.'/mobile/mobile-salesforce'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/salesforce.png"/>
                </a>
            </th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/mobile-eloqua'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/eloqua.png"/>
                </a>
            </th>
        </tr>
        <tr>
            <th class="home-expertise-tab-text">
                <a href="<?php echo $siteURL.'/mobile/mobile-salesforce'; ?>">
                    <h2>SalesForce</h2>
                </a>
            </th>
            <th class="home-expertise-tab-text">
                <a href="<?php echo $siteURL.'/mobile/mobile-eloqua'; ?>">
                    <h2>Eloqua</h2>
                </a>
            </th>
        </tr>
        <tr>
            <th>
                <a href="<?php echo $siteURL.'/mobile/mobile-gsuite'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/gsuite.png"/>
                </a>
            </th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/mobile-salesmanago'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/sales_manago.png"/>
                </a>
            </th>
        </tr>
        <tr>
            <th class="home-expertise-tab-text">
                <a href="<?php echo $siteURL.'/mobile/mobile-gsuite'; ?>">
                    <h2>G Suite</h2>
                </a>
            </th>
            <th class="home-expertise-tab-text">
                <a href="<?php echo $siteURL.'/mobile/mobile-salesmanago'; ?>">
                    <h2>SALESmanago</h2>
                </a>
            </th>
        </tr>
        <tr>
            <th colspan="2">
                <a href="<?php echo $siteURL.'/mobile/mobile-marketingxpress'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/groot.png"/>
                </a>
            </th>
<!--            <th></th>-->
        </tr>
        <tr>
            <th class="home-expertise-tab-text" colspan="2">
                <a href="<?php echo $siteURL.'/mobile/mobile-marketingxpress'; ?>">
                    <h2>Marketingxpress</h2>
                </a>
            </th>
<!--            <th class="home-expertise-tab-text"></th>-->
        </tr>
    </table>
    <!-- table for 768px view -->
    <table id="home-expertise-tab2">
        <tr>
            <th colspan="4"></th>
            <th colspan="2">
                <a href="<?php echo $siteURL.'/mobile/mobile-technology'; ?>" id="home-expertise-tab2-arrow">
                    <div id="home-expertise-tab2-arrow-text">We connect. We focus on the impact. We are unconventional. We go after results.</div>
                    <div class="icon icon-slim-right"></div>
                </a>
            </th>
        </tr>
        <tr>
            <th class="home-expertise-tab2-buffer"></th>
            <th colspan="2"><h1>EXPERTISE</h1></th>
            <th colspan="3"></th>
        </tr>
        <tr>
            <th class="home-expertise-tab2-buffer"></th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/technology'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/salesforce.png"/>
                </a>
            </th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/technology'; ?>">
                    <h2>SalesForce</h2>
                </a>
            </th>
            <th class="home-expertise-tab2-buffer"></th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/technology'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/eloqua.png"/>
                </a>
            </th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/technology'; ?>">
                    <h2>Eloqua</h2>
                </a>
            </th>
        </tr>
        <tr>
            <th class="home-expertise-tab2-buffer"></th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/technology'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/gsuite.png"/>
                </a>
            </th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/technology'; ?>">
                    <h2>G Suite</h2>
                </a>
            </th>
            <th class="home-expertise-tab2-buffer"></th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/technology'; ?>">
                    <img src="<?php bloginfo('template_url'); ?>/images/logos/sales_manago.png"/>
                </a>
            </th>
            <th>
                <a href="<?php echo $siteURL.'/mobile/technology'; ?>">
                    <h2>SALESmanago</h2>
                </a>
            </th>
        </tr>
    </table>


    <!-- testimonials -->
<?php
    // get case testimonial details
    $args = array('post_type' => 'casestudies', 'post_per_page' => -1);
    $loop = new WP_Query($args);

    $csArray = array();
    foreach($loop->posts as $cssss){
        $testimonialVisibility =  get_post_meta($cssss->ID, 'testimonial_visibility', true);
        if($testimonialVisibility == "visible" || $testimonialVisibility == "") {
            array_push($csArray, $cssss->ID);
        }
    }

    $i = rand(0, (count($csArray) - 1));
?>

    <div id="testimonials">
<?php   if($loop->have_posts()) {
            while( count($csArray) > $i ){
                $postQuote = get_post_meta($csArray[$i], 'testimonial', true);
                $postQuoteAuthor = get_post_meta($csArray[$i], 'testimonial_author', true);
                $post = get_post($csArray[$i]);
                $postContent = $post->post_content;
                $logo = $stringManipulator->stringExtract($postContent, '{logo}', '{/logo}');
                ?>
                <a class="testimonials_item" href="<?php echo $post->guid; ?>">
                    <?php echo $logo; ?>
                    <p class="testimonials_item_quote">
                        <i class="demo-icon icon-quote-left-alt">&#xe802;</i>
                    </p>
                    <div class="testimonials_item_testimonial"><?php echo $postQuote; ?></div>
                    <div class="testimonials_item_author"><?php echo $postQuoteAuthor; ?></div>
                </a>
                <?php
                $i++;
            }
        }
?>
    </div>
    <div id="testimonials_arrows_holder">
        <div id='testimonials_left_arrow'></div>
        <div id='testimonials_right_arrow'></div>
    </div>


<!-- case studies -->
    <img src="<?php bloginfo('template_url'); ?>/images/icons/cases.png" id="home-cs-book" />

    <div id="home-cs-tit">CASES</div>

    <ul id='caseStudyUl'>
<?php
    foreach($loop->posts as $caseStudy){
        $caseStudyType = wp_get_post_terms($caseStudy->ID, 'casestudytype');

        // get custom fields data for the current post
        $customFields_thumbnail =  get_post_meta($caseStudy->ID, 'thumbnail', true);

        $postContent = $caseStudy->post_content;
        $image = $stringManipulator->stringExtract($postContent, '{image}', '{/image}');
        $imageURL = $stringManipulator->stringExtract($image, 'src="', '"');

        // check for visibility
        $caseStudyVisibility =  get_post_meta($caseStudy->ID, 'visibility', true);
        if($caseStudyVisibility == "visible" || $caseStudyVisibility == "") {
?>
        <li data-type="<?php echo $caseStudyType[0]->name; ?>" class="mix <?php echo $caseStudyType[0]->name; ?>" >
            <a href="<?php echo $siteURL."/mobile/mobile-case?id=".$caseStudy->ID; ?>" class="aOfCaseStudy">
                <div class="cs-content-item" <?php if($imageURL!=""){?> style="background:url('<?php echo $imageURL; ?>');background-repeat:no-repeat;"<?php } ?> >
                    <div class="cs-overlay"></div>
                    <div class="cs-corner-overlay-content">
                        <p class="cs_permanent_text">CASE STUDY</p>
                        <p><?php echo $customFields_thumbnail; ?></p>
                    </div>
                    <div class="cs-overlay-content">
                        <h2><?php echo $customFields_thumbnail; ?></h2>
                        <p><?php echo $caseStudy->post_title; ?></p>
                    </div>
                </div>
            </a>
        </li>
<?php
        }
    };
?>
    </ul>

    <a href="<?php echo $siteURL.'/mobile/mobile-cases'; ?>" id="home-cs-plus-a">
        <img src="<?php bloginfo('template_url'); ?>/images/icons/plus2.png" id="home-cs-plus" />
    </a>


<!-- contact us field -->
<div id="home-how-can-help">
    <p id="home-how-can-text">How can we help you?</p>
    <div id="home-how-can-btn">CONTACT US</div>
</div>


    <!-- footer -->
<?php

$footer = new MobileFooter();
$footer->printHTML();