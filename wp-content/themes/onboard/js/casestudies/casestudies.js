/**
 * Created by Anatoli Asenov
 */

$(document).ready(function() {

    if( $("#caseStudyUl").length ) {
        var containerEl = $('#caseStudyUl');
        var mixer = mixitup(containerEl);
        var caseStudiesTypesBtn = $("#cs-three-types li");

        // iterate over buttons
        $.each(caseStudiesTypesBtn, function () {
            // add click event listener
            $(this).click(function () {

                // activate clicked button
                var clickedBtn = $(this);
                caseStudiesTypesBtn.removeClass("cs-three-types-active");
                clickedBtn.addClass("cs-three-types-active");

            });

        });

    }

});

