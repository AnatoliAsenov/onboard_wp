<?php
/**
 * Template Name: Basic Template
 */
include 'string_manipulation/StringManipulation.php';
include 'templates/BottomRedLine.php';

get_header();

?>



<style>
    @media screen and (min-width: 1400px) {
        #basicContainer {
            width: 1160px;
            margin: 40px auto;
        }
        #basicContainer img{
            max-width: 1160px !important;
        }
    }
    @media screen and (min-width: 1200px) and (max-width: 1400px){
        #basicContainer {
            width: 1060px;
            margin: 40px auto;
        }
        #basicContainer img{
            max-width: 1060px !important;
        }
    }
    @media screen and (max-width: 1200px){
        #basicContainer {
            width: 960px;
            margin: 40px auto;
        }
        #basicContainer img{
            max-width: 960px !important;
        }
    }
</style>

<div id="basicContainer">

<?php

echo $post->post_content;

echo "</div>";

// bottom red line content
$stringManipulator = new StringManipulation();
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();
