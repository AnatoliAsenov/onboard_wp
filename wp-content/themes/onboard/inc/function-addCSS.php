<?php
/**
 * Created by Onboard
 * User: AnatoliAsenov
 */

function add_theme_scripts() {
    // Register the style like this for a theme:
    wp_enqueue_style( 'custom-style', get_template_directory_uri() . '/css/build/style.min.css', array(), '1.0.0', 'all' );

    // Register the script like this for a theme:
    wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/js/build/script.min.js', array(), '1.0.0', 'all' );
}

add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );