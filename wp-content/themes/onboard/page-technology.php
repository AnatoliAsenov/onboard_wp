<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include 'templates/BottomRedLine.php';
include 'string_manipulation/StringManipulation.php';

get_header();
$siteURL = get_site_url();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$pageTitle = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">technology</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/technology/technology.jpg" id="topBanner">

<div id="technology-title"><?php echo $pageTitle; ?></div>

<div id="technology-main">
    <div id="technology-main-L">
        <div id="technology-main-L-text">
<?php
$numberOfParagraphs = preg_match_all('/\bparagraph-delimiter\b/', $postContent);

for($z = 0; $numberOfParagraphs/2 > $z; $z++) {
    $stringManipulator->stringExtractAndDelete($postContent, '{paragraph-delimiter}', '{/paragraph-delimiter}');
    $tempParagraph = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    echo "<p>".$tempParagraph."</p>";
}
?>
        </div>
        <table id="technology-main-L-tab">
<?php
$numberOfParagraphs = preg_match_all('/\blist-item\b/', $postContent);

for($z = 0; $numberOfParagraphs/2 > $z; $z++) {

    $stringManipulator->stringExtractAndDelete($postContent, '{list-item}', '{/list-item}');
    $tempParagraph = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    if(!($z%2)){
        echo "<tr>";
        echo "<th><div class='list-triangle facts-R-list-triangle-inline'></div><p>".$tempParagraph."</p></th>";
    }else{
        echo "<th><div class='list-triangle facts-R-list-triangle-inline'></div><p>".$tempParagraph."</p></th>";
        echo "</tr>";
    }

}

if($z%2){
    echo "<th></th></tr>";
}

?>
        </table>
    </div>
    <ul id="technology-main-R">
        <li>
            <a href="<?php echo $siteURL."/technology/salesforce"; ?>" >
                <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce_link.png" >
            </a>
        </li>
        <li>
            <a href="<?php echo $siteURL."/technology/eloqua"; ?>" >
                <img src="<?php bloginfo('template_url'); ?>/images/technology/eloqua_link.png" >
            </a>
        </li>
        <li>
            <a href="<?php echo $siteURL."/technology/gsuite"; ?>" >
                <img src="<?php bloginfo('template_url'); ?>/images/technology/gsuite_link.png" >
            </a>
        </li>
        <li>
            <a href="<?php echo $siteURL."/technology/salesmanago"; ?>" >
                <img src="<?php bloginfo('template_url'); ?>/images/technology/salesmanago_link.png" >
            </a>
        </li>
        <li>
            <a href="<?php echo $siteURL."/technology/marketingxpress"; ?>" >
                <img src="<?php bloginfo('template_url'); ?>/images/technology/mx_link.png" >
            </a>
        </li>
        <li>
            <a href="<?php echo $siteURL."/technology/credentials"; ?>" >
                <img src="<?php bloginfo('template_url'); ?>/images/technology/credentials_link.png" >
            </a>
        </li>
    </ul>
</div>

<?php

// bottom red line content
$stringManipulator->stringExtractAndDelete($postContent, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();