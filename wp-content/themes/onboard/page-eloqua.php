<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include "templates/MiddlePageLine.php";
include "templates/BottomRedLine.php";
include 'string_manipulation/StringManipulation.php';

get_header();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
?>

<!-- detecting page div-->
<div style="display:none;" id="whatisthispage">technology</div>

<!-- top banner -->
<img src="<?php bloginfo('template_url'); ?>/images/technology/technology.jpg" id="topBanner">

<?php
$stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
$title = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
$block1 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
$block2 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
$block3 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
$block4 = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$stringManipulator->stringExtractAndDelete($postContent, '{middle-line-text}', '{/middle-line-text}');
$middleLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;
?>

<!-- title of page -->
<h1 id="technology-title"><?php echo $title; ?></h1>

<!-- first part -->
<ul id="technology-firstPart">
    <li id="technology-firstPart-li1">
        <p><?php echo $block1; ?></p>
        <p><?php echo $block2; ?></p>
    </li>
    <li id="technology-firstPart-li2">
        <img src="<?php bloginfo('template_url'); ?>/images/technology/eloque/marketing_automation.png">
    </li>
</ul>


<!-- middle red line -->
<?php
    $middleLine = new MiddlePageLine();
    $middleLine->setBackgroundColor("#EB5C4E");
    $middleLine->setText($middleLineText);
    $middleLine->printHTML();
?>


<!-- second part -->
<ul id="technology-secondPart">
    <li id="technology-secondPart-li1">
        <img src="<?php bloginfo('template_url'); ?>/images/technology/eloque/phone.png">
   </li>
    <li id="technology-secondPart-li2">
        <p><?php echo $block3; ?></p>
        <ul id="technology-eloque-ul"><?php echo $block4; ?></ul>
    </li>
</ul>



<?php

// bottom red line content
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();