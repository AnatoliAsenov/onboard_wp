<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

include "templates/MiddlePageLine.php";
include "templates/BottomRedLine.php";
include 'string_manipulation/StringManipulation.php';

get_header();

$postContent = $post->post_content;
$stringManipulator = new StringManipulation();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">technology</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/technology/technology.jpg" id="topBanner">


<?php
    $stringManipulator->stringExtractAndDelete($postContent, '{title}', '{/title}');
    $title = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block1}', '{/block1}');
    $block1 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block2}', '{/block2}');
    $block2 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block3}', '{/block3}');
    $block3 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{block4}', '{/block4}');
    $block4 = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;

    $stringManipulator->stringExtractAndDelete($postContent, '{middle-line-text}', '{/middle-line-text}');
    $middleLineText = $stringManipulator->neededSubString;
    $postContent = $stringManipulator->reducedString;
?>
    <!-- title of page -->
    <h1 id="technology-title"><?php echo $title; ?></h1>

    <!-- first part -->
    <ul id="technology-firstPart">
        <li id="technology-firstPart-li1">
            <p><?php echo $block1; ?></p>
            <p><?php echo $block2; ?></p>
        </li>
        <li id="technology-firstPart-li2">
            <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/Sales-Dashboard.png">
        </li>
    </ul>


    <!-- middle red line -->
<?php
    $middleLine = new MiddlePageLine();
    $middleLine->setBackgroundColor("#248FBD");
    $middleLine->setText($middleLineText);
    $middleLine->printHTML();
?>


    <!-- second part -->
    <ul id="technology-secondPart">
        <li id="technology-secondPart-li1">
            <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/Layer-31.png">
        </li>
        <li id="technology-secondPart-li2">
            <p><?php echo $block3; ?></p>
            <p><?php echo $block4; ?></p>
            <div id="technology-secondPart-sf">
                <div data-icon="a" class="icon" id="talk_to_expert"></div>
                <div>
                    <p id="technology-secondPart-sf-tit">Talk to a CRM expert</p>
                    <p id="technology-secondPart-sf-cont">Drop us a line...</p>
                </div>
                <a href="https://www.salesforce.com/form/signup/freetrial-sales.jsp">FREE TRIAL</a>
            </div>
        </li>
    </ul>


<!-- icons -->
<table id="technology-sf-icon-tab">
    <tr><th colspan="3" id="technology-sf-icon-tab-tit">WHAT WE DO</th></tr>
    <tr>
        <th><img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/rocket-ship.png"></th>
        <th><img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/tablet.png"></th>
        <th><img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/browser.png"></th>
    </tr>
    <tr>
        <th><p>CRM CONSULTANCY & SALESFORCE IMPLEMENTATION</p></th>
        <th><p>SALES CLOUD USER TRAINING</p></th>
        <th><p>SALESFORCE ADMIN SUPPORT</p></th>
    </tr>
    <tr>
        <th><img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/Layer-32.png"></th>
        <th><img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/marketing.png"></th>
        <th><img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/analytics.png"></th>
    </tr>
    <tr>
        <th><p>DATABASE SERVICES</p></th>
        <th><p>MARKETING OPERATIONS</p></th>
        <th><p>BUSINESS INTELLIGENCE</p></th>
    </tr>
</table>

<div id="sf-banners-container">
    <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/sf-admin.png">
    <img src="<?php bloginfo('template_url'); ?>/images/technology/salesforce/sf-consultant.png">
</div>

<?php

// bottom red line content
$stringManipulator->stringExtractAndDelete($post->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$subFooter = new BottomRedLine($bottomRedLineText);
$subFooter->printHTML();

get_footer();