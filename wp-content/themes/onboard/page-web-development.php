<?php
/**
 * Created by Onboard
 * User: Anatoli Asenov
 */

get_header();
$siteURL = get_site_url();

?>

<style>
/* hide header and footer */
    #header{
        display: none;
    }
    #subFooter_container{
        display: none;
    }
    /*body{*/
        /*background-color: #eee;*/
    /*}*/
</style>

<!-- top line -->
<div>
    <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/web-development/small_logo.png" id="wd_topLogo" />
    <div id="wd_socIcons">
        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
    </div>
</div>



<!-- banner -->

<?php
    // render vertical words
    function writeVerticalWords ($word)
    {
        for ($i = 0; $i < strlen($word); $i++) {
            echo "<p>" . $word[$i] . "</p>";
        }
    }
?>

<ul id="wd_banner">
    <li class="wd_bannerVerticalTexts">
        <?php writeVerticalWords ("PROCESS"); ?>
    </li>
<!--  main banner begin  -->
    <li id="wd_banner-main">
        <div id="wd_topButton">
            <div class="wd_buttonElements"></div>
            <div class="wd_buttonElements"></div>
            <div class="wd_buttonElements"></div>
            <div id="wd_topButton-close">X</div>
        </div>
        <div id="contactsContainer">

        </div>
        <div id="wd_bannerLtext">
            <p>WE</p>
            <p>DESIGN AND</p>
            <p>BUILD</p>
        </div>
        <div id="wd_bannerRtext">
            <p>CREATIVE</p>
            <p>WEB</p>
            <p>EXPERIENCE</p>
        </div>
        <img src="<?php bloginfo('template_url'); ?>/images/whatwedo/web-development/big_logo.png" id="wd_innerLogo" />
    </li>
<!--  main banner end  -->
    <li class="wd_bannerVerticalTexts">
        <?php writeVerticalWords ("PLANNER"); ?>
    </li>
</ul>





    <!-- footer -->
<?php

get_footer();