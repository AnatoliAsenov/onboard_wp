<?php

include 'string_manipulation/StringManipulation.php';

get_header();
$siteURL = get_site_url();
$stringManipulator = new StringManipulation();

?>
<!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">home</div>


<!-- slider begin -->

    <!-- dots on the map -->
    <img src="<?php bloginfo('template_url'); ?>/images/home/dot.png" class="maskDots" id="dotID" alt="Onboard logo">

    <!-- mask of the top slider -->
    <div class="maskHolder">
        <img src="<?php bloginfo('template_url'); ?>/images/home/triangle_map.png" class="maskImg" alt="Onboard B2B">
    </div>

    <!-- text of the top banner -->
<?php

    $postContent = $post->post_content;
    $numberOfImages = preg_match_all('/\bslider-image\b/', $postContent);
    $sliderImage = array();
    $sliderText = array();

    for($i = 0; $i < $numberOfImages/2; $i++){
        $stringManipulator->stringExtractAndDelete($postContent, '{slider-image}', '{/slider-image}');
        array_push($sliderImage, $stringManipulator->neededSubString);
        $postContent = $stringManipulator->reducedString;

        $stringManipulator->stringExtractAndDelete($postContent, '{slider-text}', '{/slider-text}');
        $tempText = $stringManipulator->neededSubString;
        $postContent = $stringManipulator->reducedString;
        //$stringManipulator->stringExtractAndDelete($tempText, '<pre>', '</pre>');
        array_push($sliderText, $tempText);

        // take just first slider text
        if($i == 0){
            $textRows = explode("*", $sliderText[0]);
?>
            <div class="sliderText">
                <p class="sliderText_line1"><?php echo $textRows[0]; ?></p>
                <p class="sliderText_line2"><?php echo $textRows[1]; ?></p>
                <p class="sliderText_line3"><?php echo $textRows[2]; ?></p>
            </div>
<?php
        }
    }
?>

    <!-- slider images -->
    <div id="owl-demo">
<?php
    for($z = 0; count($sliderImage) > $z; $z++) {
        $stringManipulator->stringExtractAndDelete($sliderImage[$z], 'src="', '"');
?>
        <div class="item" data-asoctext="<?php echo $sliderText[$z]; ?>">
            <img src="<?php echo $stringManipulator->neededSubString; ?>" alt="slider image">
        </div>
<?php
    }
?>
    </div>
    <!-- slider end -->


    <!-- services begin -->
    <div id="newServices">
        <table id="services_tab">
            <tr>
                <th id="services_tab_left_th">
                    <div class="diagonals" id="services_diagonal"></div>
                    <h1>SERVICES</h1>
                    <!--h2 style="text-align:right;">Sales Impact</h2>
                    <h2 style="margin-left:160px;">B2B Marketing</h2>
                    <h2 style="">Outsourcing Excellence</h2 -->
<?php
    $stringManipulator->stringExtractAndDelete($postContent, '{content}', '{/content}');
    $allTheText = $stringManipulator->neededSubString;
    $numberOfParagraph = preg_match_all('/\btitle-row\b/', $allTheText);
    $marginLeft = array(1, 65, 140, 220);
    for($a = 0; $numberOfParagraph/2 > $a; $a++){
        $stringManipulator->stringExtractAndDelete($allTheText, '{title-row}', '{/title-row}');
        $tempText = $stringManipulator->neededSubString;
        $allTheText = $stringManipulator->reducedString;
        echo "<h2 style='margin-right:".$marginLeft[$a]."px;'>".$tempText."</h2>";
    }
?>
                    <div id="services-text-holder">
<?php
    $stringManipulator->stringExtractAndDelete($postContent, '{content}', '{/content}');
    $allTheText = $stringManipulator->neededSubString;
    $numberOfParagraph = preg_match_all('/\bparagraph-delimiter\b/', $allTheText);
    for($a = 0; $numberOfParagraph > $a; $a++){
        $stringManipulator->stringExtractAndDelete($allTheText, '{paragraph-delimiter}', '{/paragraph-delimiter}');
        $tempText = $stringManipulator->neededSubString;
        $allTheText = $stringManipulator->reducedString;
        echo "<p>".$tempText."</p>";
    }
?>
                    </div>
                </th>
                <th style="text-align:center;" id="circle_holder_th">
                    <svg overflow="visible" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <defs></defs>
                        <path data-label="Worldwide Market Entry"/>
                        <path data-label="Digital Marketing Services"/>
                        <path data-label="Database Services"/>
                        <path data-label="B2B Marketing & Sales"/>
                        <path data-label="Business Process Enablement"/>
                    </svg>
                    <a id="innerText" href="<?php echo $siteURL.'/what-we-do/business-process-enablement/'; ?>" data-siteurl="<?php echo $siteURL; ?>">
                        <div id="innerText_1"><p>Change the Game</p><p>Power Your Tech</p></div>
                        <p id="innerText_2">CRM Consultancy Services - Salesforce.com - Oracle Marketing Cloud - G Suite</p>
                        <p id="innerText_3">&#8594;</p>
                    </a>
                </th>
            </tr>
        </table>
    </div>
    <!-- services end -->


    <!-- expertise begin -->
    <div id="newExpertise">
        <div id="newExpertise_left">
            <ul>
                <li><h1>EXPERTISE</h1></li>
                <li>
                    <table>
                        <tr><th id="newExpertise_left_horizontal_buffer" colspan="8"></th></tr>
                        <tr>
                            <th>
                                <a href="<?php echo $siteURL."/technology/salesforce"; ?>" class="newExpertise_left_aaa">
                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/salesforce.png" alt="salesforce" />
                                </a>
                            </th>
                            <th class="newExpertise_left_firstPicRow">
                                <a href="<?php echo $siteURL."/technology/salesforce"; ?>" class="newExpertise_left_aaa" >
                                    <h2>Salesforce</h2>
                                </a>
                            </th>
                            <th id="newExpertise_left_verical_buffer"></th>
                            <th>
                                <a href="<?php echo $siteURL."/technology/eloqua"; ?>" class="newExpertise_left_aaa">
                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/eloqua.png" alt="eloqua" />
                                </a>
                            </th>
                            <th class="newExpertise_left_firstPicRow">
                                <a href="<?php echo $siteURL."/technology/eloqua"; ?>" class="newExpertise_left_aaa">
                                    <h2>Eloqua</h2>
                                </a>
                            </th>
                            <th id="newExpertise_left_verical_buffer"></th>
                            <th></th>
                            <th></th>
                        </tr>
                        <tr>
                            <th>
                                <a href="<?php echo $siteURL."/technology/gsuite"; ?>" class="newExpertise_left_aaa">
                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/gsuite.png" alt="gsuite" />
                                </a>
                            </th>
                            <th>
                                <a href="<?php echo $siteURL."/technology/gsuite"; ?>" class="newExpertise_left_aaa">
                                    <h2>G Suite</h2>
                                </a>
                            </th>
                            <th id="newExpertise_left_verical_buffer"></th>
                            <th>
                                <a href="<?php echo $siteURL."/technology/salesmanago"; ?>" class="newExpertise_left_aaa">
                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/sales_manago.png" alt="salesmanago" />
                                </a>
                            </th>
                            <th>
                                <a href="<?php echo $siteURL."/technology/salesmanago"; ?>" class="newExpertise_left_aaa">
                                    <h2>SALESmanago</h2>
                                </a>
                            </th>
                            <th id="newExpertise_left_verical_buffer"></th>
                            <th>
                                <a href="<?php echo $siteURL."/technology/marketingxpress"; ?>" class="newExpertise_left_aaa">
                                    <img src="<?php bloginfo('template_url'); ?>/images/logos/groot.png" alt="Marketingxpress" />
                                </a>
                            </th>
                            <th>
                                <a href="<?php echo $siteURL."/technology/marketingxpress"; ?>" class="newExpertise_left_aaa">
                                    <h2>MarketingXpress</h2>
                                </a>
                            </th>
                        </tr>
                    </table>
                </li>
            </ul>
        </div>
        <div id="newExpertise_right">
            <p id="newExpertise_right_1">We connect. We focus on the impact. We are unconventional. We go after results.</p>
            <p id="newExpertise_right_2"><a href="<?php echo $siteURL."/technology"; ?>">&#8594;</a></p>
        </div>
    </div>
    <!-- expertise end -->


    <!-- Testimonial begin -->
<?php
// get case testimonial details
$args = array('post_type' => 'casestudies', 'post_per_page' => -1);
$loop = new WP_Query($args);

$csArray = array();
foreach($loop->posts as $cssss){
    $testimonialVisibility =  get_post_meta($cssss->ID, 'testimonial_visibility', true);
    if($testimonialVisibility == "visible" || $testimonialVisibility == "") {
        array_push($csArray, $cssss->ID);
    }
}
//var_dump($csArray);
$i = rand(0, (count($csArray) - 1));
//echo "all: ".count($csArray)." / i = ".$i;
?>

    <div id="testimonialsContainer">
        <div id="testimonials_title">
            <div id="testimonials_diagonal1" class="diagonals"></div>
            <p>TESTIMONIALS</p>
            <div id="testimonials_diagonal2" class="diagonals"></div>
        </div>
        <div id="testimonials">

            <?php   if($loop->have_posts()) {
                while( count($csArray) > $i ){
                    $postQuote = get_post_meta($csArray[$i], 'testimonial', true);
                    $postQuoteAuthor = get_post_meta($csArray[$i], 'testimonial_author', true);
                    $post = get_post($csArray[$i]);
                    $postContent = $post->post_content;
                    $logo = $stringManipulator->stringExtract($postContent, '{logo}', '{/logo}');
                    ?>
                    <a class="testimonials_item" href="<?php echo $post->guid; ?>">
                        <?php echo $logo; ?>
                        <p class="testimonials_item_quote">
                            <i class="demo-icon icon-quote-left-alt">&#xe802;</i>
                        </p>
                        <div class="testimonials_item_testimonial"><?php echo $postQuote; ?></div>
                        <div class="testimonials_item_author">
                            <div class="testimonials_item_author_name"><?php echo $postQuoteAuthor; ?></div>
                            <p class="testimonials_item_author_quote testimonials_item_quote">
                                <i class="demo-icon icon-quote-right-alt">&#xe801;</i>
                            </p>
                        </div>
                    </a>
                    <?php
                    $i++;
                }
            }
            ?>

        </div>
    </div>
    <div id="testimonials_arrows_holder">
        <div id='testimonials_left_arrow'></div>
        <div id='testimonials_right_arrow'></div>
    </div>
    <!-- Testimonial end -->



    <!-- case studies begin -->
    <div id="cases">
        <img src="<?php bloginfo('template_url'); ?>/images/icons/cases.png" id="casesTopIcon" alt="Onboard case-studies" />
        <p id="casesTit">CASES</p>

        <?php
        $stringManipulator = new StringManipulation();
        $numOfPostedCases = 0;
        // do if there are posts
        if($loop->have_posts()) {

            echo "<ul id='caseStudyUl'>";

            foreach ($loop->posts as $caseStudy) {
                if ($numOfPostedCases < 3) {
                    $caseStudyType = wp_get_post_terms($caseStudy->ID, 'casestudytype');
                    $customFields_thumbnail = get_post_meta($caseStudy->ID, 'thumbnail', true);
                    $postContent = $caseStudy->post_content;
                    $image = $stringManipulator->stringExtract($postContent, '{image}', '{/image}');
                    $imageURL = $stringManipulator->stringExtract($postContent, 'src="', '"');

                    // check for visibility
                    $caseStudyVisibility = get_post_meta($caseStudy->ID, 'visibility', true);
                    if ($caseStudyVisibility == "visible" || $caseStudyVisibility == "") {

                        ?>
                        <li data-type="<?php echo $caseStudyType[0]->name; ?>"
                            class="mix <?php echo $caseStudyType[0]->name; ?>">
                            <a href="<?php echo $caseStudy->guid; ?>" class="aOfCaseStudy">
                                <div class="cs-content-item" <?php if ($imageURL != "") { ?> style="background:url('<?php echo $imageURL; ?>');background-repeat:no-repeat;"<?php } ?> >
                                    <div class="cs-overlay"></div>
                                    <div class="cs-corner-overlay-content">
                                        <p class="cs_permanent_text">CASE STUDY</p>
                                        <p><?php echo $customFields_thumbnail; ?></p>
                                    </div>
                                    <div class="cs-overlay-content">
                                        <h2><?php echo $customFields_thumbnail; ?></h2>
                                        <p><?php echo $caseStudy->post_title; ?></p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <?php

                    }
                    $numOfPostedCases++;
                }

            }
            echo "</ul>";
        }
        ?>


        <a href="<?php echo $siteURL."/case-studies"; ?>" >
            <img src="<?php bloginfo('template_url'); ?>/images/icons/plus2.png" id="casesBottIcon" alt="case-studies" />
        </a>
    </div>
    <!-- case studies end -->


    <!-- contact us field -->
    <div id="home-how-can-help">
        <p id="home-how-can-text">How can we help you?</p>
        <div id="home-how-can-btn">CONTACT US</div>
    </div>



    <!-- footer -->
<?php get_footer();