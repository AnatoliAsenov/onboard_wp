<?php

include "templates/MobileBottomRedLine.php";
include 'string_manipulation/StringManipulation.php';
include 'templates/MobileHeader.php';
include 'templates/MobileFooter.php';

$pageContent = get_post(68);
//$postContent = $pageContent->post_content;

// bottom red line content
$stringManipulator = new StringManipulation();
$stringManipulator->stringExtractAndDelete($pageContent->post_content, '{bottom-red-line}', '{/bottom-red-line}');
$bottomRedLineText = $stringManipulator->neededSubString;
$postContent = $stringManipulator->reducedString;

$header = new MobileHeader(68);
$header->printHTML();

$siteURL = get_site_url();
?>

    <!-- detecting page div-->
    <div style="display:none;" id="whatisthispage">cases</div>

    <!-- top banner -->
    <img src="<?php bloginfo('template_url'); ?>/images/mobile/cases/cases_768.jpg" id="topBanner">

<?php
    $args = array('post_type' => 'casestudies', 'post_per_page' => 3);
    $loop = new WP_Query($args);
?>

    <ul id="cs-three-types">
        <li id="cs-three-types-all" data-type="all" data-filter="all" class="cs-three-types-active control">ALL CASES</li>
        <li id="cs-three-types-service" class="control" data-type="service" data-filter=".service">SERVICE</li>
        <li id="cs-three-types-industry" class="control" data-type="industry" data-filter=".industry">INDUSTRY</li>
    </ul>


<?php

// do if there are posts
if($loop->have_posts()) {

    echo "<ul id='caseStudyUl'>";

    foreach($loop->posts as $caseStudy){
        $caseStudyType = wp_get_post_terms($caseStudy->ID, 'casestudytype');

        // get custom fields data for the current post
        $customFields_thumbnail =  get_post_meta($caseStudy->ID, 'thumbnail', true);

        $postContent = $caseStudy->post_content;
        $image = $stringManipulator->stringExtract($postContent, '{image}', '{/image}');
        $imageURL = $stringManipulator->stringExtract($image, 'src="', '"');

        // check for visibility
        $caseStudyVisibility =  get_post_meta($caseStudy->ID, 'visibility', true);
        if($caseStudyVisibility == "visible" || $caseStudyVisibility == "") {
            ?>
            <li data-type="<?php echo $caseStudyType[0]->name; ?>" class="mix <?php echo $caseStudyType[0]->name; ?>" >
                <a href="<?php echo $siteURL."/mobile/mobile-case?id=".$caseStudy->ID; ?>" class="aOfCaseStudy">
                    <div class="cs-content-item" <?php if($imageURL!=""){?> style="background:url('<?php echo $imageURL; ?>');background-repeat:no-repeat;"<?php } ?> >
                        <div class="cs-overlay"></div>
                        <div class="cs-corner-overlay-content">
                            <p class="cs_permanent_text">CASE STUDY</p>
                            <p><?php echo $customFields_thumbnail; ?></p>
                        </div>
                        <div class="cs-overlay-content">
                            <h2><?php echo $customFields_thumbnail; ?></h2>
                            <p><?php echo $caseStudy->post_title; ?></p>
                        </div>
                    </div>
                </a>
            </li>
            <?php
        }
    }

    echo "</ul>";
}

$bottomRedLine = new MobileBottomRedLine($bottomRedLineText);
$bottomRedLine->printHTML();

$footer = new MobileFooter();
$footer->printHTML();
