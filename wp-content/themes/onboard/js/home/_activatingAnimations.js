/**
 * Created by Anatoli Asenov
 */

if($("#whatisthispage").text() == "home") {

    //---------------- detecting pixel for animation activation -----------------
    var isWheelActivated = false;
    var areTrianglesActivated = false;

    $(window).scroll(function(){

        var middlePixelHeight = $("#trackingPixel_middle").offset().top;

    //-------------------- activate the wheel animation ---------------------
        var wheelTop = $("#innerText").offset().top;

        if(middlePixelHeight >= (wheelTop - 100) && isWheelActivated == false){
            wheelActivationFunc();
            isWheelActivated = true;
        }

        //------------------ activate expertise animation ---------------------

        var newExpertiseTop = $("#newExpertise").offset().top;

        if(middlePixelHeight + 100 >= newExpertiseTop && areTrianglesActivated == false){

            $("#newExpertise_left").animate({
                'marginLeft': 0
            }, 900, function(){
                $("#newExpertise_left ul").animate({
                    'paddingTop': '250px',
                    'paddingLeft': '100px',
                    'opacity': 1
                }, 700);
            });

            $("#newExpertise_right").animate({
                'right': '-53%',
                'top': '-65%'
            }, 900, function(){
                $("#newExpertise_right p").animate({
                    'opacity': 1
                }, 600);
            });

            areTrianglesActivated = true;
        }

    });

}
